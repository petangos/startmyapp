import React from 'react';
import { Table, Button, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import './style.scss';
import { withFirebase } from '../../../../../ducks/firebase';

const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@withFirebase
class CategoriesList extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  unsubscribe = () => null;

  async getCategories() {
    const client = this.props.firebase.client;
    this.unsubscribe = client
      .firestore()
      .collection('categories')
      .where(
        'post_type',
        '==',
        this.props.category.slice(0, this.props.category.length - 1)
      )
      .onSnapshot(snap =>
        this.setState({
          data: snap.docs.map((doc, index) => ({
            key: doc.id,
            id: doc.id,
            ...doc.data()
          }))
        })
      );
  }

  deletePost(postId) {
    const client = this.props.firebase.client;
    client
      .firestore()
      .collection('categories')
      .doc(postId)
      .delete();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.firebase.client.name !== this.props.firebase.client.name ||
      prevProps.category !== this.props.category
    ) {
      this.unsubscribe();
      this.getCategories();
    }
  }

  componentDidMount() {
    this.getCategories();
  }

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  onSearch = () => {
    const { searchText, tableData } = this.state;
    let reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0
                        ? [<span className="highlight">{match[0]}</span>, text]
                        : text
                  )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;
    const columns = [
      {
        title: 'title',
        dataIndex: 'label',
        key: 'title',
        render: (text, record) => (
          <Link
            to={`/categories/edit/${record.id}`}
            className="utils__link--underlined">
            {text}
          </Link>
        ),
        sorter: (a, b) => a.id - b.id
      },
      {
        title: 'Thumbnail',
        dataIndex: 'media',
        key: 'media',
        render: item =>
          item ? (
            <a href="" className="productsList__thumbnail">
              <img src={item[0].src} alt="" style={{}} />
            </a>
          ) : null
      },

      {
        title: 'Action',
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              <Link to={`/categories/${this.props.category}/edit/${record.id}`}>
                <i className="icmn-pencil mr-1" /> Edit
              </Link>
              {'   '}
              <Popconfirm
                title="Are you sure？"
                onConfirm={() => this.deletePost(record.id)}
                okText="Yes"
                cancelText="No">
                <a>
                  <i className="icmn-cross mr-1" /> Remove
                </a>
              </Popconfirm>
            </span>
          );
        }
      }
    ];

    return (
      <div className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>{this.props.postType} List</strong>
          </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            pagination={pager}
            footer={() => (
              <Link to={`/categories/${this.props.category}/edit`}>
                <Button>add new</Button>
              </Link>
            )}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}

export default CategoriesList;

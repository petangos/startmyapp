import React from 'react';
import { Table, Button } from 'antd';
import { Link } from 'react-router-dom';
import { withFirebase } from '../../../../../ducks/firebase';
import './style.scss';

const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@withFirebase
class ScreensList extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  async getAvailableScreens() {
    const client = this.props.firebase.client;
    return await client
      .firestore()
      .collection('posts')
      .where('type', '==', 'screen')
      .get()
      .then(res => res.docs.map(item => ({ id: item.id, ...item.data() })));
    /* const userRecord = await firebase
      .firestore()
      .collection('users')
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then(doc => doc.data());

    const app = await firebase
      .firestore()
      .collection('applications')
      .doc(userRecord.whitelist[0])
      .get()
      .then(res => res.data());
    return Object.keys(app.screens).map(key => ({
      name: key,
      title: app.screens[key].title,
      content: app.screens[key].content
    })); */
  }

  componentDidMount() {
    this.getAvailableScreens().then(screens =>
      this.setState({
        data: screens.map(item => ({ ...item, key: item.name }))
      })
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.setState({
        tableData: [],
        data: [],
        pager: { ...defaultPagination },
        filterDropdownVisible: false,
        searchText: '',
        filtered: false
      });
      this.getAvailableScreens().then(screens =>
        this.setState({
          data: screens.map(item => ({ ...item, key: item.name }))
        })
      );
    }
  }

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  onSearch = () => {
    const { searchText, tableData } = this.state;
    let reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0
                        ? [<span className="highlight">{match[0]}</span>, text]
                        : text
                  )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;
    const columns = [
      {
        screen: 'Screen',
        dataIndex: 'name',
        key: 'screen',
        render: (text, record) => {
          return (
            <Link to={`/screens/edit/${record.id}`}>
              <h5>{text}</h5>
            </Link>
          );
        },
        sorter: (a, b) => a.id - b.id
      },
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        render: (text, record) => {
          return <h5>{text}</h5>;
        },
        sorter: (a, b) => a.id - b.id
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              <Link to={`/screens/edit/${record.id}`}>
                <i className="icmn-pencil mr-1" /> Edit
              </Link>
            </span>
          );
        }
      }
    ];

    return (
      <div className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>Screens List</strong>
          </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            pagination={pager}
            onChange={this.handleTableChange}
            footer={() => (
              <Link to="screens/edit">
                <Button>Add</Button>
              </Link>
            )}
          />
        </div>
      </div>
    );
  }
}

export default ScreensList;

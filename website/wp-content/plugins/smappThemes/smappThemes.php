<?php
/*
  Plugin Name: SMApp Themes
  Description: Adjustment Themes
  Author: Koko-Apps
  Version: 1.0.1
 */
if (!defined('ABSPATH')) {
    exit;
}



if (!class_exists('SMAppThemes')) {

    class SMAppThemes {

        public $enable_caps = array(
            'edit_published_posts',
            'edit_published_pages',
            'edit_private_pages',
            'edit_private_posts',
            'edit_others_posts',
            'publish_posts',
            'delete_posts',
            'delete_private_posts',
            'delete_published_posts',
            'delete_others_posts',
            'manage_categories',
            'manage_links',
            'moderate_comments',
            'upload_files',
        );
        public $disable_caps = array(
            'edit_others_pages',
            'publish_pages',
            'delete_pages',
            'delete_private_pages',
            'delete_published_pages',
            'delete_others_pages',
            'export',
            'import',
            'list_users',
            'promote_users'
        );

        public function __construct() {
            add_action('init', array($this, 'smapp_theme_init'));
            add_action('admin_init', array($this, 'admin_init'));
            add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 99, 0);
            add_action('admin_menu', array($this, 'admin_menu'), 99, 0);
            add_action('wp_dashboard_setup', array($this, 'wp_dashboard_setup'), 99, 0);
            add_action('admin_head', array($this, 'admin_head'), 99, 0);
            add_filter('get_site_icon_url', array($this, 'smapp_theme_fav_icon'), 99, 0);
            add_action('login_enqueue_scripts', array($this, 'smapp_login_logo_one'));

            add_filter('page_row_actions', function ($actions) {
                if (current_user_can('administrator')) {
                    return $actions;
                }
                unset($actions['inline hide-if-no-js']);
                return $actions;
            }, 10, 1);
        }

        public function smapp_theme_init() {
            load_plugin_textdomain('smapp', false, basename(dirname(__FILE__)) . '/languages');
        }

        public function admin_init() {
            $this->smapp_theme_settings_fields();

            if (current_user_can('administrator')) {
                return;
            }

//            if ($_SERVER["SCRIPT_NAME"] == '/wp-admin/index.php') {
//                wp_redirect(admin_url('edit.php?post_type=shop_order'));
//                die();
//            }
        }

        public function admin_enqueue_scripts() {
            wp_enqueue_style('admin_css', plugins_url('/assets/css/admin.css', __FILE__));
            if (!current_user_can('administrator')) {
                wp_enqueue_style('shopm_css', plugins_url('/assets/css/shop-manager.css', __FILE__));
            }
        }

        public function admin_menu() {
            global $menu;

            add_options_page(__("Theme Settings", 'smapp'), __("Theme Settings", 'smapp'), "manage_options", "smapp-theme-settings", array($this, "smapp_theme_settings_page"));

            if (current_user_can('administrator')) {
                return;
            }

            add_filter('pre_site_transient_update_core', array($this, 'remove_core_updates')); //hide updates for WordPress itself
            add_filter('pre_site_transient_update_plugins', array($this, 'remove_core_updates')); //hide updates for all plugins
            add_filter('pre_site_transient_update_themes', array($this, 'remove_core_updates')); //hide updates for all themes

            remove_menu_page('index.php');                  //Dashboard
            remove_menu_page('jetpack');                    //Jetpack*
            remove_menu_page('edit.php');                   //Posts
            remove_menu_page('upload.php');                 //Media
            remove_menu_page('edit-comments.php');          //Comments
            remove_menu_page('themes.php');                 //Appearance
            remove_menu_page('plugins.php');                //Plugins
            remove_menu_page('users.php');                  //Users
            remove_menu_page('tools.php');                  //Tools
            remove_menu_page('options-general.php');        //Settings

            $remove = array('wc-settings', 'wc-status', 'wc-addons',);
            foreach ($remove as $submenu_slug) {
                if (!current_user_can('update_core')) {
                    remove_submenu_page('woocommerce', $submenu_slug);
                }
            }

            $menu['55.5'][0] = __('Shop', 'smapp');

            $role = get_role('shop_manager');
            foreach ($this->disable_caps as $cap) {
                $role->remove_cap($cap);
            }
            foreach ($this->enable_caps as $cap) {
                $role->add_cap($cap);
            }

            global $submenu;
            unset($submenu['edit.php?post_type=page'][10]);
            if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
                echo '<style type="text/css">
												.subsubsub li:not(.mine),
												.wrap .page-title-action { display:none; }
											</style>';
            }
        }

        public function remove_core_updates() {
            global $wp_version;
            return(object) array(
                        'last_checked' => time(),
                        'version_checked' => $wp_version,
                        'updates' => array()
            );
        }

        public function wp_dashboard_setup() {
            if (current_user_can('administrator')) {
                return;
            }

            global $wp_meta_boxes;
            unset($wp_meta_boxes['dashboard']);
        }

        public function admin_head() {
            $upload_dir = wp_upload_dir();

            if (!empty($upload_dir['basedir'])) {
                $favicon_url = $upload_dir['baseurl'] . '/themes/favicon/favicon.ico';
                echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
            }
        }

        public function smapp_theme_settings_page() {
            echo '<div class="wrap">
							<form method="post" action="options.php" enctype="multipart/form-data">';
            settings_fields("section");
            do_settings_sections("smapp-theme-options");
            submit_button();
            echo "</form>\n</div>";
        }

        public function smapp_theme_settings_fields() {
            add_settings_section("section", __("Theme Settings", 'smapp'), null, "smapp-theme-options");
            add_settings_field("smapp-fav-icon", __("Fav Icon", 'smapp'), array($this, 'smapp_favicon_element'), "smapp-theme-options", "section", 'smapp');
            add_settings_field("smapp-logo-image", __("Logo Image", 'smapp'), array($this, 'smapp_logo_element'), "smapp-theme-options", "section", 'smapp');
            register_setting("section", "smapp-theme-options", array($this, 'validate_upload'));
        }

        function smapp_favicon_element() {
            echo '<p><input type="file" name="smapp_favicon" placeholder="' . __('Upload Fav Icon', 'smapp') . '" />';
            $smapp_options = get_option('smapp-theme-options');
            if (isset($smapp_options['smapp_favicon'])) {
                echo "<img src=\"$smapp_options[smapp_favicon]\"/>";
            }
            echo "</p>";
        }

        function smapp_logo_element() {
            echo '<p><input type="file" name="smapp_logo_image" placeholder="' . __('Upload logo Image', 'smapp') . '" />';
            $smapp_options = get_option('smapp-theme-options');
            if (isset($smapp_options['smapp_logo_image'])) {
                echo "<img src=\"$smapp_options[smapp_logo_image]\"/>";
            }
            echo "</p>";
        }

        function validate_upload($theme_options) {
            static $pass_count = 0;
            $pass_count++;

            if ($pass_count <= 1) {
                $keys = array_keys($_FILES);
                $i = 0;
                foreach ($_FILES as $image) {
                    // if a files was upload
                    if ($image['size']) {
                        // if it is an image
                        if (preg_match('/(jpg|jpeg|png|gif|icon)$/', $image['type'])) {
                            $override = array('test_form' => false);
                            // save the file, and store an array, containing its location in $file
                            $file = wp_handle_upload($image, $override);
                            $theme_options[$keys[$i]] = $file['url'];
                        } else {
                            // Not an image.
                            $options = get_option('smapp-theme-options');
                            $theme_options[$keys[$i]] = $options[$keys[$i]];
                            // Die and let the user know that they made a mistake.
//							wp_die('No image was uploaded.');
                            add_action('admin_notices', function() {
                                echo __('Not a valid image!', 'smapp');
                            });
                        }
                    }
                    // Else, the user didn't upload a file.
                    // Retain the image that's already on file.
                    else {
                        $options = get_option('smapp-theme-options');
                        $theme_options[$keys[$i]] = $options[$keys[$i]];
                    }
                    $i++;
                }
            }
            return $theme_options;
        }

        function smapp_login_logo_one() {
            $smapp_options = get_option('smapp-theme-options');
            if ($smapp_options['smapp_logo_image']) {
                ?>
                <style type="text/css">
                    body.login div#login h1 a {
                        background-image: url(<?php echo $smapp_options['smapp_logo_image'] ?>) !important;
                        width: 100% !important;
                        background-size: contain !important;

                    }
                </style>
                <?php
            }
        }

        function smapp_theme_fav_icon() {
            $smapp_options = get_option('smapp-theme-options');
            return $smapp_options['smapp_favicon'];
        }

    }

    $SMAppThemes = new SMAppThemes();
}


if (!class_exists('SMAppRedirectLogin')) {

    class SMAppRedirectLogin {

        /**
         * Init - Hook redirect to template_redirect if filter is used, else to parse_request to prevent unnecessary load.
         *
         * @since 1.7.0
         */
        public static function init() {

            if (has_filter('rtl_override_redirect')) {
                $hook = 'template_redirect';
            } else {
                $hook = 'parse_request';
            }
            add_action($hook, array(__CLASS__, 'redirect'), 1);
        }

        /**
         * If the rtl_override_redirect filter is not true the user is logged in or authentication redirect is invoked.
         *
         * @since 1.5
         */
        public static function redirect() {

            /**
             * Filter to override the redirect.
             *
             * @since 1.7.0
             *
             * @var boolean $override Set to true to override redirect.
             */
            $override = apply_filters('rtl_override_redirect', $override = false);

            if (strpos($_SERVER["REQUEST_URI"], '/wp-json/') !== false) {
                return true;
            }

            if (true !== $override) {
                is_user_logged_in() || auth_redirect();
            }
        }

    }

    SMAppRedirectLogin::init();
}
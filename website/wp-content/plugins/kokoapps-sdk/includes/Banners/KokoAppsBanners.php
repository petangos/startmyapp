<?php

class KokoAppsBanners extends KokoAppsBannersHelp {

    function __construct() {
        add_action('init', array(&$this, 'init'));
        register_activation_hook(__FILE__, array($this, 'register_activation_hook'));
    }

    function init() {
        load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages');

        $labels = array(
            'name' => _x($this->name, 'Post Type General Name', $this->textdomain),
            'singular_name' => _x($this->singular_name, 'Post Type Singular Name', $this->textdomain),
            'menu_name' => __($this->name, $this->textdomain),
            'parent_item_colon' => __('Parent ' . $this->singular_name, $this->textdomain),
            'all_items' => __('All ' . $this->name, $this->textdomain),
            'view_item' => __('View ' . $this->singular_name, $this->textdomain),
            'add_new_item' => __('Add New Banners', $this->textdomain),
            'add_new' => __('Add New', $this->textdomain),
            'edit_item' => __('Edit ' . $this->singular_name, $this->textdomain),
            'update_item' => __('Update ' . $this->singular_name, $this->textdomain),
            'search_items' => __('Search ' . $this->singular_name, $this->textdomain),
            'not_found' => __('Not Found', $this->textdomain),
            'not_found_in_trash' => __('Not found in Trash', $this->textdomain),
        );

        $args = array(
            'label' => __($this->slug, $this->textdomain),
            'description' => __($this->singular_name . ' news and reviews', $this->textdomain),
            'labels' => $labels,
            // Features this CPT supports in Post Editor
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
                'revisions',
                'custom-fields',
            ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies' => array('genres'),
            /* A hierarchical CPT is like Pages and can have
             * Parent and child items. A non-hierarchical CPT
             * is like Posts.
             */
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 15,
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => true
        );

        // Registering your Custom Post Type
        register_post_type($this->post_type, $args);
    }

    function register_activation_hook() {
        
    }

}

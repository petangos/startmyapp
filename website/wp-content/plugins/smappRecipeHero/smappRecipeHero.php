<?php

/*
  Plugin Name: SMApp RecipeHero
  Plugin URI:
  Description: Extends RecipeHero
  Author: KokoApps
  Version: 1.0.1
 */

if (!defined('ABSPATH')) {
	exit;
}


if (!class_exists('smappRecipeHero')) {

	class smappRecipeHero {
//		$textdomain =

		function __construct() {
			add_action( 'init', array($this, 'enableRestSupport'), 25 );
			add_action( 'rest_api_init', array($this, 'enableMetaRestSupport') );
		}

		function enableRestSupport() {
			global $wp_post_types, $wp_taxonomies;

			//be sure to set this to the name of your post type!
			$post_type_name = 'recipe';
			if( isset( $wp_post_types[ $post_type_name ] ) ) {
				$wp_post_types[$post_type_name]->show_in_rest = true;
//				// Optionally customize the rest_base or controller class
//				$wp_post_types[$post_type_name]->rest_base = $post_type_name;
//				$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
			}


			//be sure to set this to the name of your taxonomy!
			$taxonomy_names = array('course', 'cuisine');
			foreach ($taxonomy_names as $taxoname) {
				if (isset($wp_taxonomies[$taxoname])) {
					$wp_taxonomies[$taxoname]->show_in_rest = true;

//				// Optionally customize the rest_base or controller class
//				$wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
//				$wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
				}
			}
		}


		function enableMetaRestSupport() {

			register_rest_field( 'recipe', 'meta', array(
					'get_callback' => function( $object ) {
						$post_id = $object['id'];
						return get_post_meta( $post_id );
					},
					'schema' => null,
				)
			);
			register_rest_field( 'recipe', 'image', array(
					'get_callback' => function( $object ) {
						$post_id = $object['id'];

						$image_id = get_post_thumbnail_id($post_id);
						return array(
							'id' => (int) $image_id,
							'date_created' => wc_rest_prepare_date_response($object->post_date),
							'date_created_gmt' => wc_rest_prepare_date_response($object->post_date_gmt),
							'date_modified' => wc_rest_prepare_date_response($object->post_modified),
							'date_modified_gmt' => wc_rest_prepare_date_response($object->post_modified_gmt),
							'src' => wp_get_attachment_url($image_id),
							'title' => get_the_title($image_id),
							'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
						);
					},
					'schema' => null,
				)
			);
		}

	}

	$smappRecipeHero = new smappRecipeHero();
}

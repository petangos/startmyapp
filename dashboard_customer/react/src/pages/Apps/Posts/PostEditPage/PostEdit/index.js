import React from 'react';
import AddForm from './AddForm';
import './style.scss';

class PostEdit extends React.Component {
  render() {
    return (
      <section className="card">
        <div className="card-header mb-2">
          <div className="utils__title">
            <strong>{this.props.postType} Add/Edit</strong>
          </div>
        </div>
        <div className="card-body">
          <div className="add-post">
            <AddForm {...this.props} />
          </div>
        </div>
      </section>
    );
  }
}

export default PostEdit;

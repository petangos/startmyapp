// import React from 'react'
import React from 'react'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { Form, Input, Button, Radio, Select, Upload, Icon } from 'antd'
import { translate } from 'react-i18next'
//import './wysiwyg-editor.css'

const FormItem = Form.Item
const RadioGroup = Radio.Group
const Dragger = Upload.Dragger

@Form.create()
class AddForm extends React.Component {
  render() {
    const { t, form } = this.props
    return (
      <Form className="add-post__form mt-3">
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>{t('Title')}</strong>
            </label>
            <Input placeholder={t('Title')} />
          </div>
        </FormItem>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>{t('Type')}</strong>
            </label>
            <RadioGroup>
              <Radio value={'text'}>{t('Text')}</Radio>
              <Radio value={'video'}>{t('Video')}</Radio>
              <Radio value={'image'}>{t('Image')}</Radio>
              <Radio value={'audio'}>{t('Audio')}</Radio>
              <Radio value={'vimeo'}>{t('Vimeo')}</Radio>
            </RadioGroup>
          </div>
        </FormItem>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>{t('Category')}</strong>
            </label>
            <Select
              mode="tags"
              size="default"
              placeholder={t('Select post category')}
              defaultValue={['travel', 'lifestyle']}
              style={{ width: '100%' }}
            />
          </div>
        </FormItem>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>{t('Content')}</strong>
            </label>
            <div className="add-post__editor">
              <Editor />
            </div>
          </div>
        </FormItem>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>{t('Files')}</strong>
            </label>
            <Dragger>
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">{t('Click or drag file to this area to upload')}</p>
              <p className="ant-upload-hint">
                {t(
                  'Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files',
                )}
              </p>
            </Dragger>
          </div>
        </FormItem>
        <FormItem>
          <div className="add-post__submit">
            <span className="mr-3">
              <Button type="primary" htmlType="submit">
                {t('Save and Post')}
              </Button>
            </span>
            <Button type="danger" htmlType="submit">
              {t('Discard')}
            </Button>
          </div>
        </FormItem>
      </Form>
    )
  }
}

export default translate()(AddForm)

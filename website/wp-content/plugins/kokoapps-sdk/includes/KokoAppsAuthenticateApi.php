<?php

class KokoAppsAuthenticateApi {

    private $expiration = 0;

    public function __construct() {
        global $KokoAppsexpiration;

        $time = strtotime(date("m/d/Y h:i:s a", time() + ($KokoAppsexpiration * HOUR_IN_SECONDS)));

        $this->expiration = $time;

        add_action('rest_api_init', function () {
            register_rest_route('kokoapps/v2/', '/authenticate', array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(&$this, 'authenticate'),
                'args' => array(
                ),
            ));

            register_rest_route('kokoapps/v2/', '/authenticate/token/isvalid', array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(&$this, 'tokenIsValid'),
                'args' => array(
                ),
            ));
        });


        add_filter('rest_endpoints', function ($routes) {

            if (!($route = & $routes['/wc/v2/products'])) {
                return $routes;
            }

            $route[0]['args']['orderby']['enum'][] = 'meta_value_num';

            $route[0]['args']['meta_key'] = array(
                'description' => 'The meta key to query.',
                'type' => 'string',
                'enum' => ['total_sales'],
                'validate_callback' => 'rest_validate_request_arg',
            );

            return $routes;
        });

        add_filter('woocommerce_rest_product_object_query', function ($args, $request) {
            $args['meta_key'] = $request['meta_key'];
            return $args;
        }, 10, 2);
    }

    public function authenticate($request) {
        switch ($_POST['provider']) {
            case 'facebook':
                return KokoAppsFacebookLogin::Login($request, $this->expiration);
            default:
                return KokoAppsLogin::Login($request, $this->expiration);
        }
    }

    public function tokenIsValid($request) {
        if (!empty($request['kokoapps_access_token'])) {
            $args = array(
                'meta_key' => 'kokoapps_access_token',
                'meta_value' => $request['kokoapps_access_token']
            );
            $user_query = new WP_User_Query($args);
            $users = $user_query->get_results();
            if (!empty($users)) {
                $userdata = $user_query->get_results()[0]->data;

                if (!KokoAppsToken::is_still_valid($userdata)) {
                    return wp_send_json(new WP_Error('user', 'User error', 'Access token'), 401);
                }

                return KokoAppsLogin::GetInfo($userdata, $request['kokoapps_access_token']);
            } else {
                return wp_send_json(new WP_Error('user', 'User error', 'Access token'), 401);
            }
        } else {
            return wp_send_json(new WP_Error('user', 'User error', 'Access token'), 401);
        }
    }

}

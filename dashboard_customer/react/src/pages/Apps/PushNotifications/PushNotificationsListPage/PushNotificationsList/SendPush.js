import React from 'react';
import { Button, Input, Select, message } from 'antd';
import moment from 'moment';
import { Form, Field } from 'react-final-form';
import { withFirebase } from '../../../../../ducks/firebase';

@withFirebase
export default class SendPush extends React.Component {
  state = { data: {}, tokens: [], users: [], isTokendReady: false };

  componentDidMount() {
    this.getUsers().then(users => this.setState({ users }));
    this.getTokens().then(tokens =>
      this.setState({ tokens, isTokendReady: true })
    );
  }

  getTokens() {
    return this.props.firebase.client
      .firestore()
      .collection('token')
      .get()
      .then(res => res.docs.map(item => item.data()));
  }

  getUsers() {
    return this.props.firebase.client
      .firestore()
      .collection('users')
      .get()
      .then(res =>
        res.docs.map(item => ({ name: item.data().full_name, uid: item.id }))
      );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.getUsers().then(users => this.setState({ users }));
      this.getTokens().then(tokens =>
        this.setState({ tokens, isTokendReady: true })
      );
    }
  }

  async onSubmit(values) {
    return new Promise(async (resolve, reject) => {
      const recipients = this.state.tokens
        .filter(item => {
          if (values.users && values.users.length > 0)
            return (
              item.release_channel === values.releaseChannel &&
              values.users.find(user => user === item.uid)
            );
          return item.release_channel === values.releaseChannel;
        })
        .map(item => ({ uid: item.uid, token: item.expoToken }));
      const pushRecord = await this.props.firebase.client
        .firestore()
        .collection('push')
        .add({
          message: {
            title: values.title,
            body: values.message
          },
          created_at: moment().unix(),
          release_channel: values.releaseChannel,
          status: 'sent',
          recipients
        });

      const request = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          message: {
            body: values.message,
            data: { messageId: pushRecord.id },
            title: values.title
          },

          tokens: recipients.map(item => item.token)
        })
      };

      const endpoint = 'https://www.koko-apps.com/services/push';

      const response = await fetch(endpoint, request);
      if (response.ok) message.success('Messages sent');
      else message.error('An error occured while sending messages');
      resolve(response.ok);
    });
  }

  form = React.createRef();

  render() {
    return (
      <Form
        ref={this.form}
        onSubmit={val => this.onSubmit(val)}
        className="login-form"
        initialValues={{ users: [], releaseChannel: 'production' }}>
        {({ submitting, valid, handleSubmit, reset }) => {
          return (
            <form onSubmit={handleSubmit}>
              <div className="row">
                <div className="col-lg-6">
                  <label className="form-label mb-0">Title</label>
                  <Field
                    name="title"
                    validate={val =>
                      val && val.length > 3 && val.length < 50
                        ? undefined
                        : 'Title has to be between 3 and 50 characters long.'
                    }>
                    {({ input, meta }) => (
                      <Input placeholder="Notification title" {...input} />
                    )}
                  </Field>
                </div>
                <div className="col-lg-6">
                  {this.props.firebase.applications
                    .find(item => item.name === '[DEFAULT]')
                    .auth().currentUser.uid && (
                    <React.Fragment>
                      <label className="form-label mb-0">Environment</label>
                      <Field name="releaseChannel">
                        {({ input, meta }) => (
                          <Select {...input}>
                            <Select.Option value="staging">
                              Staging
                            </Select.Option>
                            <Select.Option value="production">
                              Production
                            </Select.Option>
                          </Select>
                        )}
                      </Field>
                    </React.Fragment>
                  )}
                </div>
              </div>
              {/*  <div className="row">
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Scheduele</label>
              {getFieldDecorator('start_time', {
                rules: [{ required: true, message: 'Choose environment' }]
              })(<Input placeholder="Phone" />)}
            </FormItem>
          </div>
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Channel</label>
              {getFieldDecorator('channel', {})(
                <Input placeholder="Notification Channel (optional)" />
              )}
            </FormItem>
          </div>
        </div> */}
              <div className="row">
                <div className="col-lg-12">
                  <label className="form-label mb-0">Users (optional)</label>
                  <Field name="users">
                    {({ input, meta }) => (
                      <Select
                        mode="multiple"
                        {...input}
                        placeholder="Leave empty for global send">
                        {this.state.users.map(user => (
                          <Select.Option key={user.uid} value={user.uid}>
                            {user.name}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </Field>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <label className="form-label mb-0">Message</label>
                  <Field
                    name="message"
                    validate={val =>
                      val && val.length > 3 && val.length <= 250
                        ? undefined
                        : 'Message has to be between 3 and 250 characters long'
                    }>
                    {({ input, meta }) => (
                      <Input placeholder="Notification title" {...input} />
                    )}
                  </Field>
                </div>
              </div>
              <div className="form-actions">
                <Button
                  loading={submitting}
                  style={{ width: 150 }}
                  type="primary"
                  htmlType="submit"
                  disabled={!this.state.isTokendReady || submitting || !valid}
                  className="mr-3">
                  Submit
                </Button>
                <Button
                  onClick={() => {
                    if (this.form.current) this.form.current.form.reset();
                  }}>
                  Reset
                </Button>
              </div>
            </form>
          );
        }}
      </Form>
    );
  }
}

"use strict";

const admin = require('firebase-admin');

const FIREBASE_SERVICE_ACCOUNT = require('../xl-fb-admin.json');

const moment = require('moment');

const _require = require('../utils'),
      dateify = _require.dateify,
      toDate = _require.toDate,
      cleanObject = _require.cleanObject;

if (admin.apps.length === 0) {
  admin.initializeApp({
    credential: admin.credential.cert(FIREBASE_SERVICE_ACCOUNT),
    databaseURL: 'https://koko-apps.firebaseio.com'
  });
  const firestore = new admin.firestore();
  const settings = {
    timestampsInSnapshots: true
  };
  firestore.settings(settings);
}

module.exports = {};
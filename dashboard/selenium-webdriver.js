/**
 * java -jar selenium-server-standalone-3.14.0.jar
 */

const { Builder, By, Key, promise, until } = require('selenium-webdriver');
const { Channel, Options } = require('selenium-webdriver/firefox');

let i = 0;
function resposition(driver) {
  return driver.manage().window().setRect({
    width: 600,
    height: 400,
    x: 300 * (i++),
    y: 0
  });
}

async function doSearch(driver) {
  try {
    // Start on the base about page.
    await driver.get('about:')
    // Reposition so users can see the three windows.
    await resposition(driver);
    // Pause so users can see the magic.
    await promise.delayed(750);
    // Now do the rest.
    await driver.get('http://www.google.com/ncr');
    await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
    console.log('Success!');
  } catch (ex) {
    console.log('An error occured! ' + ex);
  } finally {
    await driver.quit();
  }
}

function createDriver(channel) {
  let options = new Options().setBinary(channel);
  return new Builder()
    .forBrowser('firefox')
    .usingServer('http://localhost:4444/wd/hub')
    .setFirefoxOptions(options)
    .build();
}

Promise.all([
  doSearch(createDriver(Channel.RELEASE)),
  doSearch(createDriver(Channel.AURORA)),  // Developer Edition.
  doSearch(createDriver(Channel.NIGHTLY)),
]).then(_ => {
  console.log('All done!');
}, err => {
  console.error('An error occured! ' + err);
  setTimeout(() => { throw err }, 0);
});
import React from 'react';
import { Table } from 'antd';
//import './style.scss';
import { withFirebase } from '../../../../../../ducks/firebase';
import moment from 'moment';

const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@withFirebase
class Meals extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  async getMeals() {
    const { uid } = this.props;
    const meals = await this.props.firebase.client
      .firestore()
      .collection('posts')
      .where('type', '==', 'meal')
      .where('author', '==', uid)
      .get()
      .then(res => res.docs.map(item => item.data()));
    const data = meals.reduce((acc, cur) => {
      const today = moment(cur.created_at.toDate()).format('DD-MM-YYYY');
      if (!acc[today]) acc[today] = {};
      acc[today][cur.title] = cur.content;
      return acc;
    }, {});
    return Promise.resolve(
      Object.keys(data)
        .map(key => ({ date: key, ...data[key] }))
        .sort(
          (a, b) =>
            moment(a.date, 'DD-MM-YYYY').isBefore(moment(b.date, 'DD-MM-YYYY'))
              ? 1
              : -1
        )
    );
  }

  async getUsers() {
    const client = this.props.firebase.client;
    this.unsubscribe = client
      .firestore()
      .collection('users')
      .onSnapshot(res => {
        const users = res.docs.map(item => ({
          uid: item.id,
          ...item.data(),
          key: item.id
        }));
        this.setState({ data: users });
      });
  }

  async componentDidMount() {
    this.setState({ data: await this.getMeals() });
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.setState({ data: await this.getMeals() });
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;
    const columns = [
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date'
      },
      {
        title: 'Breakfast',
        dataIndex: 'breakfast',
        key: 'breakfast'
      },
      {
        title: 'Brunch',
        dataIndex: 'brunch',
        key: 'brunch'
      },
      {
        title: 'Lunch',
        dataIndex: 'lunch',
        key: 'lunch'
      },
      {
        title: 'Supper',
        dataIndex: 'supper',
        key: 'supper'
      },
      {
        title: 'Dinner',
        dataIndex: 'dinner',
        key: 'dinner'
      },
      {
        title: 'Night',
        dataIndex: 'night',
        key: 'night'
      },
      {
        title: 'Snack',
        dataIndex: 'snack',
        key: 'snack'
      }
    ];

    return (
      <div className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>Meals</strong>
          </div>
        </div>
        <div className="card-body">
          <Table
            rowKey={item => item.date}
            columns={columns}
            dataSource={data}
            pagination={pager}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}

export default Meals;

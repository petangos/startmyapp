import { createAction, createReducer } from 'redux-act';
import { connect } from 'react-redux';
import React from 'react';

const REDUCER = 'firebase';
const NS = `@@${REDUCER}/`;

export const setFirebaseApp = createAction(`${NS}SET_FIREBASE_APP`);
export const initFirebaseApps = createAction(`${NS}INIT_FIREBASE_APPS`);
const initialState = {
  client: null,
  applications: []
};

export default createReducer(
  {
    [setFirebaseApp]: (state, client) => {
      window.localStorage.setItem('@kokoapps/projectId', client.name);
      return {
        ...state,
        client
      };
    },
    [initFirebaseApps]: (state, applications) => ({ ...state, applications })
  },

  initialState
);

export function withFirebase(WrappedComponent) {
  const mapStateToProps = (state, props) => ({ firebase: state.firebase });
  return @connect(mapStateToProps)
  class extends React.Component {
    render() {
      const { firebase, ...rest } = this.props;
      return <WrappedComponent firebase={firebase} {...rest} />;
    }
  };
}

<?php
/*
  Plugin Name: SMApp User Custom Data API
  Plugin URI:
  Description: Implements custom user meta data update via REST API
  Author: KokoApps
  Version: 1.0.1
 */

if (!defined('ABSPATH')) {
	exit;
}


if (!class_exists('smappUserCustomData')) {

	class smappUserCustomData {

		private $textdomain = 'smapp-usercustom';

		private $custom_fields = array();

		function __construct() {
			add_action('init', array($this, 'init'), 25);

			$this->custom_fields = $this->getCustomFields();
		}


		function init() {
			load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages');
			add_action('rest_api_init', array($this, 'rest_api_init'), 10, 2);
			add_action('show_user_profile', array($this, 'show_custom_profile_fields'));
			add_action('edit_user_profile', array($this, 'show_custom_profile_fields'));
		}

		public function rest_api_init() {
			register_rest_route('kokoapps/v2', 'user/(?P<uid>\d+)/data',
				array(
					'methods'             => WP_REST_Server::EDITABLE,
					'callback'            => array($this, 'setUserData'),
					'args'                => array(
						'uid' => array(
							'required'          => true,
							'validate_callback' => function ($param, $request, $key) {
								return is_numeric($param);
							}
						),
					),
					'permission_callback' => function ($request) {
						return current_user_can('read');
					}
				)
			);
		}

		function setUserData($request) {
			$params =  $request->get_params();
			$result = array();
			$user = get_user_by('ID', $params['uid']);

			if (! $user)  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			elseif(! $params['data'] || ! is_array($params['data']) || ! $this->is_assoc($params['data'])) {
				$result = array (
					"code" => "err",
					"message" => "Data param invalid",
					"data" => array(),
					"status" => 400
				);
			}
			else {
				$count = 0;
				foreach ($params['data'] as $key => $val) {
					if(strpos($key, 'smapp_custom_') !== false) {
						update_user_meta($params['uid'], $key, $val);
					}
				}
				$meta = get_user_meta($params['uid']);
				$meta_custom = array();
				foreach ($meta as $key => $valar) {
					if(strpos($key, 'smapp_custom_') !== false) {
						$meta_custom[$key] = $valar[0];
					}
				}

				$result = array(
					"code"    => "OK",
					"message" => "Success",
					"data"    => $meta_custom,
					"status"  => 200
				);
			}

			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}


		function show_custom_profile_fields($user) {

			$meta = get_user_meta($user->ID);
			$meta_custom = array();
			foreach ($meta as $key => $valar) {
				if(strpos($key, 'smapp_custom_') !== false) {
					$meta_custom[$key] = $valar[0];
				}
			}

			echo '<h3>Extra profile information</h3>
							<table class="form-table">';

			$meta_number = 0;
			foreach ($meta_custom as $key => $val) {
				$meta_number++;
				echo <<<HTML
				<tr>
					<th><label for="$key">$key</label></th>
					<td>
						<input  type="text" 
											name="$key" 
											id="$key" 
											value="$val" 
											class="regular-text" />
						<br />
						<span class="description"></span>
					</td>
				</tr>
HTML;
			}
			echo '</table>';
		}


		function is_assoc($arr) {
			return array_keys($arr) !== range(0, count($arr) - 1);
		}

		private function getCustomFields() {

			$fields = array(
				'mobile_phone' => array("type" => "text",
				                        "title" => __("Mobile Phone", $this->textdomain),
																"validation" => "0[257]\d{8}"),
				'gender' => array("type" => "select",
				                  "title" => __("Gender", $this->textdomain),
				                  "values" => array("male", "female")),
				'age' => array("type" => "text",
				               "title" => __("Age", $this->textdomain),
				               "validation" => "\d+" ),
				'height' => array("type" => "text",
				                  "title" => __("Height", $this->textdomain),
				                  "validation" => "\d+" ),
				'weight' => array("type" => "text",
				                  "title" => __("Weight", $this->textdomain),
				                  "validation" => "\d+" ),

				'activity' => array("type" => "select",
				                  "title" => __("Activity", $this->textdomain),
				                    ),
			);
			return $fields;
		}

	}

	$smappUserCustomData = new smappUserCustomData();
}


//נייד
//----נתונים אישיים
//מין
//גיל
//גובה
//משקל
//----נתוני פעילות
//ספורט
//ללא פעילות
//פעילות נמוכה
//פעילות בינונית
//פעילות גבוהה
//יעד
//לרדת במשקל
//לשמור על המשקל
//סדנאות
//התמודדות עם מצבי לחץ
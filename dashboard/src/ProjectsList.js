import React from 'react';
import { db } from './firebase';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableFooter,
  Paper,
  Typography,
  TableRow
} from '@material-ui/core';
import { Android, Publish } from '@material-ui/icons';
import { Wrapper, Center } from './components';
import moment from 'moment';
function parseApplicationData(data) {
  return data;
}

export class ProjectsList extends React.Component {
  state = {
    applications: []
  };
  componentDidMount() {
    db.collection('applications').onSnapshot(apps => {
      let applications = [];
      apps.forEach(app =>
        applications.push({
          id: app.id,
          data: parseApplicationData(app.data())
        })
      );
      this.setState({ applications });
    });
  }
  render() {
    return (
      <Wrapper>
        <Center>
          <Typography variant="headline">Kokoapps</Typography>
          <Paper>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell>App name</TableCell>
                  <TableCell>Last updated</TableCell>
                  <TableCell>Version</TableCell>
                  <TableCell>Staging</TableCell>
                  <TableCell>Production</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.applications.map(item => {
                  const manifest =
                    item.data && item.data.manifest
                      ? JSON.parse(item.data.manifest)
                      : {};
                  const last_updated =
                    item.data && item.data.last_updated
                      ? moment(item.data.last_updated.toDate()).format(
                          'MMM Do YYYY [at] HH:mm'
                        )
                      : '----';
                  return (
                    <TableRow
                      key={item.id}
                      hover
                      onClick={() =>
                        this.props.history.push(
                          `${this.props.match.url}/${item.id}`
                        )
                      }>
                      <TableCell>
                        <Android />
                      </TableCell>
                      <TableCell>{item.data.name}</TableCell>
                      <TableCell>{last_updated}</TableCell>
                      <TableCell>{manifest.version}</TableCell>
                      <TableCell>{<Publish />}</TableCell>
                      <TableCell>{<Publish />}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
              <TableFooter />
            </Table>
          </Paper>
        </Center>
      </Wrapper>
    );
  }
}

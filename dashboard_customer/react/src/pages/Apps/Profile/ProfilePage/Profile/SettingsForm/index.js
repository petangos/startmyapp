import React from 'react';
//import { push } from 'react-router-redux'
import { Form, Input, Button, notification } from 'antd';
import { auth, db } from 'config/firebase/firebase';
const FormItem = Form.Item;
//const InputGroup = Input.Group;

@Form.create()
class SettingsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      displayName: props.data.displayName
    };
  }

  onSubmit = (isSubmitForm: ?boolean) => event => {
    event.preventDefault();
    const { form } = this.props;
    if (!isSubmitForm) {
      form.validateFields((error, values) => {
        if (!error) {
          try {
            auth.currentUser
              .updateProfile({
                displayName: values.displayName,
                photoURL: 'resources/images/avatars/1.jpg'
              })
              .then(function() {})
              .catch(function(error) {
                notification.open({
                  type: 'error',
                  message:
                    '(updateProfile) Unfortunately, system has stopped. contact support for further information !'
                });
              });
          } catch (e) {
            notification.open({
              type: 'error',
              message:
                'Unfortunately, system has stopped. contact support for further information !'
            });
          }

          try {
            var docData = {
              phone: values.phone,
              address: values.address
            };
            db.collection('users')
              .doc(auth.currentUser.uid)
              .set(docData)
              .then(function() {
                notification.open({
                  type: 'success',
                  message: 'You have successfully saved form!'
                });
              });
          } catch (e) {
            notification.open({
              type: 'error',
              message:
                'Unfortunately, system has stopped. contact support for further information !'
            });
          }
        }
      });
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { isSubmitForm } = this.props;

    return (
      <Form onSubmit={this.onSubmit(isSubmitForm)} className="login-form">
        <h5 className="text-black mt-4">
          <strong>Personal Information</strong>
        </h5>
        <div className="row">
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Fullname</label>
              {getFieldDecorator('displayName', {
                rules: [
                  { required: true, message: 'Please input your fullname' }
                ],
                initialValue: this.props.data.displayName
              })(<Input placeholder="Fullname" />)}
            </FormItem>
          </div>
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Email</label>
              <br />
              {this.props.data.email}
            </FormItem>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Phone</label>
              {getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please input your phone' }],
                initialValue: this.props.data.phone
              })(<Input placeholder="Phone" />)}
            </FormItem>
          </div>
          <div className="col-lg-6">
            <FormItem>
              <label className="form-label mb-0">Address</label>
              {getFieldDecorator('address', {
                rules: [
                  { required: true, message: 'Please input your address' }
                ],
                initialValue: this.props.data.address
              })(<Input placeholder="Address" />)}
            </FormItem>
          </div>
        </div>
        <div className="form-actions">
          <Button
            style={{ width: 150 }}
            type="primary"
            htmlType="submit"
            className="mr-3">
            Submit
          </Button>
          {/** <Button htmlType="submit">Cancel</Button>*/}
        </div>
      </Form>
    );
  }
}

export default SettingsForm;

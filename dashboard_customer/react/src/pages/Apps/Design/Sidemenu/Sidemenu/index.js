import React from "react";
import IconForm from "./IconForm";
import LoadingForm from "./LoadingForm";
import "./style.scss";
import { Tabs, Icon } from "antd";

const TabPane = Tabs.TabPane;

class Branding extends React.Component {
  render() {
    return (
      <section className="card">
        <div className="card-header mb-2">
          <div className="utils__title">
            <h1>Design</h1>
            <strong>
              Update your app color scheme, app icon and splash screens.
            </strong>
          </div>
        </div>
        <div className="card-body">
          <div className="add-post">
            <p>
              Note: If your app has already been published, changing the App
              Name, App Icon, or Loading Screens will require a resubmission to
              the app stores for the changes to take effect.
            </p>

            <Tabs defaultActiveKey="1">
              <TabPane
                tab={
                  <span>
                    <Icon type="mobile" />
                    App icon & Display name
                  </span>
                }
                key="1"
              >
                <IconForm />
              </TabPane>
              <TabPane
                tab={
                  <span>
                    <Icon type="loading-3-quarters" />
                    App loading screens 
                  </span>
                }
                key="2"
              >
                <LoadingForm />
              </TabPane>
            </Tabs>
          </div>
        </div>
      </section>
    );
  }
}

export default Branding;

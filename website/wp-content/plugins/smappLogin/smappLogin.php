<?php

/*
  Plugin Name: SMApp Login
  Description: Adjustment Login Page
  Author: Koko-Apps
  Version: 1.0.1
 */
if (!defined('ABSPATH')) {
    exit;
}


if (!class_exists('SMAppLogin')) {

    class SMAppLogin {

        public function __construct() {
            if (!isset($_SERVER["SCRIPT_NAME"]))
                return;

            if ($_SERVER["SCRIPT_NAME"] != '/wp-login.php')
                return;

            add_action('login_head', array(&$this, 'login_head'), 200);
            add_action('login_redirect', array(&$this, 'login_redirect'), 200);
            add_action('login_headerurl', array(&$this, 'login_headerurl'), 200);
        }

        public function login_head() {
            $version = '2.9';

            wp_enqueue_style('smappLogin_css', plugins_url('/smappLogin/css/smappLogin.css'), array(), $version);
            wp_register_script('smappLogin_js', plugins_url('/smappLogin/js/smappLogin.js'), array('jquery'), $version);

            $_message = __("We have just sent you an email with a password change link.<br>If you can't locate it, please check your Spam or Bulk email folder in case the email got delivered there instead of your inbox.", "smappLogin");

            if (isset($_GET['action']) && $_GET['action'] == "resetpass")
                $_message = '';

            wp_localize_script('smappLogin_js', '_message', $_message);

            wp_enqueue_script('smappLogin_js');

            if (isset($_GET['checkemail']) && $_GET['checkemail'] == "confirm" ||
                    (
                    isset($_GET['action']) &&
                    $_GET['action'] == "resetpass" &&
                    isset($_POST['pass1']) &&
                    isset($_POST['pass2']) &&
                    ($_POST['pass1'] == $_POST['pass2'])
                    )) {

                wp_enqueue_style('smappLogin_checkemail_css', plugins_url('/smappLogin/css/smappLogin_checkemail.css'), array(), $version);
                wp_enqueue_script('smappLogin_checkemail_js', plugins_url('/smappLogin/js/smappLogin_checkemail.js'), array(), $version);

                wp_enqueue_style('smappLogin_message_css', plugins_url('/smappLogin/css/smappLogin_message.css'), array(), $version);

                do_action('smappLogin_themes_css');
            }
            nocache_headers();

            echo '<meta name="viewport" content="initial-scale=1, maximum-scale=1">';
        }

        public function login_redirect($url) {
            global $current_user;
            $url = '/wp-admin/edit.php?post_type=shop_order';
            // is there a user ?
            if (is_array($current_user->roles)) {
                if (is_admin()) {
                    $url = '';
                }
                return $url;
            }
        }

        public function smapp_onlogin_redirect($redirect, $user) {
            // Get the first of all the roles assigned to the user
            $role = $user->roles[0];
            $dashboard = admin_url();
            $shop = get_permalink(wc_get_page_id('shop'));
            switch ($role) {
                case 'administrator':
                    $redirect = $dashboard;
                    break;
                case 'shop-manager':
                    $redirect = $shop;
                    break;
                default:
                    //Redirect any other role to the previous visited page or, if not available, to the home
                    $redirect = wp_get_referer() ? wp_get_referer() : home_url();
            }
            return $redirect;
        }

        public function login_headerurl($url) {
            return 'https://' . $_SERVER["HTTP_HOST"];
        }

        public function install() {
            //check if upload/themes folder exists
            $upload_dir = wp_upload_dir();
            $folder = $upload_dir['basedir'] . '/themes';
            if (wp_mkdir_p($folder)) {
                //
            }
        }

    }

    $SMAppLogin = new SMAppLogin();

    register_activation_hook(__FILE__, array('SMAppLogin', 'install'));
}
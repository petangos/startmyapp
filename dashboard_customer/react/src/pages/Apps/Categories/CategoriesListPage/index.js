import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import CategoriesList from './CategoriesList/';

class CategoriesListPage extends React.Component {
  static defaultProps = {
    pathName: 'Categories List',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;

    return (
      <Page {...props}>
        <Helmet title={`Categories List`} />
        <CategoriesList category={this.props.match.params.category} />
      </Page>
    );
  }
}

export default CategoriesListPage;

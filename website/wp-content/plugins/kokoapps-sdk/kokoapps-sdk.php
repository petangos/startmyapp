<?php
/*
 * Plugin Name: KokoApps SDK
 * Plugin URI: https://www.kokoapps.co.il/SDK/
 * Description: Implmenets KokoApps SDK into Wordpress
 * Author: KokoApps
 * Version: 1.0.1
 * Text Domain: kokoapps
 * Domain Path: /languages/
 * 
 * @package KokoAppsSDK
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!defined('KOKOAPPS_PLUGIN_FILE')) {
    define('KOKOAPPS_PLUGIN_FILE', __FILE__);
}

if (!class_exists('KokoAppsAuthenticateApi')) {
    include_once dirname(__FILE__) . '/includes/KokoAppsToken.php';
    include_once dirname(__FILE__) . '/includes/KokoAppsLogin.php';
    include_once dirname(__FILE__) . '/includes/KokoAppsFacebookLogin.php';
    include_once dirname(__FILE__) . '/includes/KokoAppsAuthenticateApi.php';

    $KokoAppsexpiration = 5; //H
    $KokoApps['AuthenticateApi'] = new KokoAppsAuthenticateApi();
}


if (!class_exists('KokoAppsBanners')) {
    include_once dirname(__FILE__) . '/includes/Banners/KokoAppsBannersHelp.php';
    include_once dirname(__FILE__) . '/includes/Banners/KokoAppsBanners.php';
    include_once dirname(__FILE__) . '/includes/Banners/KokoAppsBannersApi.php';
    $KokoApps['BannersPostType'] = new KokoAppsBanners();
    $KokoApps['BannersApi'] = new KokoAppsBannersApi();
}


if (!class_exists('KokoAppsPayment')) {
    include_once dirname(__FILE__) . '/includes/Payment/KokoAppsPayment.php';
    $KokoApps['PaymentApi'] = new KokoAppsPayment();
}
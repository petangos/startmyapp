#!/usr/bin/env node
const fs = require('fs-extra');
const path = require('path');
const program = require('commander');

async function save(srcDir, destDir) {
  try {
    await fs.copy(
      path.join(srcDir, 'koko-manifest.json'),
      path.join(destDir, 'koko-manifest.json')
    );
    await fs.copy(
      path.join(srcDir, 'app.json'),
      path.join(destDir, 'app.json')
    );
    await fs.copy(
      path.join(srcDir, 'google-services.json'),
      path.join(destDir, 'google-services.json')
    );
    await fs.remove(path.join(destDir, 'assets'));
    await fs.copy(path.join(srcDir, 'assets'), path.join(destDir, 'assets'));
    return Promise.resolve();
  } catch (e) {
    console.log(e);
    return Promise.reject();
  }
}

async function kokoSwitch(incoming_project_name, cwd) {
  if (!incoming_project_name) {
    console.log('project name is required');
    return;
  }

  const active_directory = path.join(cwd, 'store');

  /* console.log(`saving ${active_project_name}'s resources...`);
  try {
    await save(
      active_directory,
      path.join(cwd, 'store_package', active_project_name)
    );
    console.log('resources saved successfuly.');
  } catch (e) {
    console.log('failed to save resources.');
    return;
  } */
  console.log('replacing to new project recourses...');
  try {
    await save(
      path.join(cwd, 'store_package', incoming_project_name),
      active_directory
    );
    console.log('resources replaced successfuly.');
    return Promise.resolve();
  } catch (e) {
    console.log('failed to replace resources.');
    return Promise.reject();
  }
}

module.exports = { kokoSwitch };

import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import Calendar from './Calendar/';

class CalendarPage extends React.Component {
  static defaultProps = {
    pathName: 'Posts List',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    const postType = props.match.path.slice(1);
    return (
      <Page {...props}>
        <Helmet title={`${postType} List`} />
        <Calendar postType={postType} />
      </Page>
    );
  }
}

export default CalendarPage;

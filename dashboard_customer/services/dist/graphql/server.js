"use strict";

const _require = require('apollo-server-express'),
      ApolloServer = _require.ApolloServer,
      gql = _require.gql,
      AuthenticationError = _require.AuthenticationError;

const _require2 = require('jsonwebtoken'),
      sign = _require2.sign,
      verify = _require2.verify;

const fs = require('fs-extra');

const path = require('path');

const _require3 = require('../config'),
      initializeApp = _require3.initializeApp,
      koko = _require3.koko,
      client = _require3.client;

const admin = require('firebase-admin');

function createServer() {
  const typeDefs = require('./schema');

  const resolvers = require('./resolvers');

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({
      req
    }) => {
      const token = req.headers.authorization;
      const appToken = req.headers['x-application-authorization'];

      if (!appToken) {
        throw new AuthenticationError('Application not authorized');
        return null;
      }

      const appId = verify(appToken, 'SECRETHASH');
      const jsonFileName = `${appId}.json`;
      const jsonPath = path.join(process.cwd(), '.secrets', jsonFileName);

      try {
        if (fs.existsSync(jsonPath)) {
          const appSecrets = fs.readJsonSync(jsonPath);
          initializeApp(appSecrets, 'client');
        } else {
          const appSecrets = await admin.firestore().collection('applications').where('name', '==', appId).get().then(res => res.docs.map(item => item.data().firebase_secrets)).then(res => res[0]);
          fs.writeJSONSync(jsonPath, appSecrets);
          initializeApp(appSecrets);
        }
      } catch (e) {
        console.log(e);

        if (admin.apps.length < 2) {
          const appSecrets = await admin.firestore().collection('applications').where('name', '==', appId).get().then(res => res.docs.map(item => item.data().firebase_secrets)).then(res => res[0]);
          fs.writeJSONSync(jsonPath, appSecrets);
          initializeApp(appSecrets);
        }
      }

      let uid;

      try {
        const client = admin.apps[1];
        const verified = await client.auth().verifyIdToken(token);
        uid = verified.uid;
      } catch (e) {
        console.log('catch', e);
        uid = '';
      } //console.log(token, '\n\n');
      //console.log(appToken, '\n\n');


      return {
        uid,
        appId
      };
    }
  });
  return server;
}

module.exports = createServer;
import React from 'react'
import Page from '../../../../components/LayoutComponents/Page'
import { translate } from 'react-i18next'
import Helmet from 'react-helmet'
import ArticlesList from './ArticlesList/'

class ArticlesListPage extends React.Component {
  static defaultProps = {
    pathName: 'Product List',
    roles: ['agent', 'administrator'],
  }

  render() {
    const { t } = this.props
    const props = this.props
    return (
      <Page {...props}>
        <Helmet title={t('Article List')} />
        <ArticlesList />
      </Page>
    )
  }
}

export default translate()(ArticlesListPage)

<?php

class KokoAppsFacebookLogin {

    public static function Login($request, $expiration) {

        if (!empty($request['kokoapps_access_token'])) {
            $args = array(
                'meta_key' => 'kokoapps_fb_access_token',
                'meta_value' => $request['kokoapps_access_token']
            );
            $user_query = new WP_User_Query($args);
            $users = $user_query->get_results();
            if (!empty($users)) {
                $user = $user_query->get_results()[0]->data;
                $result['action'] = 'existing user';
                $result['token'] = KokoAppsToken::set($user->data->ID, $expiration);
                $result = KokoAppsLogin::GetInfo($user->data, $result['token']);
                return new WP_REST_Response($result);
            } else {
                $token = $request['kokoapps_access_token'];

                //search email in graph
                $url = 'https://graph.facebook.com/me?';
                $params = array(
                    'access_token' => $token,
                    'fields' => 'id,first_name,last_name,name,email,picture'
                );
                $url .= http_build_query($params);
                $fbdata = (array) json_decode(file_get_contents($url));
                if (!isset($fbdata['id']) || !isset($fbdata['email'])) {
                    return wp_send_json(new WP_Error('user', 'User facebook', 'Facebook Login failed'), 401);
                } else {
                    $user = get_user_by('email', $fbdata['email']);
                    if (!$user) {
                        // Create an username
                        $username = sanitize_user(str_replace(' ', '_', strtolower($fbdata['name'])));
                        // Creating our user
                        $user_id = wp_create_user($username, wp_generate_password(), $fbdata['email']);
                        if (is_wp_error($user_id)) {
                            return wp_send_json(new WP_Error('user', 'User facebook', 'Facebook User Registration failed'), 401);
                        } else {
                            $user = new WP_User($user_id);
                        }
                        if ($user) {
                            update_user_meta($user->ID, 'first_name', $fbdata['first_name']);
                            update_user_meta($user->ID, 'last_name', $fbdata['last_name']);
                            update_user_meta($user->ID, 'facebook_id', $fbdata['id']);
                            update_user_meta($user->ID, 'picture', $fbdata['picture']->data->url);
                            $result['action'] = 'new user by facebook email';
                            $result['avatar'] = $fbdata['picture']->data->url;
                            $result['token'] = KokoAppsToken::set($user->ID, $expiration);
                            $result = KokoAppsLogin::GetInfo($user, $result['token']);
                            return new WP_REST_Response($result);
                        } else {
                            return wp_send_json(new WP_Error('user', 'User facebook', 'Facebook User Registration failed'), 401);
                        }
                    } else {
                        $result['action'] = 'existing user by email';
                        $result['token'] = KokoAppsToken::set($user->ID, $expiration);
                        $result = KokoAppsLogin::GetInfo($user, $result['token']);
                        return new WP_REST_Response($result);
                    }
                }

                return wp_send_json(new WP_Error('user', 'User facebook', 'Access token'), 401);
            }
        } else {
            return wp_send_json(new WP_Error('user', 'User facebook', 'Access token'), 401);
        }
    }

}

import React from 'react';
//import { push } from 'react-router-redux'
//const InputGroup = Input.Group;

const Field = ({ title, value }) => (
  <span>
    <b>{title}:</b> {value}
  </span>
);

class Info extends React.Component {
  render() {
    const {
      full_name,
      email,
      phone,
      gender,
      age,
      height,
      weight,
      activity,
      target
    } = this.props.data;
    return (
      <React.Fragment>
        <h5 className="text-black mt-4">
          <strong>Personal Information</strong>
        </h5>
        <div className="row" style={{ marginBottom: 10 }}>
          <div className="col-lg-6">
            <Field title="Full Name" value={full_name} />
          </div>
          <div className="col-lg-6">
            <Field title="Email" value={email} />
          </div>
        </div>
        <div className="row" style={{ marginBottom: 10 }}>
          <div className="col-lg-6">
            <Field title="Phone" value={phone} />
          </div>
          <div className="col-lg-6">
            <Field title="Age" value={age} />
          </div>
        </div>
        <div className="row" style={{ marginBottom: 10 }}>
          <div className="col-lg-6">
            <Field title="Gender" value={gender} />
          </div>
        </div>
        <hr />
        <div className="row" style={{ marginBottom: 10 }}>
          <div className="col-lg-6">
            <Field title="Height" value={height} />
          </div>
          <div className="col-lg-6">
            <Field title="Weight" value={weight} />
          </div>
        </div>
        <div className="row" style={{ marginBottom: 10 }}>
          <div className="col-lg-6">
            <Field title="Activity Level" value={activity} />
          </div>
          <div className="col-lg-6">
            <Field title="Goal" value={target} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Info;

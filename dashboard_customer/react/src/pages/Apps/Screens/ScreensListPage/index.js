import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import ScreensList from './ScreensList/';

class ScreenListPage extends React.Component {
  static defaultProps = {
    pathName: 'Posts List',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;

    return (
      <Page {...props}>
        <Helmet title={`Screens List`} />
        <ScreensList />
      </Page>
    );
  }
}

export default ScreenListPage;

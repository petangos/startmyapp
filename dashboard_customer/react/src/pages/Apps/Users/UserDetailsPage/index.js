import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import UserDetails from './UserDetails';

class UserDetailsPage extends React.Component {
  static defaultProps = {
    pathName: 'Users Details',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    return (
      <Page {...props}>
        <Helmet title="Users Details" />
        <UserDetails uid={this.props.match.params.uid} />
      </Page>
    );
  }
}

export default UserDetailsPage;

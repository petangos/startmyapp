// import React from 'react'
import React from 'react';

import moment from 'moment';
import { withRouter } from 'react-router-dom';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Form, Input, Button, Upload, Icon } from 'antd';
import { withFirebase } from '../../../../../../ducks/firebase';

//import './wysiwyg-editor.css'
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
const FormItem = Form.Item;

@withRouter
@withFirebase
@Form.create()
class AddForm extends React.Component {
  state = {
    data: {}
  };

  constructor(props) {
    super(props);
    this.uploadImage = this.uploadImage.bind(this);
  }

  uploadImage(img) {
    return new Promise((resolve, reject) => {
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(reject)
        .then(snap => {
          snap.ref
            .getDownloadURL()
            .then(url => resolve({ data: { link: url } }));
        });
    });
  }

  uploadMainImage(img) {
    return new Promise((resolve, reject) => {
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(reject)
        .then(snap => {
          snap.ref.getDownloadURL().then(url => resolve(url));
        });
    });
  }

  async componentDidMount() {
    if (this.props.catId)
      try {
        const data = await this.getCategory();
        this.setState({
          data
        });
      } catch (e) {
        console.log(e);
      }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.setState({ data: { media: [] } });
      if (this.props.catId)
        try {
          const data = await this.getCategory();
          this.setState({
            data
          });
        } catch (e) {
          console.log(e);
        }
    }
  }
  onSubmit(event) {
    event.preventDefault();

    const { form } = this.props;

    form.validateFields((error, { media, ...rest }) => {
      const docValues = {
        ...rest,
        last_modified: firebase.firestore.Timestamp.now()
      };

      if (media) {
        docValues.media = [{ type: 'image', src: media }];
      }
      docValues.post_type = this.props.postType;
      if (!docValues.name) {
        docValues.name = docValues.label;
      }
      const client = this.props.firebase.client;
      const collection = client.firestore().collection('categories');
      const doc = this.props.catId
        ? collection.doc(this.props.catId)
        : collection.doc();

      doc.set(docValues, { merge: true }).then(this.props.history.goBack);
    });
  }

  async getCategory() {
    const client = this.props.firebase.client;
    const data = await client
      .firestore()
      .collection('categories')
      .doc(this.props.catId)
      .get()
      .then(res => ({
        ...res.data(),
        media:
          res.data().media && res.data().media.length
            ? res.data().media[0].src
            : ''
      }));
    return data;
  }
  render() {
    const { form } = this.props;
    return (
      <Form className="add-post__form mt-3" onSubmit={e => this.onSubmit(e)}>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>Title</strong>
            </label>
            {form.getFieldDecorator('label', {
              initialValue: this.state.data.label,
              rules: [{ required: true, message: 'input required' }]
            })(<Input placeholder="Category title" />)}
          </div>
        </FormItem>

        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>Slug</strong>
            </label>
            {form.getFieldDecorator('name', {
              initialValue: this.state.data.name
            })(<Input placeholder="Post description" />)}
          </div>
        </FormItem>

        {/*        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>Parent</strong>
            </label>
            {form.getFieldDecorator('parent', {
              initialValue: this.state.data.parent,
              rules: [{ required: true, message: 'input required' }]
            })(<Input placeholder="Parent category" />)}
          </div>
        </FormItem> */}

        <FormItem>
          {form.getFieldDecorator('media')(
            <div className="form-group">
              <label className="add-post__label">
                <strong>Post Image</strong>
              </label>

              <Upload
                multiple={false}
                name="media"
                onChange={({ file }) => {
                  if (file.status === 'done') {
                    this.setState({
                      data: { ...this.state.data, media: file.response }
                    });
                    this.props.form.setFieldsValue({ media: file.response });
                  }
                }}
                accept={'image/jpg,image/png'}
                customRequest={({ file, onSuccess }) =>
                  this.uploadMainImage(file).then(url => onSuccess(url))
                }
                action={''}>
                {this.state.data.media ? (
                  <img
                    alt=""
                    src={this.state.data.media}
                    style={{ maxWidth: 250, maxHeight: 200 }}
                  />
                ) : (
                  <Button>
                    <Icon type="upload" /> Click to Upload
                  </Button>
                )}
              </Upload>
            </div>
          )}
        </FormItem>

        <FormItem>
          <div className="add-post__submit">
            <span className="mr-3">
              <Button
                type="primary"
                htmlType="submit"
                disabled={!this.props.form.isFieldsTouched()}>
                Save
              </Button>
            </span>
            {/*  <Button type="danger" htmlType="submit">
              Discard
            </Button> */}
          </div>
        </FormItem>
      </Form>
    );
  }
}

export default AddForm;

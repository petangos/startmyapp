const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fsFcm = require('./fsFcm');
const httpFs = require('./httpFs');

admin.initializeApp();

console.log(admin.app().options.projectId);

admin.firestore().settings({timestampsInSnapshots: true});

exports.sendPush = fsFcm(functions, admin);

exports.smappPush = httpFs(functions, admin);
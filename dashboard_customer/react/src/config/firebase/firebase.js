import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

var firebaseConfig = {
  apiKey: 'AIzaSyA1SFTlDGjbN9FiydWkDnPLq4QeC99nYrk',
  authDomain: 'koko-apps.firebaseapp.com',
  databaseURL: 'https://koko-apps.firebaseio.com',
  projectId: 'koko-apps',
  storageBucket: 'koko-apps.appspot.com',
  messagingSenderId: '90002079257'
};
if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

// Initialize Cloud Firestore through Firebase
export const db = firebase.firestore();
// Disable deprecated features
db.settings({
  timestampsInSnapshots: true
});

export const auth = firebase.auth();
export const now = firebase.firestore.Timestamp.now;
export const config = firebaseConfig;

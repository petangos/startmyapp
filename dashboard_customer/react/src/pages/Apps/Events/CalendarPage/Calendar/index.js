import React from 'react';
import {
  Input,
  Button,
  Switch,
  InputNumber,
  Upload,
  Calendar,
  Select,
  Modal,
  Badge,
  Popconfirm,
  Icon,
  Row,
  Form,
  TimePicker,
  Col
} from 'antd';
import './style.scss';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import moment from 'moment';
import { withFirebase } from '../../../../../ducks/firebase';

@withFirebase
@Form.create()
export default class EventList extends React.Component {
  unsubscribe = () => null;
  state = {
    visible: false,
    selected: moment(),
    data: {},
    isImageChanged: false,
    confirmLoading: false,
    events: [],
    mode: ''
  };

  componentDidMount() {
    this.getEvents();
  }

  componentDidUpdate(prevProps) {
    if (this.props.firebase.client.name !== prevProps.firebase.client.name) {
      this.unsubscribe();
      this.getEvents();
    }
  }

  uploadMainImage(img) {
    return new Promise((resolve, reject) => {
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(reject)
        .then(snap => {
          snap.ref.getDownloadURL().then(url => resolve(url));
        });
    });
  }

  getEvents() {
    this.unsubscribe = this.props.firebase.client
      .firestore()
      .collection('events')
      .onSnapshot(snap => {
        const events = snap.docs.map(doc => ({
          id: doc.id,
          ...doc.data(),
          media:
            doc.data().media && doc.data().media.length
              ? doc.data().media[0].src
              : '',
          start_time: doc.data().start_time
            ? moment(doc.data().start_time.toDate())
            : null,
          end_time: doc.data().end_time
            ? moment(doc.data().end_time.toDate())
            : null
        }));
        this.setState({ events });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  async onDelete(event, bulk) {
    if (!bulk) {
      const eventDoc = await this.props.firebase.client
        .firestore()
        .collection('events')
        .doc(this.state.data.id);
      eventDoc.delete().then(() => Promise.resolve());
    } else {
      const eventsToDelete = await this.props.firebase.client
        .firestore()
        .collection('events')
        .where('template', '==', this.state.data.template)
        .get();

      await Promise.all(eventsToDelete.docs.map(event => event.ref.delete()));
      return Promise.resolve();
    }
  }

  onSubmit(event, bulk) {
    event.preventDefault();
    this.setState({ confirmLoading: true });
    const { form } = this.props;

    form.validateFields(
      (
        error,
        {
          hasCapacity,
          image,
          start_time,
          end_time,
          media,
          repetitions = 1,
          ...rest
        }
      ) => {
        return new Promise(async (resolve, reject) => {
          if (error) reject();

          const eventTemplatesCollection = this.props.firebase.client
            .firestore()
            .collection('event_templates');

          const eventTemplateDoc =
            this.state.mode === 'create'
              ? eventTemplatesCollection.doc()
              : this.state.data.template;

          const docValues = { ...rest };
          if (!media) {
            delete docValues['media'];
          } else {
            docValues.media = [{ type: 'image', src: media }];
          }

          eventTemplateDoc.set(
            {
              ...docValues
            },
            { merge: true }
          );

          if (this.state.mode === 'edit' && bulk) {
            const eventsToEdit = await this.props.firebase.client
              .firestore()
              .collection('events')
              .where('template', '==', eventTemplateDoc)
              .get();

            eventsToEdit.docs.forEach(eventDoc => {
              const start = moment(eventDoc.data().start_time.toDate()).set({
                hour: start_time ? start_time.hours() : null,
                minute: start_time ? start_time.minutes() : null,
                seconds: 0
              });
              const end = end_time
                ? moment(
                    eventDoc.data().end_time
                      ? eventDoc.data().end_time.toDate()
                      : undefined
                  ).set({
                    hour: end_time ? end_time.hours() : null,
                    minute: end_time ? end_time.minutes() : null,
                    seconds: 0
                  })
                : null;
              eventDoc.ref.set(
                {
                  ...docValues,
                  start_time: firebase.firestore.Timestamp.fromDate(
                    start.toDate()
                  ),
                  end_time: end
                    ? firebase.firestore.Timestamp.fromDate(end.toDate())
                    : null
                },
                { merge: true }
              );
            });
          } else {
            const eventsCollection = this.props.firebase.client
              .firestore()
              .collection('events');
            for (let offset = 0; offset < repetitions; offset++) {
              const offestJump = docValues.recurrency;

              const start = moment(this.state.selected)
                .add(offset, offestJump)
                .set({
                  hour: start_time ? start_time.hours() : null,
                  minute: start_time ? start_time.minutes() : null,
                  seconds: 0
                });
              const end = end_time
                ? moment(this.state.selected)
                    .add(offset, offestJump)
                    .set({
                      hour: end_time ? end_time.hours() : null,
                      minute: end_time ? end_time.minutes() : null,
                      seconds: 0
                    })
                : null;

              const eventDoc =
                this.state.mode === 'create'
                  ? eventsCollection.doc()
                  : eventsCollection.doc(this.state.data.id);

              if (!docValues.media && this.state.mode === 'create')
                docValues.media = [];
              eventDoc.set(
                {
                  template: eventTemplateDoc,
                  ...docValues,
                  start_time: firebase.firestore.Timestamp.fromDate(
                    start.toDate()
                  ),
                  end_time: end
                    ? firebase.firestore.Timestamp.fromDate(end.toDate())
                    : null,
                  participants: []
                },
                { merge: true }
              );
            }
          }

          this.setState({ visible: false, data: {}, isImageChanged: false });
          this.props.form.resetFields();
          this.setState({ confirmLoading: false });
          resolve();
        });
      }
    );
  }

  render() {
    const { form } = this.props;
    return (
      <React.Fragment>
        <div style={{ backgroundColor: 'white' }}>
          <Calendar
            dateCellRender={date => {
              const events = this.state.events.filter(item =>
                item.start_time.isSame(date, 'day')
              );
              return (
                <ul
                  style={{
                    listStyle: 'none',
                    margin: 0,
                    padding: 0,
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    width: '100%',
                    textOverflow: 'ellipsis'
                  }}>
                  {events.map(event => (
                    <li key={event.title} style={{}}>
                      <Badge
                        status="success"
                        text={event.title}
                        onClick={() => {
                          this.setState({
                            visible: true,
                            selected: date,
                            mode: 'edit',
                            data: { ...event }
                          });
                        }}
                      />
                    </li>
                  ))}
                </ul>
              );
            }}
            onSelect={date => {
              if (
                this.state.events.filter(item =>
                  item.start_time.isSame(date, 'day')
                ).length === 0
              )
                this.setState({
                  visible: true,
                  selected: date,
                  data: {},
                  mode: 'create'
                });
            }}
          />
        </div>
        <Modal
          width={'65%'}
          centered
          footer={
            <div>
              {this.state.mode === 'edit' && (
                <React.Fragment>
                  <Popconfirm
                    title="Are you sure delete?"
                    onConfirm={e => {
                      this.onDelete(e, true).then(() => {
                        this.setState({
                          visible: false,
                          data: {},
                          isImageChanged: false
                        });
                        this.props.form.resetFields();
                      });
                    }}
                    onCancel={() => null}
                    okText="Yes"
                    cancelText="No">
                    <Button type="danger">Delete</Button>
                  </Popconfirm>

                  {this.state.data.recurrency !== 'once' && (
                    <Popconfirm
                      title="Are you sure delete?"
                      onConfirm={e => {
                        this.onDelete(e, true).then(() => {
                          this.setState({
                            visible: false,
                            data: {},
                            isImageChanged: false
                          });
                          this.props.form.resetFields();
                        });
                      }}
                      onCancel={() => null}
                      okText="Yes"
                      cancelText="No">
                      <Button type="danger">Delete All</Button>
                    </Popconfirm>
                  )}
                </React.Fragment>
              )}
              <Button
                onClick={() => {
                  this.setState({
                    data: {},
                    visible: false,
                    isImageChanged: false
                  });
                  this.props.form.resetFields();
                }}>
                Cancel
              </Button>
              <Button
                type="primary"
                disabled={
                  !this.props.form.isFieldsTouched() &&
                  !this.state.isImageChanged
                }
                onClick={this.onSubmit.bind(this)}>
                Save
              </Button>
              {this.state.mode !== 'create' &&
                this.state.data.recurrency !== 'once' && (
                  <Button type="primary" onClick={e => this.onSubmit(e, true)}>
                    Save All
                  </Button>
                )}
            </div>
          }
          title={`Create Event - ${moment(this.state.selected).format(
            'DD-MM-YYYY'
          )}`}
          onCancel={() =>
            this.setState({
              visible: false,
              data: {},
              isImageChanged: false
            })
          }
          visible={this.state.visible}>
          <Form.Item>
            <div className="form-group">
              <label className="add-post__label">
                <strong>Title</strong>
              </label>
              {form.getFieldDecorator('title', {
                initialValue: this.state.data.title,
                rules: [{ required: true, message: 'input required' }]
              })(<Input placeholder="Event Title" />)}
            </div>
          </Form.Item>
          <Form.Item>
            <div className="form-group">
              <label className="add-post__label">
                <strong>Description</strong>
              </label>
              {form.getFieldDecorator('description', {
                initialValue: this.state.data.description || '',
                rules: []
              })(<Input placeholder="Description" />)}
            </div>
          </Form.Item>
          <Row>
            <Col span={4}>
              <Form.Item>
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Full Day</strong>
                  </label>
                  {form.getFieldDecorator('full_day', {
                    initialValue: this.state.data.full_day || false,
                    valuePropName: 'checked',
                    rules: []
                  })(<Switch checkedChildren="full day" />)}
                </div>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item>
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Start Time</strong>
                  </label>
                  {form.getFieldDecorator('start_time', {
                    initialValue: this.state.data.start_time,
                    rules: []
                  })(
                    <TimePicker
                      minuteStep={5}
                      format="HH:mm"
                      disabled={form.getFieldValue('full_day')}
                    />
                  )}
                </div>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item>
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>End Time</strong>
                  </label>
                  {form.getFieldDecorator('end_time', {
                    initialValue: this.state.data.end_time,
                    rules: []
                  })(
                    <TimePicker
                      minuteStep={5}
                      format="HH:mm"
                      disabled={form.getFieldValue('full_day')}
                    />
                  )}
                </div>
              </Form.Item>
            </Col>
            {this.state.mode === 'create' && (
              <React.Fragment>
                <Col span={4}>
                  <Form.Item>
                    <div className="form-group">
                      <label className="add-post__label">
                        <strong>Recurrency</strong>
                      </label>
                      {form.getFieldDecorator('recurrency', {
                        initialValue: this.state.data.recurrency || 'once',
                        rules: []
                      })(
                        <Select>
                          <Select.Option value="once">Only Once</Select.Option>
                          <Select.Option value="day">Every Day</Select.Option>
                          <Select.Option value="week">Every Week</Select.Option>
                          <Select.Option value="momth">
                            Every Month
                          </Select.Option>
                          <Select.Option value="year">Every Year</Select.Option>
                        </Select>
                      )}
                    </div>
                  </Form.Item>
                </Col>

                <Col span={4}>
                  <Form.Item>
                    <div className="form-group">
                      <label className="add-post__label">
                        <strong>Repetitions</strong>
                      </label>
                      {form.getFieldDecorator('repetitions', {
                        initialValue: this.state.data.repetitions,
                        rules: []
                      })(
                        <InputNumber
                          disabled={form.getFieldValue('recurrency') === 'once'}
                          min={1}
                          max={100}
                        />
                      )}
                    </div>
                  </Form.Item>
                </Col>
              </React.Fragment>
            )}
          </Row>

          <Form.Item>
            <div className="form-group">
              <label className="add-post__label">
                <strong>Location</strong>
              </label>
              {form.getFieldDecorator('location', {
                initialValue: this.state.data.location || '',
                rules: []
              })(<Input placeholder="Location" />)}
            </div>
          </Form.Item>

          <Form.Item>
            {form.getFieldDecorator('media')(
              <div className="form-group">
                <label className="add-post__label">
                  <strong>Post Image</strong>
                </label>

                <Upload
                  multiple={false}
                  name="media"
                  onChange={({ file }) => {
                    if (file.status === 'done') {
                      this.setState({
                        isImageChanged: true,
                        data: { ...this.state.data, media: file.response }
                      });
                      this.props.form.setFieldsValue({ media: file.response });
                    }
                  }}
                  accept={'image/jpg,image/png'}
                  customRequest={({ file, onSuccess }) =>
                    this.uploadMainImage(file).then(url => onSuccess(url))
                  }
                  action={''}>
                  {this.state.data.media && this.state.data.media.length ? (
                    <img
                      alt=""
                      src={this.state.data.media}
                      style={{ maxWidth: 250, maxHeight: 200 }}
                    />
                  ) : (
                    <Button>
                      <Icon type="upload" /> Click to Upload
                    </Button>
                  )}
                </Upload>
              </div>
            )}
          </Form.Item>
          <Row>
            <Col span={5}>
              <Form.Item>
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Participants</strong>
                  </label>
                  {form.getFieldDecorator('hasCapacity', {
                    initialValue: this.state.data.capacity > 0 || false,
                    valuePropName: 'checked',
                    rules: []
                  })(<Switch checkedChildren="Yes" unCheckedChildren="No" />)}
                </div>
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item>
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>
                      Capacity{' '}
                      {this.state.data.participants &&
                        this.state.data.participants.length > 0 && (
                          <small>
                            {this.state.data.participants.length} participants
                            signed
                          </small>
                        )}
                    </strong>
                  </label>
                  {form.getFieldDecorator('capacity', {
                    initialValue: this.state.data.capacity || 0,
                    rules: [{ type: 'number', message: 'Must be number' }]
                  })(
                    <InputNumber
                      disabled={!form.getFieldValue('hasCapacity')}
                    />
                  )}
                </div>
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <div className="form-group">
              <label className="add-post__label">
                <strong>Status</strong>
              </label>
              {form.getFieldDecorator('is_open', {
                initialValue: this.state.data.is_open || false,
                valuePropName: 'checked',
                rules: []
              })(<Switch checkedChildren="open" unCheckedChildren="closed" />)}
            </div>
          </Form.Item>
        </Modal>
      </React.Fragment>
    );
  }
}

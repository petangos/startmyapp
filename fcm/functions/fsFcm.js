const async = require('asyncawait/async');
const await = require('asyncawait/await');

const fsFcm = (functions, admin) => {

	const fs = admin.firestore();
	const fcm = admin.messaging();

	const sendPush = async((snap, context) => {
		console.log('push create detected in ' + admin.app().options.projectId);

		const doc = snap.data();

		console.log('NEW: ', JSON.stringify(doc, null, 2));

		if (!doc) {
			return null;
		}

		const payload = {
			notification: {
				title: doc.title,
				body: doc.body
			},
			data: {}
		};

		console.log('PAYLOAD: ', payload);



		const getTokens = async(() => {
			ret = [];

			let release_channel = 'staging';
			console.log("ENV: ", doc.env);
			if(doc.env) {
				let tokens = await(fs.collection('token').where('release_channel', '==', doc.env).get());
				// console.log(tokens);
				tokens.forEach((token) => {
					ret.push(token.id);
				});
			}
			else {
				console.log("Release channel not specified");
			}
			return ret;
		});

		const tokens = await(getTokens());

		if(tokens.length) {
			console.log('TOKENS: ', tokens);
			fcm.sendToDevice(tokens, payload).then(response => {
				// See the MessagingDevicesResponse reference documentation for
				// the contents of response.
				console.log('Successfully sent message:', JSON.stringify(response, null, 2));
				return response;
			})
			.catch(error => {
				console.log('Error sending message:', error);
			});
		}
		else {
			console.log("No tokens found");
		}
		return null;
	});


	return functions.firestore.document('push/{pushId}').onCreate(sendPush);
};

module.exports = fsFcm;
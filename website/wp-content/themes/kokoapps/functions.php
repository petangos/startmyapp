<?php

function add_require_scripts_files() {
    wp_enqueue_style('layout', get_template_directory_uri() . '/style.css', array(), '1.0.0', "all");
    wp_enqueue_style('rtl', get_template_directory_uri() . '/rtl.css', array('layout'), '1.0.0', "all");
}

add_action('wp_enqueue_scripts', 'add_require_scripts_files');

import React from 'react';
import { Tabs } from 'antd';
import './style.scss';
import Avatar from 'components/CleanComponents/Avatar';
import SettingsForm from './SettingsForm';
import { auth, db } from 'config/firebase/firebase';

const TabPane = Tabs.TabPane;

class ProfileApp extends React.Component {
  state = {
    displayName: '',
    email: '',
    photoURL: '',
    emailVerified: '',
    uid: '',
    phone: '',
    address: ''
  };

  itemsRef = '';

  componentDidMount() {
    const user = auth.currentUser;
    this.setState({
      displayName: user.displayName,
      email: user.email,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      uid: user.uid
    });

    db.collection('users')
      .doc(user.uid)
      .get()
      .then(doc => {
        if (doc.exists) {
          this.setState({
            phone: doc.data().phone,
            address: doc.data().address
          });
        } else {
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    const { displayName, photoURL } = this.state;
    return (
      <div className="profile">
        <div className="row">
          <div className="col-xl-4">
            <div
              className="card profile__header"
              style={{ backgroundImage: 'url(' + photoURL + ')' }}>
              <div className="profile__header-card">
                <div className="card-body text-center">
                  <Avatar
                    src={photoURL || 'resources/images/avatars/temp.jpg'}
                    size="110"
                    border="true"
                    borderColor="white"
                  />
                  <br />
                  <br />
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-8">
            <div className="card profile__social-info">
              <div className="profile__social-name">
                <h2>
                  <span className="text-black mr-2">
                    <strong>{displayName}</strong>
                  </span>
                </h2>
                {/** <p className="mb-1">{lastname}</p>*/}
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <Tabs defaultActiveKey="1">
                  <TabPane
                    tab={
                      <span>
                        <i className="icmn-cog" /> Settings
                      </span>
                    }
                    key="1">
                    <SettingsForm data={this.state} />
                  </TabPane>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileApp;

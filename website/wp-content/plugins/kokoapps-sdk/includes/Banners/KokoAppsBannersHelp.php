<?php

class KokoAppsBannersHelp {

    protected $post_type = 'app_banners';
    protected $name = 'Banners';
    protected $singular_name = 'Banner';
    protected $slug = 'banner';
    protected $slug_api = 'banners';
    protected $textdomain = 'KokoAppsBanners';

}

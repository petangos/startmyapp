import React from 'react';
import { Table } from 'antd';
import { Link } from 'react-router-dom';
import './style.scss';

import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({ firebase: state.firebase });

const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@connect(mapStateToProps)
class UsersList extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  async getUsers() {
    const client = this.props.firebase.client;
    this.unsubscribe = client
      .firestore()
      .collection('users')
      .onSnapshot(res => {
        const users = res.docs.map(item => ({
          uid: item.id,
          ...item.data(),
          key: item.id
        }));
        this.setState({ data: users });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidMount() {
    this.getUsers();
  }

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  onSearch = () => {
    const { searchText, tableData } = this.state;
    let reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0
                        ? [<span className="highlight">{match[0]}</span>, text]
                        : text
                  )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.unsubscribe();
      this.getUsers();
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;
    const columns = [
      {
        title: 'Name',
        dataIndex: 'full_name',
        key: 'title',
        render: (text, record) => (
          <Link to={`/users/${record.uid}`} className="utils__link--underlined">
            {text}
          </Link>
        ),
        sorter: (a, b) => a.uid - b.uid
      },

      {
        title: 'Action',
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              <Link to={`/users/${record.uid}`}>
                <i className="icmn-pencil mr-1" /> Edit
              </Link>{' '}
            </span>
          );
        }
      }
    ];

    return (
      <div className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>{this.props.postType} List</strong>
          </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            pagination={pager}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}

export default UsersList;

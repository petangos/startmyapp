<?php

class KokoAppsPayment {

    public function __construct() {
        register_rest_field('product', 'attributes', array(
            'get_callback' => array($this, "get_callback_attributes"),
        ));

        add_filter('woocommerce_rest_prepare_product_variation_object', array($this, 'woocommerce_rest_prepare_product_variation_object'), 999, 3);

        add_action('rest_api_init', function () {

            register_rest_route('kokoapps/v2', '/payment/(?P<gw>\w+)/(?P<order_id>\d+)', array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'getPaymentUrl'),
                'args' => array(
                    'gw' => array(
                        'required' => true,
                        'validate_callback' => function($param, $request, $key) {
                            return !empty($param);
                        }
                    ),
                    'order_id' => array(
                        'required' => true,
                        'validate_callback' => function($param, $request, $key) {
                            return is_numeric($param);
                        }
                    ),
                ),
                'permission_callback' => function ($request) {
                    return current_user_can('read');
                }
                    )
            );
        });
    }

    public function getPaymentUrl($request) {
        $params = $request->get_params();
        $result = array();
        if (!$order = wc_get_order($params['order_id'])) {
            $result = array(
                "code" => "no_order",
                "message" => "Order not found",
                "data" => array(),
                "status" => 404
            );
        } else {

            $available_gateways = WC()->payment_gateways->get_available_payment_gateways();

            if (!isset($available_gateways[$params['gw']])) {
                $result = array(
                    "code" => "no_gw",
                    "message" => "gateway not found",
                    "data" => $result,
                    "status" => 404
                );
            } else {

                WC()->session->set('order_awaiting_payment', $params['order_id']);
                // Process Payment.
                $result = $available_gateways[$params['gw']]->process_payment($params['order_id']);
                if ($result['result'] == 'success') {

                    switch ($params['gw']) {
                        case 'directpay':
                            $query = parse_url($result['redirect'], PHP_URL_QUERY);
                            parse_str($query, $queryparams);
                            $result['iframe'] = preg_replace('/^_/', '', $queryparams['frul']);
                            break;
                        case 'yaadpay':
                            $result['iframe'] = WC()->api_request_url('WC_Iframe_Yaadpay') . "?order_id=" . $params['order_id'];
                            break;
                        default:
                            $result['iframe'] = $result['redirect'];
                    }

                    unset($result['redirect']);
//				        if($params['gw'] == 'directpay') {
//					        $query = parse_url($result['redirect'], PHP_URL_QUERY);
//					        parse_str($query, $queryparams);
//					        $result['iframe'] = $queryparams['frul'];
//				        }
//				        elseif ($params['gw'] == 'directpay') {
//					        $result['iframe'] = WC()->api_request_url('WC_Iframe_Yaadpay') . "?order_id=" . $params['order_id'];
//				        }
//				        else {
//					        $result['iframe'] = $result['redirect'];
//				        }
//				        unset($result['redirect']);

                    $result = array(
                        "code" => "success",
                        "message" => "Order processing started",
                        "data" => $result,
                        "status" => 200
                    );
                } else {
                    $result = array(
                        "code" => "error",
                        "message" => "Order processing failed",
                        "data" => $result,
                        "status" => 200
                    );
                }
            }
        }
        $response = new WP_REST_Response($result);
        $response->set_status($result['status']);
        return $response;
    }

    public function get_callback_attributes($product) {
        $product_new = new WC_Product($product['id']);
        $attributes = $product_new->get_attributes();

        foreach ($attributes as $item) {
            foreach ($item->get_options() as $option) {

                $data = get_term($option, $item->get_name());
                if (!is_wp_error($data)) {
                    $data = array(
                        "id" => $data->term_id,
                        "name" => $data->name,
                        "option" => $data->name,
                        "option_label" => urldecode($data->slug),
                        "option_id" => (int) $data->term_id
                    );
                } else {
                    $data = array(
                        "id" => -1,
                        "name" => $option,
                        "option" => $option,
                        "option_label" => $option,
                        "option_id" => -1
                    );
                }
                $tmp[$item->get_id()][] = $data;
            }
        }
        unset($product_new);

        foreach ($product['attributes'] as $pkey => $pval) {
            $product['attributes'][$pkey]['options'] = $tmp[$pval['id']];
        }
        return $product['attributes'];
    }

    public function woocommerce_rest_prepare_product_variation_object($response, $item, $request) {
        global $wpdb;

        $data = $response->get_data();
        $params = $request->get_url_params();

        foreach ($data['attributes'] as $key => $value) {
            $querystr = "SELECT term_id FROM `wp_terms` WHERE slug LIKE '{$data['attributes'][$key]['option']}' LIMIT 1;";
            $term = $wpdb->get_results($querystr, OBJECT);
            $data['attributes'][$key]['option_id'] = (int) $term[0]->term_id;

            $data['attributes'][$key]['option_label'] = urldecode($data['attributes'][$key]['option']);
        }

        return $data;
    }

}

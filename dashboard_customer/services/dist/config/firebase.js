"use strict";

const admin = require('firebase-admin');

const FIREBASE_SERVICE_ACCOUNT = require('../../koko-apps-firebase-admin.json');

module.exports.initializeApp = function initializeApp(appSecrets) {
  const namespace = appSecrets ? 'client' : undefined;

  if (!appSecrets || appSecrets && !admin.apps.find(item => item.name === 'client')) {
    const app = admin.initializeApp({
      credential: admin.credential.cert(appSecrets || FIREBASE_SERVICE_ACCOUNT),
      databaseURL: 'https://koko-apps.firebaseio.com'
    }, namespace);
    const firestore = app.firestore();
    const settings = {
      timestampsInSnapshots: true
    };
    firestore.settings(settings);
  }
};

module.exports.koko = admin.apps.find(item => item.name === '[DEFAULT]');
module.exports.client = admin.apps.find(item => item.name === 'client');
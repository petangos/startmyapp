import React from 'react';
import { Table } from 'antd';
import { withFirebase } from '../../../../../ducks/firebase';
import './style.scss';
import moment from 'moment';

const SendPush = React.lazy(() => import('./SendPush'));
const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@withFirebase
class ProductsList extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  unsubscribe = () => null;

  pushForm = React.createRef();

  componentDidMount() {
    this.unsubscribe = this.getNotifications();
  }

  getNotifications() {
    const { client } = this.props.firebase;
    return client
      .firestore()
      .collection('push')
      .onSnapshot(snap => {
        const data = snap.docs
          .map(item => ({ key: item.id, ...item.data() }))
          .sort(
            (a, b) =>
              moment.unix(a.created_at).isBefore(moment.unix(b.created_at))
                ? 1
                : -1
          );
        this.setState({ data });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.unsubscribe();
      this.unsubscribe = this.getNotifications();
    }
  }

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  onSearch = () => {
    const { searchText, tableData } = this.state;
    let reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0
                        ? [<span className="highlight">{match[0]}</span>, text]
                        : text
                  )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;

    const columns = [
      {
        title: 'Title',
        dataIndex: 'message.title',
        key: 'title'
      },
      {
        title: 'Created At',
        dataIndex: 'created_at',
        key: 'created_at',
        sorter: (a, b) => a.name - b.name,
        render: txt => moment.unix(txt).format('DD/MM/YYYY HH:mm')
      },
      {
        title: 'Environment',
        dataIndex: 'release_channel',
        key: 'release_channel',
        sorter: (a, b) => a.release_channel - b.release_channel
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        sorter: (a, b) => a.status - b.status,
        render: text => text || 'qeueued'
      },

      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            {/* <a href="" className="mr-2">
              <i className="icmn-pencil mr-1" /> View
            </a> */}
            {record.status !== 'sent' && (
              <a href="">
                <i className="icmn-cross mr-1" /> Cancel
              </a>
            )}
          </span>
        )
      }
    ];

    return (
      <React.Fragment>
        <div className="card">
          <div className="card-header">
            <div className="utils__title">
              <strong>Send Push Notification</strong>
            </div>
          </div>
          <div className="card-body">
            <React.Suspense fallback={<span>loading...</span>}>
              <SendPush ref={this.pushForm} />
            </React.Suspense>
          </div>
        </div>
        <div className="card">
          <div className="card-header">
            <div className="utils__title">
              <strong>Notifications List</strong>
            </div>
          </div>
          <div className="card-body">
            <Table
              columns={columns}
              dataSource={data}
              pagination={pager}
              onChange={this.handleTableChange}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ProductsList;

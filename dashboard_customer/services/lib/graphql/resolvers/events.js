const admin = require('firebase-admin');
const moment = require('moment');
const { dateify, toDate, cleanObject } = require('../utils');

function getEvents({ day, month, limit, offset, attending, ...rest }, { uid }) {
  const client = admin.apps[1];
  const collection = client.firestore().collection('events');
  let query = collection;
  if (day)
    query = collection
      .where(
        'start_time',
        '>=',
        admin.firestore.Timestamp.fromDate(toDate(day))
      )
      .where(
        'start_time',
        '<=',
        admin.firestore.Timestamp.fromDate(toDate(day, 1))
      );
  else if (month) {
    query = collection
      .where(
        'start_time',
        '>=',
        admin.firestore.Timestamp.fromDate(moment(month, 'MM-YYYY').toDate())
      )
      .where(
        'start_time',
        '<=',
        admin.firestore.Timestamp.fromDate(
          moment(month, 'MM-YYYY')
            .add(1, 'M')
            .toDate()
        )
      );
  }
  if (attending === true && uid)
    query = query.where('participants', 'array-contains', uid);

  if (limit) query = query.limit(parseInt(limit));
  return query.get().then(res =>
    res.docs.map(doc => {
      const event = doc.data();
      return {
        id: doc.id,
        is_user_attending: event.participants
          ? event.participants.includes[uid]
          : false,
        start_time: dateify(event.start_time.toDate()),
        end_time: event.end_time ? dateify(event.end_time.toDate()) : null,
        recurrency: event.recurrency || 'once',
        is_full_day: event.full_day || false,
        title: event.title || '',
        description: event.description || '',
        short_description: event.short_description || '',
        participants: event.participants || [],
        is_open: event.is_open || false,
        capacity: event.capacity || 0,
        location: event.location || '',
        media: event.media || []
      };
      return event;
    })
  );
}

function getEvent(id) {
  const client = admin.apps[1];

  return client
    .firestore()
    .collection('events')
    .doc(id)
    .get()
    .then(doc => {
      const event = doc.data();
      event.id = doc.id;
      event.start_time = dateify(event.start_time.toDate());
      if (event.end_time) event.end_time = dateify(event.end_time.toDate());
      return event;
    });
}

function addEvent(args) {
  const client = admin.apps[1];

  const event = cleanObject(args);
  event.start_time = admin.firestore.Timestamp.fromDate(
    toDate(args.start_time)
  );
  if (event.end_time)
    event.end_time = admin.firestore.Timestamp.fromDate(toDate(args.end_time));
  event.media = event.media.map(item => ({ ...item }));
  return client
    .firestore()
    .collection('events')
    .add(event)
    .then(res =>
      res.get().then(doc => {
        const event = doc.data();
        event.id = doc.id;
        event.start_time = dateify(event.start_time.toDate());
        if (event.end_time) event.end_time = dateify(event.end_time.toDate());
        return event;
      })
    );
}

async function registerForEvent(args) {
  const client = admin.apps[1];

  const res = await client
    .firestore()
    .collection('events')
    .doc(args.eid)
    .get();
  const { participants, capacity } = res.data();
  if (participants && participants.length >= capacity) {
    return { type: 'fail', error: 'no available spots' };
  }
  return client
    .firestore()
    .collection('events')
    .doc(args.eid)
    .update({
      participants: admin.firestore.FieldValue.arrayUnion(args.uid)
    })
    .then(() => ({ type: 'success' }))
    .catch(() => ({ type: 'fail' }));
}

function unregisterFromEvent(args) {
  const client = admin.apps[1];

  return client
    .firestore()
    .collection('events')
    .doc(args.eid)
    .update({
      participants: admin.firestore.FieldValue.arrayRemove(args.uid)
    })
    .then(() => ({ type: 'success' }))
    .catch(() => ({ type: 'fail' }));
}

function deleteEvent(id) {
  const client = admin.apps[1];

  return client
    .firestore()
    .collection('events')
    .doc(id)
    .delete()
    .then(() => ({ type: 'success' }))
    .catch(() => ({ type: 'fail' }));
}

module.exports = {
  getEvents,
  addEvent,
  deleteEvent,
  getEvent,
  registerForEvent,
  unregisterFromEvent
};

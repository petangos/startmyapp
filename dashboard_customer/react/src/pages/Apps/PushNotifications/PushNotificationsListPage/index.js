import React from 'react';
import Page from '../../../../components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import PushNotificationsList from './PushNotificationsList/';

class PushNotificationsListPage extends React.Component {
  static defaultProps = {
    pathName: 'Push Notifications',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    return (
      <Page {...props}>
        <Helmet title="Push Notifications" />
        <PushNotificationsList />
      </Page>
    );
  }
}

export default PushNotificationsListPage;

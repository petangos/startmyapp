import React from "react";
import RegisterForm from "./RegisterForm";
import "./style.scss";

class Register extends React.Component {
  state = {};

  componentDidMount() {
    document.getElementsByTagName("body")[0].style.overflow = "hidden";
  }

  componentWillUnmount() {
    document.getElementsByTagName("body")[0].style.overflow = "";
  }

  render() {
    return (

      <div className="main-login main-login--fullscreen">
        <div className="main-login__block main-login__block--extended pb-0">
          <div className="row">
            <div className="col-xl-12">
              <div className="main-login__block__inner">
                <div className="main-login__block__form">
                  <RegisterForm email={this.state.restoredEmail} />
                </div>
                <div className="main-login__block__sidebar">
                  <div className="main-login__block__promo text-black text-center">
                    <h1 className="mb-3 text-black">
                      <strong>WELCOME TO KOKO APPS</strong>
                    </h1>
                    <p className="main-login__block__sidebar__title text-white">
                      Grow your business with a self managed, 100% native ui application.
                </p>
                  </div>
                  <div className="main-login__block__sidebar__item">
                    Make updates to your app in real time.
                    Kokoapps's was built with you in mind. Instead of having to deal with
                    complicated code to build and manage your app,
                    all you need to do is click and edit. It's almost magic.
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;

<?php
/*
  Plugin Name: SMApp Badge
  Plugin URI:
  Description: Implmenets Badge
  Author: KokoApps
  Version: 1.0.1
 */
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('SmappBadge')) {

    class SmappBadgeHelp {

        protected $post_type = 'app_badges';
        protected $name = 'Badges';
        protected $singular_name = 'Badge';
        protected $slug = 'badge';
        protected $slug_api = 'badges';
        protected $textdomain = 'smappBadge';

    }

    class SmappBadge extends SmappBadgeHelp {

        function __construct() {
            add_action('init', array(&$this, 'init'));

            //run activation steps
            register_activation_hook(__FILE__, array($this, 'register_activation_hook'));
        }

        function init() {
            load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages');

            $labels = array(
                'name' => _x($this->name, 'Post Type General Name', $this->textdomain),
                'singular_name' => _x($this->singular_name, 'Post Type Singular Name', $this->textdomain),
                'menu_name' => __($this->name, $this->textdomain),
                'parent_item_colon' => __('Parent ' . $this->singular_name, $this->textdomain),
                'all_items' => __('All ' . $this->name, $this->textdomain),
                'view_item' => __('View ' . $this->singular_name, $this->textdomain),
                'add_new_item' => __('Add New Badge', $this->textdomain),
                'add_new' => __('Add New', $this->textdomain),
                'edit_item' => __('Edit ' . $this->singular_name, $this->textdomain),
                'update_item' => __('Update ' . $this->singular_name, $this->textdomain),
                'search_items' => __('Search ' . $this->singular_name, $this->textdomain),
                'not_found' => __('Not Found', $this->textdomain),
                'not_found_in_trash' => __('Not found in Trash', $this->textdomain),
            );

            $args = array(
                'label' => __($this->slug, $this->textdomain),
                'description' => __($this->singular_name . ' news and reviews', $this->textdomain),
                'labels' => $labels,
                // Features this CPT supports in Post Editor
                'supports' => array(
                    'title',
                    'editor',
                    'excerpt',
                    'thumbnail',
                    'revisions',
                    'custom-fields',
                ),
                // You can associate this CPT with a taxonomy or custom taxonomy. 
                'taxonomies' => array('genres'),
                /* A hierarchical CPT is like Pages and can have
                 * Parent and child items. A non-hierarchical CPT
                 * is like Posts.
                 */
                'hierarchical' => false,
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'show_in_nav_menus' => true,
                'show_in_admin_bar' => true,
                'menu_position' => 15,
                'can_export' => true,
                'has_archive' => false,
                'exclude_from_search' => true,
                'publicly_queryable' => true
            );

            // Registering your Custom Post Type
            register_post_type($this->post_type, $args);
        }

        function register_activation_hook() {
            
        }

    }

    $smappBadge = new SmappBadge();
}

if (!class_exists('SmappBadgeWoocommerce')) {

    //woocommerce_product_write_panel_tabs
    class SmappBadgeWoocommerce extends SmappBadgeHelp {

        function __construct() {
            add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));

            add_filter('woocommerce_product_data_tabs', array(&$this, 'woocommerce_product_data_tabs'), 99, 1);
            add_action('woocommerce_product_data_panels', array(&$this, 'woocommerce_product_data_panels'));
            add_action('woocommerce_process_product_meta', array(&$this, 'woocommerce_process_product_meta'));
        }

        public function admin_enqueue_scripts() {
            wp_enqueue_script('SmappBadgeWoocommerce_js', plugin_dir_url(__FILE__) . 'assets/js/admin.js', array('jquery'), '1.0.0', true
            );
            wp_enqueue_style(
                    'SmappBadgeWoocommerce_css', plugins_url('/assets/css/style.css', __FILE__)
            );
        }

        function woocommerce_product_data_tabs($product_data_tabs) {
            $product_data_tabs['my-custom-tab'] = array(
                'label' => __($this->name, $this->textdomain),
                'target' => 'smapp_badge_woocommerce_data',
            );
            return $product_data_tabs;
        }

        function woocommerce_product_data_panels() {
            global $woocommerce, $post;

            $value = get_post_meta($post->ID, $this->post_type . '_select', true);

            if (empty($value))
                $value = '';
            ?>
            <div id="smapp_badge_woocommerce_data" class="panel woocommerce_options_panel">
                <?php
                $posts = get_posts(array(
                    'post_type' => $this->post_type
                ));

                //echo '<pre>'.print_r($posts,1).'</pre>';die();
                $options[''] = __('Select a value', $this->textdomain);
                $options['discount'] = __('Discount', $this->textdomain);

                foreach ($posts as $item)
                    $options[$item->ID] = $item->post_title;

                woocommerce_wp_select(array(
                    'id' => $this->post_type . '_select',
                    'label' => __('Select a ' . $this->singular_name, $this->textdomain),
                    'options' => $options,
                    'value' => $value,
                ));


                $discount_value = get_post_meta($post->ID, $this->post_type . '_discount', true);
                woocommerce_wp_text_input(array(
                    'id' => $this->post_type . '_discount',
                    'wrapper_class' => 'smapp_badge_woocommerce_field_hidden',
                    'label' => __('Insert discount', $this->textdomain),
                    'data_type' => array(
                        'class' => 'price',
                        'value' => ''
                    ),
                    'value' => $discount_value,
                ));
                ?>
            </div>
            <?php
        }

        function woocommerce_process_product_meta($post_id) {
            $id = $this->post_type . '_select';
            $woocommerce_select = $_POST[$id];
            if (!empty($woocommerce_select))
                update_post_meta($post_id, $id, esc_attr($woocommerce_select));
            else {
                update_post_meta($post_id, $id, '');
            }

            $id = $this->post_type . '_discount';
            $woocommerce_select = $_POST[$id];
            if (!empty($woocommerce_select))
                update_post_meta($post_id, $id, esc_attr($woocommerce_select));
            else {
                update_post_meta($post_id, $id, '');
            }
        }

    }

    $smappBadge = new SmappBadgeWoocommerce();
}

if (!class_exists('SmappBadgeApi')) {

    class SmappBadgeApi extends SmappBadgeHelp {

        public function __construct() {

            add_action('rest_api_init', function () {
                register_rest_route('kokoapps/v2/', $this->slug_api, array(
                    'methods' => WP_REST_Server::READABLE,
                    'callback' => array(&$this, 'smapp_api'),
                    'args' => array(
                    ),
                ));
            });
            register_rest_field('product', $this->slug_api, array(
                'get_callback' => array($this, "get_callback"),
            ));
        }

        public function smapp_api() {
            $posts = get_posts(array(
                'post_type' => $this->post_type
            ));

            if (empty($posts)) {
                return new WP_Error('No Data', __('Invalid',$this->textdomain), array('status' => 404));
            }

            $temp = array();
            foreach ($posts as $item) {
                $image_id = get_post_thumbnail_id($item->ID);

                $item->images = array(
                    array(
                        'id' => (int) $image_id,
                        'date_created' => wc_rest_prepare_date_response($item->post_date),
                        'date_created_gmt' => wc_rest_prepare_date_response($item->post_date_gmt),
                        'date_modified' => wc_rest_prepare_date_response($item->post_modified),
                        'date_modified_gmt' => wc_rest_prepare_date_response($item->post_modified_gmt),
                        'src' => wp_get_attachment_url($image_id),
                        'title' => get_the_title($attachment),
                        'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                    )
                );

                $temp[] = $item;
            }

            return $temp;
        }

        public function get_callback($product) {
            $key_transient = $this->slug_api . $product['id'];

            // Get any existing copy of our transient data
            if (false === ( $result_array = get_transient($key_transient) )) {

                $app_badges_select = get_post_meta($product['id'], 'app_badges_select', true);
                $item = get_post($app_badges_select);

                $image_id = get_post_thumbnail_id($item->ID);
                $image = array();
                if ($image_id != '') {
                    $image = array(
                        'id' => (int) $image_id,
                        'date_created' => wc_rest_prepare_date_response($item->post_date),
                        'date_created_gmt' => wc_rest_prepare_date_response($item->post_date_gmt),
                        'date_modified' => wc_rest_prepare_date_response($item->post_modified),
                        'date_modified_gmt' => wc_rest_prepare_date_response($item->post_modified_gmt),
                        'src' => wp_get_attachment_url($image_id),
                        'title' => '',
                        'alt' => '',
                    );
                }
                $result_array[] = array(
                    'id' => $item->ID,
                    'name' => $item->post_title,
                    'image' => $image
                );

                set_transient($key_transient, $result_array, 7 * DAY_IN_SECONDS);
            }
            
            return $result_array;
        }

    }

    $smappBadgeApi = new SmappBadgeApi();
}
import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import PostsList from './PostsList/';

class PostsListPage extends React.Component {
  static defaultProps = {
    pathName: 'Posts List',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    const postType = props.match.path.slice(1);
    return (
      <Page {...props}>
        <Helmet title={`${postType} List`} />
        <PostsList postType={postType} />
      </Page>
    );
  }
}

export default PostsListPage;

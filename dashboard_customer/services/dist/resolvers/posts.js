"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const admin = require('firebase-admin');

const _require = require('../utils'),
      dateify = _require.dateify,
      toDate = _require.toDate,
      cleanObject = _require.cleanObject;

async function getPosts(_ref) {
  let _ref$per_page = _ref.per_page,
      per_page = _ref$per_page === void 0 ? 20 : _ref$per_page,
      _ref$page = _ref.page,
      page = _ref$page === void 0 ? 1 : _ref$page,
      created_at = _ref.created_at,
      _ref$limit = _ref.limit,
      limit = _ref$limit === void 0 ? 20 : _ref$limit,
      _ref$offset = _ref.offset,
      offset = _ref$offset === void 0 ? 0 : _ref$offset,
      category = _ref.category,
      uid = _ref.uid,
      rest = _objectWithoutProperties(_ref, ["per_page", "page", "created_at", "limit", "offset", "category", "uid"]);

  const client = admin.apps[1];
  const qs = rest;
  const collection = client.firestore().collection('posts');
  let query = collection;
  Object.keys(qs).forEach(key => query = query.where(key, '==', qs[key]));
  if (rest.type === 'meal') query = query.where('author', '==', uid);
  if (category) query = query.where('categories', 'array-contains', category);
  if (per_page) query = query.limit(parseInt(per_page));
  if (limit) query = query.limit(parseInt(limit));
  if (created_at) query = query.where('created_at', '==', admin.firestore.Timestamp.fromDate(toDate(created_at)));
  return query.get().then(res => {
    return res.docs.map(doc => {
      const post = doc.data();
      post.id = doc.id;
      if (post.created_at) post.created_at = dateify(post.created_at.toDate());
      return post;
    });
  });
}

function getPost(id) {
  const client = admin.apps[1];
  return client.firestore().collection('posts').doc(id).get().then(doc => {
    const post = doc.data();
    post.id = doc.id;
    if (post.created_at) post.created_at = dateify(post.created_at.toDate());
    return post;
  });
}

function addPost(args) {
  const client = admin.apps[1];
  const post = cleanObject(args.post);
  if (post.created_at) post.created_at = admin.firestore.Timestamp.fromDate(toDate(post.created_at));
  return client.firestore().collection('posts').add(post).then(res => res.get().then(doc => {
    const post = doc.data();
    post.id = doc.id;
    if (post.created_at) post.created_at = dateify(post.created_at.toDate());
    return post;
  }));
}

function editPost(args) {
  const client = admin.apps[1];
  const post = cleanObject(args.post);
  if (post.created_at) post.created_at = admin.firestore.Timestamp.fromDate(toDate(post.created_at));
  delete post.id;
  return client.firestore().collection('posts').doc(args.post.id).update(post).then(() => client.firestore().collection('posts').doc(args.post.id).get().then(res => _objectSpread({
    id: args.post.id
  }, res.data())));
}

function deletePost(id) {
  const client = admin.apps[1];
  return client.firestore().collection('posts').doc(id).delete().then(() => ({
    type: 'success'
  })).catch(() => ({
    type: 'fail'
  }));
}

function mapPostResponse(post) {}

module.exports = {
  getPosts,
  addPost,
  deletePost,
  getPost,
  editPost,
  mapPostResponse
};
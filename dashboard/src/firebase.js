import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

var config = {
  apiKey: 'AIzaSyA1SFTlDGjbN9FiydWkDnPLq4QeC99nYrk',
  authDomain: 'koko-apps.firebaseapp.com',
  databaseURL: 'https://koko-apps.firebaseio.com',
  projectId: 'koko-apps',
  storageBucket: 'koko-apps.appspot.com',
  messagingSenderId: '90002079257'
};
if (firebase.apps.length === 0) {
  firebase.initializeApp(config);
}
const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);
export const auth = firebase.auth();
export const db = firestore;
export const now = firebase.firestore.Timestamp.now;

const events = require('./events');
const posts = require('./posts');
const categories = require('./categories');
const { toDate, dateify } = require('../utils');
const admin = require('firebase-admin');
const { AuthenticationError } = require('apollo-server-express');

const resolvers = {
  Query: {
    events: (root, args, context) => {
      return events.getEvents(args, context);
    },
    event: (root, args) => events.getEvent(args.id),
    posts: (root, args, { uid }) => posts.getPosts({ ...args, uid }),
    user: async (root, args, { uid }) => {
      const client = admin.apps[1];
      if (!uid) throw new AuthenticationError('you must be logged in');
      const user = await client
        .firestore()
        .collection('users')
        .doc(uid)
        .get()
        .then(res => ({ uid: res.id, ...res.data() }));
      return user;
    },
    post: (root, args) => posts.getPost(args.id),
    categories: (root, args) => categories.getCategories(args)
  },

  Post: {
    links: async (parent, args) => {
      const client = admin.apps[1];
      return [];
      const collection = client
        .firestore()
        .collection('posts')
        .orderBy('created_at');
      const next =
        parent && parent.created_at
          ? await collection
              .startAfter(
                admin.firestore.Timestamp.fromDate(toDate(parent.created_at))
              )
              .get()
              .then(
                res =>
                  res.docs.map(doc => {
                    const post = doc.data();
                    post.id = doc.id;
                    if (post.created_at)
                      post.created_at = dateify(post.created_at.toDate());
                    return post;
                  })[0]
              )
          : null;

      const prev =
        parent && parent.created_at
          ? await collection
              .endBefore(
                admin.firestore.Timestamp.fromDate(toDate(parent.created_at))
              )
              .get()
              .then(
                res =>
                  res.docs.map(doc => {
                    const post = doc.data();
                    post.id = doc.id;
                    if (post.created_at)
                      post.created_at = dateify(post.created_at.toDate());
                    return post;
                  })[0]
              )
          : await collection.get().then(
              res =>
                res.docs.map(doc => {
                  const post = doc.data();
                  post.id = doc.id;
                  if (post.created_at)
                    post.created_at = dateify(post.created_at.toDate());
                  return post;
                })[0]
            );
      return [
        {
          rel: 'next',
          resource: next
        },
        {
          rel: 'prev',
          resource: prev
        }
      ];
    }
  },

  Event: {
    is_user_attending: (parent, args, context) => {
      const client = admin.apps[1];
      return context.uid
        ? parent.participants &&
            parent.participants.length &&
            parent.participants.includes(context.uid)
        : false;
    },
    participants: parent => {
      const client = admin.apps[1];
      return parent.participants
        ? parent.participants.map(uid =>
            client
              .auth()
              .getUser(uid)
              .then(user => ({
                id: user.uid,
                name: user.displayName,
                avatar: {
                  type: 'image',
                  src: user.photoURL,
                  alt: ''
                }
              }))
          )
        : [];
    }
  },
  Mutation: {
    ///events
    addEvent: (root, args) => events.addEvent(args),
    deleteEvent: (root, args) => events.deleteEvent(args.id),
    registerForEvent: (root, args) => events.registerForEvent(args, root),
    unregisterFromEvent: (root, args) => events.unregisterFromEvent(args),
    ///posts
    addPost: (root, args) => posts.addPost(args),
    deletePost: (root, args) => posts.deletePost(args.id),
    editPost: (root, args) => posts.editPost(args),
    setUserProfile: (root, args) => {
      const client = admin.apps[1];
      const values = { ...args };
      delete values['id'];
      return client
        .firestore()
        .collection('users')
        .doc(args.id)
        .set(values, { merge: true })
        .catch(() => ({ type: 'fail' }))
        .then(() => ({ type: 'success' }));
    },
    // utils
    reportOpenedNotification: (root, { notId }, { uid }) => {
      const client = admin.apps[1];
      const notRef = client
        .firestore()
        .collection('push')
        .doc(notId);
      return client
        .firestore()
        .runTransaction(transaction => {
          return transaction.get(notRef).then(doc => {
            transaction.update(notRef, {
              recipients: doc
                .data()
                .recipients.map(item =>
                  item.uid === uid ? { ...item, opened: true } : item
                )
            });
          });
        })
        .catch(() => ({ type: 'fail' }))
        .then(() => ({ type: 'success' }));
    }
  }
};
function getLinksForPost(parent) {
  console.log(parent);
}

module.exports = resolvers;

import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import CategoryEdit from './CategoryEdit';

class CategoryEditPage extends React.Component {
  static defaultProps = {
    pathName: 'Add Blog Post',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    const category = this.props.match.params.category;
    const postType = category.slice(0, category.length - 1);
    return (
      <Page {...props}>
        <Helmet title={`Category Add/Edit`} />
        <CategoryEdit catId={props.match.params.cat_id} postType={postType} />
      </Page>
    );
  }
}

export default CategoryEditPage;

import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import PostEdit from './PostEdit';

class PostEditPage extends React.Component {
  static defaultProps = {
    pathName: 'Add Blog Post',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    const postType = props.match.path.split('/')[1];
    const postId = props.match.params.post_id;
    return (
      <Page {...props}>
        <Helmet title={`${postType} Add/Edit`} />
        <PostEdit
          postType={`${postType.substr(0, postType.length - 1)}`}
          postId={postId}
        />
      </Page>
    );
  }
}

export default PostEditPage;

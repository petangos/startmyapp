"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const admin = require('firebase-admin');

const _require = require('../utils'),
      dateify = _require.dateify,
      toDate = _require.toDate,
      cleanObject = _require.cleanObject;

module.exports = {
  getCategories(args) {
    const client = admin.apps[1];
    let query = client.firestore().collection('categories');
    if (args.post_type) query = query.where('post_type', '==', args.post_type);
    return query.get().then(res => res.docs.map(doc => {
      return _objectSpread({
        id: doc.id
      }, doc.data());
    }));
  }

};
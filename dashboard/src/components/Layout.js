import styled from 'styled-components';

export const Wrapper = styled.div`
  background-color: ${props =>
    props.background ? props.theme.background : 'transparent'};
  display: flex;
  flex: 1;
`;

export const Center = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

import React from 'react';
import ProfileHeadCard from 'components/CleanComponents/ProfileHeadCard';

class DashboardBeta extends React.Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-lg-12 col-xl-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-xl-12">
                    <ProfileHeadCard />
                  </div>
                </div>
              </div>
            </div>
            {/** 
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Work Progress</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Work Progress information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-xl-12">
                    <ProgressGroup />
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Employees</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with Employees important information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <UserCard type={'primary'} />
                  </div>
                  <div className="col-md-6">
                    <UserCard />
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Task Table</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Task Table information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-12">
                    <Table
                      columns={taskTableColumns}
                      dataSource={taskTableData}
                      rowSelection={taskTableRowSelection}
                      pagination={false}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Information Cards</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Information Cards information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <InfoCard form="interactive" icon="database" type="danger" />
                  </div>
                  <div className="col-md-6">
                    <InfoCard form="interactive" icon="database" btnType="success" type="success" />
                  </div>
                </div>
              </div>
            </div>
            */}
          </div>
          {/** <div className="col-lg-12 col-xl-6">
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Server Info</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Server Info information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-6">
                    <SliderCard />
                  </div>
                  <div className="col-lg-6">
                    <SliderCard inverse={true} />
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Server Statistics</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Server Statistics information
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-6">
                    <InfoCard form="stats" icon="database" type="primary" />
                    <InfoCard form="stats" icon="users" type="empty" />
                  </div>
                  <div className="col-lg-6">
                    <InfoCard form="stats" icon="bullhorn" type="primary" />
                    <InfoCard form="stats" icon="price-tags" type="empty" />
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Server Configuration</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Server Configuration information
                </div>
              </div>
              <div className="card-body">
                <div className="mb-5">
                  <Slider marks={rangeMarks} defaultValue={rangeSlider.first} />
                </div>
                <div className="mb-4">
                  <Slider range marks={rangeMarks} defaultValue={rangeSlider.second} />
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Week Revenue Statistics, Billions</strong>
                </div>
                <Donut type="primary" name="Nuts" />
                <Donut type="success" name="Apples" />
                <Donut color="yellow" name="Peaches" />
              </div>
              <div className="card-body">
                <ChartistGraph
                  data={weekChartistData}
                  options={weekChartistOptions}
                  type="Line"
                  className="chart-area height-300 mt-4 chartist"
                />
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Month Site Visits Growth, %</strong>
                </div>
                <Donut type="primary" name="Income" />
                <Donut type="success" name="Outcome" />
              </div>
              <div className="card-body">
                <ChartistGraph
                  data={monthCartistData}
                  options={monthChartistOptions}
                  type="Bar"
                  className="chart-area height-300 mt-4 chartist"
                />
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Chat</strong>
                </div>
                <div className="utils__titleDescription">Block with important Chat information</div>
              </div>
              <div className="card-body">
                <Chat />
              </div>
            </div>
          </div>*/}
        </div>
        {/** <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">
                <div className="row">
                  <div className="col-lg-3">
                    <InfoCard form="bordered" icon="home" type="danger" />
                  </div>
                  <div className="col-lg-3">
                    <InfoCard form="bordered" icon="key" type="primary" />
                  </div>
                  <div className="col-lg-3">
                    <InfoCard form="bordered" icon="play2" type="warning" />
                  </div>
                  <div className="col-lg-3">
                    <InfoCard form="bordered" icon="database" type="sucess" />
                  </div>
                </div>
              </div>
              <div className="card-body">
                <Table
                  columns={tableColumns}
                  dataSource={this.state.tableData}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="utils__title">
                  <strong>Calendar</strong>
                </div>
                <div className="utils__titleDescription">
                  Block with important Calendar information
                </div>
              </div>
              <div className="card-body">
                <Calendar dateCellRender={dateCellRender} />
              </div>
            </div>
          </div>
        </div>*/}
      </div>
    );
  }
}

export default DashboardBeta;

<?php

/*
  Plugin Name: SMApp Contact Us
  Plugin URI:
  Description: Implmenets Contact Us
  Author: KokoApps
  Version: 1.0.1
 */
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('SMAppContactUs')) {

    class SMAppContactUs {

        function __construct() {
            add_action('init', array(&$this, 'init'));
            add_action('rest_api_init', array(&$this, 'rest_api_init'));
        }

        function init() {
            load_plugin_textdomain('smapp-contact-us', false, basename(dirname(__FILE__)) . '/languages');
        }

        function smapp_contac_us($data) {
            $_POST["your-name"] = $data->get_param('fname');
            $_POST["your-subject"] = $data->get_param('subject');
            $_POST["your-email"] = $data->get_param('email');
            $_POST["your-phone"] = $data->get_param('phone');
            $_POST["your-message"] = $data->get_param('message');

            $html = "
            <b>From:</b> {$_POST["your-name"]} <{$_POST["your-email"]}>
            <br />
            <b>Subject:</b> {$_POST["your-subject"]}
            <br />
            <b>Phone:</b> {$_POST["your-phone"]}
            <br />
            <b>Email:</b> {$_POST["your-email"]}
            <br />
            <b>Message Body:</b>
            <br />
            {$_POST["your-message"]}
            <br />
            <br />
            Host: {$_SERVER["HTTP_HOST"]}
        ";

            add_filter('wp_mail_content_type', function( $content_type ) {
                return 'text/html';
            });

            
            $setting= get_option('admin_email');
            if(strpos($_SERVER["HTTP_HOST"], 'dev.') !== false){
                $setting='kfir@petangos.com';
            }
            wp_mail(
                $setting, __('New Contact', 'smapp-contact-us'), '<html><body dir="ltr">' . $html . '</body></html>', '', array(), $_POST["your-email"], $_POST["your-name"]
            );

            return array(
                'status' => 'Success'
            );
        }

        function rest_api_init() {
            register_rest_route('kokoapps/v2/contact/', '/submit', array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(&$this, 'smapp_contac_us'),
                'args' => array(),
            ));
        }

    }

    return new SMAppContactUs();
}
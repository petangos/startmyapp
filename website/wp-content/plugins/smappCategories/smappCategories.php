<?php
/*
  Plugin Name: SMApp Categories
  Plugin URI:
  Description: Implmenets custom-fields-to-categories
  Author: KokoApps
  Version: 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}


if (!class_exists('smappCategories')) {

    class smappCategories {

        final public function __construct() {
            add_action('admin_init', array(&$this, 'admin_init'));
            add_action('admin_enqueue_scripts', array($this, 'admin_scripts_and_styles'));
            add_action('product_cat_edit_form_fields', array(&$this, 'product_cat_edit_form_fields'));
            add_action('edited_product_cat', array(&$this, 'edited_product_cat'));
        }

        function admin_init() {
            load_plugin_textdomain('smappCategories', false, basename(dirname(__FILE__)) . '/languages');
        }

        public function admin_scripts_and_styles() {

            // Get current screen attributes
            $screen = get_current_screen();

            if ($screen != null and $screen->id == "edit-product_cat") {

                // Adds WP Modal Window References			
                wp_enqueue_media();

                // Enque the script
                wp_enqueue_script('smappCategories_admin_script', plugin_dir_url(__FILE__) . 'assets/js/wcb-admin.js', array('jquery'), '1.0.0', true
                );

                // Add Style
                wp_enqueue_style(
                        'smappCategories_admin_styles', plugins_url('/assets/css/wcb-admin.css', __FILE__)
                );
            }
        }

        function product_cat_edit_form_fields($term) {
            //getting term ID
            $term_id = $term->term_id;

            //------------------------------------------------------------------
            $components=array();
            $tmp = get_term_meta($term_id, 'smappCategories_components', true);
            if(!empty($tmp)){
                foreach ($tmp as $item) {
                    $components[$item] = $item;
                }
            }
            $options = array(
                'drawer' => __('App Drawer', 'smappCategories'),
                'homepage_top' => __('App HomePage Top', 'smappCategories'),
            );
            ?>
            <tr class="form-field">
                <th scope="row" valign="top">
                    <label for="smappCategories_components"><?php _e('Display Area In App', 'smappCategories'); ?></label>
                </th>
                <td>
                    <select multiple name="smappCategories_components[]" style="height:200px !important;">
                        <option value="" selected><?php _e('No Selection',$this->textdomain)?></option>
                        <?php
                        foreach ($options as $key => $value) {
                            ?><option <?php print (in_array($key, $components) ? 'selected' : ''); ?> value="<?php print $key; ?>"><?php print $value; ?></option><?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php
            //------------------------------------------------------------------
            $tmp = get_term_meta($term_id, 'smappCategories_sliders', true);
            $sliderMax = 3;
            for ($i = 1; $i <= $sliderMax; $i++) {
                $fldName = 'smappCategories_sliders_' . $i;
                $sliders_id = $tmp[$i];
                ?>
                <tr class="form-field">
                    <th scope="row" valign="top">  
                        <label for="sliders_url">
                            <?php print __('Sliders Image:', 'smappCategories') . ' ' . $i; ?>
                        </label>  
                    </th>  
                    <td>  
                        <fieldset>
                            <a data-id="smappCategories_sliders_<?php print $i; ?>" class='wcb_upload_file_button button' uploader_title='Select File' uploader_button_text='Include File'>
                                <?php print __('Upload File', 'smappCategories') . ' ' . $i; ?>
                            </a>
                            <a data-id="smappCategories_sliders_<?php print $i; ?>" class='wcb_remove_file button'>
                                <?php print __('Remove File', 'smappCategories') . ' ' . $i; ?>
                            </a>
                            <label class='label_<?php print $fldName; ?>'>
                                <?php if ($sliders_id != null) echo basename(wp_get_attachment_url($sliders_id)) ?>
                            </label>
                        </fieldset>
                        <fieldset>				
                            <img class="img_admin_<?php print $fldName; ?>" src="<?php if ($sliders_id != null) echo wp_get_attachment_url($sliders_id) ?>" />
                        </fieldset>
                        <input type="hidden" class='image_<?php print $fldName; ?>' name='smappCategories_sliders[<?php print $i ?>]' value='<?php if ($sliders_id != null) echo $sliders_id; ?>' />
                    </td>
                </tr>
                <?php
            }
        }

        // A callback function to save our extra taxonomy field(s)  
        public function edited_product_cat($term_id) {
            if (isset($_POST['smappCategories_components'])) {
                update_term_meta($term_id, 'smappCategories_components', $_POST['smappCategories_components']);
            }
            if (isset($_POST['smappCategories_sliders'])) {
                update_term_meta($term_id, 'smappCategories_sliders', $_POST['smappCategories_sliders']);
            }
        }

    }

    $smappCategories = new smappCategories();
}


if (!class_exists('smappCategoriesApi')) {

    class smappCategoriesApi {

        public function __construct() {
            add_action('woocommerce_rest_prepare_product_cat', array(&$this, 'woocommerce_rest_prepare_product_cat'), 999, 3);
        }

        public function woocommerce_rest_prepare_product_cat($response, $item, $request) {
            $data = $response->get_data();

            if (empty($item->term_id)) {
                $url = $request->get_url_params();
                $term_id = $url['id'];
            } else {
                $term_id = $item->term_id;
            }

            $data['components'] = array();
            $data['sliders'] = array();
            
            $tmp=get_term_meta($term_id, 'smappCategories_components', true);
            if ($tmp != false) {
                $data['components'] = $tmp;
            } 
            
            $tmp = get_term_meta($term_id, 'smappCategories_sliders', true);
            if ($tmp != false) {
                $images = array();
                foreach ($tmp as $image_id) {
                    if (empty($image_id)) {
                        continue;
                    }
                    $attachment = get_post($image_id);

                    $images[] = array(
                        'id' => (int) $image_id,
                        'date_created' => wc_rest_prepare_date_response($attachment->post_date),
                        'date_created_gmt' => wc_rest_prepare_date_response($attachment->post_date_gmt),
                        'date_modified' => wc_rest_prepare_date_response($attachment->post_modified),
                        'date_modified_gmt' => wc_rest_prepare_date_response($attachment->post_modified_gmt),
                        'src' => wp_get_attachment_url($image_id),
                        'title' => get_the_title($attachment),
                        'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                    );
                }
                $data['sliders'] = $images;
            } 
            
            
            $response->set_data($data);

            return $response;
        }

    }

    $smappCategoriesApi = new smappCategoriesApi();
}
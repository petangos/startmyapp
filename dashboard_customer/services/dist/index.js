"use strict";

var _push = require("./push");

var _bodyParser = _interopRequireDefault(require("body-parser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const express = require('express');

const createServer = require('./graphql/server');

const cors = require('cors');

const graphqlServer = createServer();

const _require = require('./config'),
      initializeApp = _require.initializeApp;

initializeApp();
const app = express();
app.use(cors({
  origin: '*'
}));
graphqlServer.applyMiddleware({
  app,
  path: '/api'
});
app.use(_bodyParser.default.json());
const port = 8889;
app.get('/', (req, res) => res.send('Hello World!'));
app.get('/redirect', (req, res) => {
  const path = req.query.path;
  return res.redirect(path);
});
app.post('/push', _push.push);
app.listen(port, () => console.log(`KokoApps Server is running on port: ${port}!`));
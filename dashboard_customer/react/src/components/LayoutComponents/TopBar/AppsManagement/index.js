import React from 'react';
import { Menu, Dropdown } from 'antd';

import { setFirebaseApp, withFirebase } from '../../../../ducks/firebase';

@withFirebase
class AppsManagement extends React.Component {
  handleClick = e => {
    const app = this.props.firebase.applications.find(
      item => item.name === e.key
    );
    this.props.dispatch(setFirebaseApp(app));
  };

  render() {
    const menuItems = this.props.firebase.applications
      .filter(item => item.name !== '[DEFAULT]')
      .map(item => ({
        key: item.name,
        value: item
      }));
    const menu = (
      <Menu selectable={false} onClick={this.handleClick.bind(this)}>
        {menuItems.map(function(itemStr) {
          return (
            <Menu.Item key={itemStr.key}>
              {/* <span className="topbar__dropdownTitle">
                <strong>{itemStr.value.name}</strong>
              </span> */}

              {itemStr.value.name}
            </Menu.Item>
          );
        })}
      </Menu>
    );

    return (
      <div className="topbar__dropdown d-inline-block mr-4">
        <Dropdown overlay={menu} trigger={['click']} placement="bottomLeft">
          <a className="ant-dropdown-link" href="/">
            <i className="icmn-database mr-2 topbar__dropdownIcon" />
            <span className="d-none d-xl-inline">
              <strong>
                {this.props.firebase.client
                  ? this.props.firebase.client.name
                  : 'My Apps'}
              </strong>
            </span>
          </a>
        </Dropdown>
      </div>
    );
  }
}

export default AppsManagement;

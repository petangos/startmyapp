﻿=== Cart2Cart: OpenСart to WooCommerce Migration ===
Сontributors: Cart2Cart
Tags: opencart to woocommerce, opencart to woocommerce migration, migrate opencart to woocommerce,woocommerce, opencart, migration, data transfer, import, export
Requires at least:  3.1.2
Tested up to: 4.8.1
Stable tag: 4.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Perform a smooth and secure database transfer from OpenCart to WooCommerce. Free Demo & 24/7 are at your disposal! 

== Description== 
**Supported WooCommerce versions: 1.1.x – 1.6.x, 2.x, 3.x**
**Supported OpenCart versions: 0.7.x - 2.x, 3.x**

Cart2Cart: OpenCart to WooCommerce Migration plugin is the easiest way to switch your current OpenCart store to WooCommerce securely and quickly. The service is easy to use and the whole process will be done within your WordPress interface and accompanied by Migration Wizard.

Migration from OpenCart to WooCommerce with Cart2Cart enables you to migrate all your store data (customers, products, orders, etc.), preserving relations between entities with no special technical skills and additional efforts. Replatforming will be done automatically after your provide required credentials. The whole process will last couple of hours and will not impact on your current OpenCart store performance (it will remain active).

**Cart2Cart: OpenCart to WooCommerce Migration plugin is free to download, and after installation you’ll be offered 2 migration options:**
 
Free Demo migration — allows you to move a limited number of eCommerce data from your current OpenCart store to a live WooCommerce store or Cart2Cart test store (as you wish) within 30 minutes.
Full Migration — lets you import UNLIMITED amount of products, orders, customers, categories, variants, attributes and other related entities to WooCommerce. The price of Full Migration starts at $69 and may vary depending on the amount of transferable entities and additional migration options you may choose. 
 
### Benefits you receive performing OpenCart to WooCommerce migration using Cart2Cart

- **Get your new store in few clicks**
Install Cart2Cart: OpenCart to WooCommerce Migration plugin, set the connection between two stores and start your automated migration to WooCommerce in 1 click.

- **100% active OpenCart store**
Data transfer from OpenCart to WooCommerce will not affect your current store performance. You are welcome to continue selling goods while your entities are intensively migration to WooCommerce. There is absolutely no need to worry that your can lose customers or money during replatforming. 

- **Customers 24/7 Support is always there for you**
If there are any questions or problems to be solved, Cart2Cart 24/7 support staff will gladly answer them or fix issues. Feel free to contact support team via multiple channels:  live chat, email or phone.

- **No code manipulations and manual data input**
After you provide required credentials, Cart2Cart: OpenCart to WooCommerce migration plugin will transfer your eCommerce data to WooCommerce with no user interaction. It means that you don’t need to posses exceptional tech skills or appeal to developer’s help - replatforming will be done automatically.

- **Pay exactly for what you move**
Cart2Cart offers fair and flexible pricing policy to its user - you will pay only for the data you migrate from one store to another. The total price of migration directly depends on the number of entities you want to transfer and additional options that you may pick up to expand your migration possibilities.

- **Use Free Demo to check the quality of migration**
Cart2Cart enables you to perform Free Demo migration from OpenCart to WooCommerce and testify the quality of data migration. During Demo transfer a limited number of entities will be moved to your live WooCommerce store or Cart2Cart test store in 30 minutes. After evaluation of migration result you can continue with full-scale replatforming.

### What data can be transferred within OpenCart to WooCommerce migration 

**Products** - Options , Attributes, Name, SKU, Full Description, Status, Custom Fields, Manufacturer, Tax Class, Price, Sale Price, Sale Price From-To Date, URL, Meta Title, Meta Keywords, Meta Description, Weight, Width, Height, Depth, Downloadable Products ( Files, Max Downloads), Product Tags, Variants (Weight, Attributes, Quantity, Price, Additional image), Base Image, Additional Images, Quantity, Manage Stock, Stock Status.

**Orders** - ID, Order Date, Order Status, Custom Order Status, Order Products (Name, SKU, Option, Image), Product Price, Quantity, Shipping Price, Total Price, Order Comments, Order Status Historyб Customer Name, Email, Billing Address (First Name, Last Name, Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone), Shipping Address (Company, Address 1, Address 2, Country, State, City, Zip Code). 

**Customers** - First Name, Last Name, Email, Customer Group, Passwords.
Billing Address (First Name, Last Name, Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone ).
Shipping Address (First Name, Last Name, Company, Address 1, Address 2, Country, State, City, Zip Code).

**Categories** - Name, Description, Image, URL, Meta Title, Meta Description, Meta Keywords.
**Manufacturers** - Name, Image  
**Taxes** - Tax Class (Tax Name, Rate, Country, State).
**Coupons** - Name, Description, Status, Coupon Code, Uses Per Coupon, Uses Per Customer, Type Discount, Discount Amount, Product, Category, Coupon Expire Date.
**Reviews** - Created Date, Status, Rate, User Name, Comment, Product.
**Multistore**
**Multiple Languages**

**For updated information about supported entities available for migration from OpenCart to WooCommerce - check entities table <a href="https://www.shopping-cart-migration.com/shopping-cart-migration-options/4924-opencart-to-woocommerce-migration"> here. </a>**

### Additional options available for OpenCart to WooCommerce migration

* Clear current data on WooCommerce  before migration
* Skip product thumbnail images migration
* Migrate categories and products SEO URLs
* Create the 301 redirects to WooCommerce store after migration
* Preserve order IDs on WooCommerce store
* Strip HTML from category, product names and descriptions
* Migrate images from products descriptions, categories descriptions and blog posts descriptions
* Migrate customer's passwords 
* Change products quantity to 100 on WooCommerce store.

==Installation==

1. Download the plugin.
1. Log in your WordPress admin panel
1. Go to Plugins > Add New > Browse. Pick Cart2Cart plugin and click “Install Now” button. 
1. Activate it by clicking “Activate plugin”.
1. Find Cart2Cart plugin on the left side menu. Register Cart2Cart and proceed with the migration setup.

== Frequently Asked Questions ==

= Can I migrate product options to WooCommerce? =
Due to WooCommerce peculiarities, Cart2Cart migrates product options to WooCommerce as product variants. However, you can install the paid Add-on on your WooCommerce store prior to data transfer, and your product options will be moved directly into the plugin without converting to variants.
For more detailed information about migration of product options to WooCommerce go <a href="https://www.shopping-cart-migration.com/faq/45-woocommerce/can-migrate-product-options-woocommerce"> here. </a>

= Can I migrate metadata to WooCommerce? =
Yes, Cart2Cart supports metadata migration. Online shop owners can transfer their meta titles, keywords and descriptions from and to WooCommerce store, but they need additional WordPress plugin for it.
For more detailed information about metadata migration to WooCommerce go <a href="http://www.shopping-cart-migration.com/faq/45-woocommerce/302-can-i-migrate-metadata-to-woocommerce"> here. </a>

= How to migrate manufacturers from and to WooCommerce? =
Cart2Cart now supports migration of manufacturers to and from WooCommerce shopping cart. In order to do this, merchants will need to install an additional module on their WooCommerce stores. 
For more detailed information about manufacturers migration to WooCommerce go <a href="http://www.shopping-cart-migration.com/faq/45-woocommerce/287-how-to-migrate-manufacturers-from-and-to-woocommerce"> here </a> .
 
= Will my customer passwords be transferred during WooCommerce migration? =
Yes, there is an opportunity to migrate passwords from and to WooCommerce, providing that the store has default settings and no custom code modifications are included. Specifically, shop owners can move passwords to/from Magento (plugin required). 

**Note:**  *If your shopping cart has any modifications in password settings, we can not guarantee its accurate transfer.* 

Also, you can perform password migration from PrestaShop, Magento and OpenCart to WooCommerce using Cart2Cart: Password Migration to WooCommerce plugin.

Check out detailed information how to install and use Cart2Cart: Password Migration to WooCommerce plugin<a href=" http://www.shopping-cart-migration.com/faq/45-woocommerce/329-will-my-customer-passwords-be-migrated-after-woocommerce-migration"> here. </a>

== Screenshots ==
1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png
6. screenshot-6.png
7. screenshot-7.png
8. screenshot-8.png

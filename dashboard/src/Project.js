import React from 'react';
import {
  Paper as _Paper,
  CircularProgress,
  Button,
  Typography
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import { Publish, Save } from '@material-ui/icons';
import { Center } from './components';
import JSONInput from './json';
import { db, now } from './firebase';
import locale from './json/locale/en';
import styled from 'styled-components';

const Paper = styled(_Paper)`
  width: 60vw;
  margin: 3em;
  padding: 1em;
`;

const Json = styled(JSONInput)``;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 1em;
`;

class ProjectContainer extends React.Component {
  state = {
    title: '',
    jsonData: null
  };
  constructor(props) {
    super(props);
    this.appRef = db
      .collection('applications')
      .doc(props.match.params.project_id);
    this.save = this.save.bind(this);
  }
  componentDidMount() {
    this.appRef.onSnapshot(snap => {
      const manifest = snap.data().manifest || '{}';
      this.setState({
        jsonData: JSON.parse(manifest),
        title: manifest.name || snap.data().name
      });
    });
  }

  save(jsonData) {
    this.appRef.set(
      { manifest: JSON.stringify(jsonData), last_updated: now() },
      { merge: true }
    );
  }

  render() {
    return (
      <ProjectComponent
        save={this.save}
        data={this.state.jsonData}
        title={this.state.title}
        project_id={this.props.match.params.project_id}
      />
    );
  }
}

const ProjectComponent = ({ data, project_id, save, title }) => (
  <React.Fragment>
    <Center>
      {!data && <CircularProgress />}
      {data && (
        <React.Fragment>
          <Paper style={{ maxWidth: '80vw' }}>
            <Form onSubmit={values => save(values.manifest)}>
              {({
                handleSubmit,
                pristine,
                invalid,
                dirtySinceLastSubmit,
                submitSucceeded
              }) => (
                <form>
                  <Typography variant="headline">{title}</Typography>
                  <Field name="manifest">
                    {({ input }) => (
                      <Json
                        theme="light_mitsuketa_tribute"
                        id={project_id}
                        locale={locale}
                        height="70vh"
                        width="100%"
                        placeholder={data}
                        onChange={e => input.onChange(e.jsObject)}
                      />
                    )}
                  </Field>

                  <Row>
                    <Button
                      disabled={
                        pristine ||
                        invalid ||
                        (submitSucceeded && !dirtySinceLastSubmit)
                      }
                      variant="contained"
                      color="primary"
                      onClick={handleSubmit}>
                      <Save />
                      Save
                    </Button>

                    <Button
                      variant="outlined"
                      color="primary"
                      disabled={pristine || invalid || !dirtySinceLastSubmit}>
                      <Publish />
                      Save and Publish
                    </Button>
                  </Row>
                </form>
              )}
            </Form>
          </Paper>
        </React.Fragment>
      )}
    </Center>
  </React.Fragment>
);

export const Project = ProjectContainer;

import React from 'react';
import { Form, Input, Button, Upload, Icon } from 'antd';

const FormItem = Form.Item;

const Dragger = Upload.Dragger;

@Form.create()
class IconForm extends React.Component {
  render() {
    return (
      <Form className="add-post__form mt-3">
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>App Icon Name</strong>
            </label>
            <Input placeholder="Icon Name" />
          </div>
        </FormItem>
        <FormItem>
          <div className="form-group">
            <label className="add-post__label">
              <strong>Files</strong>
            </label>
            <Dragger>
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">
                Click or drag file to this area to upload
              </p>
            </Dragger>
          </div>
        </FormItem>
        <FormItem>
          <div className="add-post__submit">
            <span className="mr-3">
              <Button type="primary" htmlType="submit">
                Save and Post
              </Button>
            </span>
          </div>
        </FormItem>
      </Form>
    );
  }
}

export default IconForm;

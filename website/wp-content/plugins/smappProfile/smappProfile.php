<?php
/*
  Plugin Name: SMApp Profile
  Description: Adjustment Themes
  Author: Koko-Apps
  Version: 1.0.1
 */
if (!defined('ABSPATH')) {
    exit;
}



if (!class_exists('SmappProfile')) {

    class SmappProfileHelp {

        protected $post_type = 'app_profile';
        protected $name = 'Profle';
        protected $singular_name = 'Profile';
        protected $slug = 'profile';
        protected $slug_api = 'profile';
        protected $textdomain = 'SmappProfile';

    }

    class SmappProfile extends SmappProfileHelp {

        public function SmappProfile() {
            add_action('admin_menu', array(&$this, 'admin_menu'));
        }

        function admin_menu() {
            add_menu_page(__('Profile', $this->textdomain), __('Profile', $this->textdomain), 'edit_posts', 'smapp_profile', array(&$this, 'page_callback_function'), 'dashicons-media-spreadsheet');
            // add_submenu_page('smapp_profile', __('Information', $this->textdomain), 'Sub-menu 1 Title', 'edit_posts', 'menu_slug', 'page_callback_function1');
        }

        function page_callback_function() {
            global $wpdb;
            ?>
            <h1><?php _e($this->name, $this->textdomain); ?></h1>
            <form method="POST" action="?page=add_data">
                <label>Team Name: </label><input type="text" name="team_name" /><br />
                <label>Team City: </label><input type="text" name="team_city" /><br />
                <label>Team State: </label><input type="text" name="team_state" /><br />
                <label>Team Stadium: </label><input type="text" name="team_stadium" /><br />
                <input type="submit" value="submit" />
            </form>';
            <?php
            $default_row = $wpdb->get_row("SELECT * FROM $table_name ORDER BY team_id DESC LIMIT 1");
            if ($default_row != null) {
                $id = $default_row->team_id + 1;
            } else {
                $id = 1;
            }
            $default = array(
                'team_id' => $id,
                'team_name' => '',
                'team_city' => '',
                'team_state' => '',
                'team_stadium' => '',
            );
            $item = shortcode_atts($default, $_REQUEST);

            $wpdb->insert($table_name, $item);
        }

    }

    $SmappProfile = new SmappProfile();
}



if (!class_exists('SmappProfileApi')) {

    class SmappProfileApi extends SmappProfileHelp {

        public function __construct() {

            add_action('rest_api_init', function () {
                register_rest_route('kokoapps/v2/', $this->slug_api, array(
                    'methods' => WP_REST_Server::READABLE,
                    'callback' => array(&$this, 'smapp_api'),
                    'args' => array(
                    ),
                ));
            });
        }

        public function smapp_api() {
            
        }

    }

    $smappBadgeApi = new SmappProfileApi();
}
<?php
/*
  Plugin Name: SMApp Push
  Description: Mobile Push Notifications
  Author: Koko-Apps
  Version: 1.0.2
 */
if (!defined('ABSPATH')) {
	exit;
}


if (!class_exists('SMAppPush')) {

	class SMAppPushHelp {

		protected $post_type = 'app_push';
		protected $name = 'Push';
		protected $singular_name = 'Push';
		protected $slug = 'smapp_push';
		protected $slug_api = 'push';
		protected $textdomain = 'smappPush';

	}

	class SMAppPush {

		private $textdomain = 'smapp-push';
		private $text_length = 30;
		private $push_gateway;
		private $projectId;
		private $method;

		function __construct() {

			add_action('init', array($this, 'init'));
//			add_action('init', array($this, 'create_push_channel_taxonomy'));

			add_filter('post_row_actions', array($this, 'set_push_list_row_actions'), 10, 2);
			add_filter('manage_smapp_push_posts_columns', array($this, 'set_push_list_columns'));
			add_action('manage_smapp_push_posts_custom_column', array($this, 'set_push_list_columns_data'), 10, 2);
			add_filter('manage_edit-smapp_push_sortable_columns', array($this, 'set_push_list_columns_sortable'));
//			add_action('load-post.php', array($this, 'set_push_edit_restrictions'), 10, 3);
			add_filter('wp_insert_post_data', array($this, 'validate_push_data'), 10, 2);
			add_action('save_post', array($this, 'save_push_state_externally'), 10, 3);
			add_action('admin_notices', array($this, 'admin_message'));
			add_action('admin_menu', array($this, 'admin_menu'));
			add_action('admin_init', array($this, 'settings_fields'));
			add_action('admin_action_push_clone', array($this, 'push_clone'));

			add_action('admin_head-post.php', array($this, 'set_push_edit_parts_hidden'));
			add_action('admin_head-post-new.php', array($this, 'set_push_edit_parts_hidden'));
//			add_action( 'edit_page_form', array($this, 'smapp_push_editor') );
			$this->push_gateway = preg_replace('/\/?$/', '', get_option('smapp-push-gateway'));
			$this->projectId = get_option('smapp-push-projectId');
			$this->method = get_option('smapp-push-method');
		}

		function init() {
			load_plugin_textdomain('smapp-push', false, basename(dirname(__FILE__)) . '/languages');
			$this->create_push_post_type();

			add_filter('user_has_cap', array($this, 'set_push_edit_restrictions'), 10, 3);
			add_filter('excerpt_length', function ($length) {
				global $post;
				if ($post->post_type == 'smapp_push') {
					return $this->text_length;
				}
				return $length;
			});

			add_filter('bulk_actions-edit-smapp_push', function ($actions) {
				return [];
			});

			add_filter('gettext', function ($translation, $original) {
				if ('Excerpt' == $original) {
					return __('Notification Content', $this->textdomain);
				}
				return $translation;
			}, 10, 2);
		}

		function set_push_edit_parts_hidden() {
			global $post;
			if ($post->post_type == 'smapp_push') {
				echo <<<CSSS
				<style>
					body.post-type-smapp_push #submitdiv .misc-pub-post-status,
					body.post-type-smapp_push #submitdiv .misc-pub-visibility, 
					/*body.post-type-smapp_push #submitdiv #delete-action,*/
					body.post-type-smapp_push #postexcerpt .inside p:last-child {
						display: none;
					}
				</style>
CSSS;
				if ($post->post_status == 'publish') {
					echo <<<CSSS
				<style>
					
					body.post-type-smapp_push #submitdiv {
						/*display: none;*/
					}
				</style>
CSSS;
				}
			}
		}

		function admin_menu() {
			add_submenu_page(
				'edit.php?post_type=smapp_push', __("SMAPP Push Settings", 'smapp-push'), __("Settings", 'smapp-push'), "manage_options", "smapp-push-settings", array($this, "settings_page")
			);
		}

		function settings_page() {

			echo '<div class="wrap">
							<form method="post" action="options.php"  enctype="multipart/form-data">';
			settings_fields("smapp-push-settings-group");
			settings_errors();
			do_settings_sections("smapp-push-settings");
			?>
			<p>
				<label>
					<?php _e("Gateway", 'smapp-push') ?>
					<input
						type="text"
						name="smapp-push-gateway"
						placeholder="<?php echo __('Gateway URL', $this->textdomain) ?>"
						value="<?php echo esc_attr(get_option('smapp-push-gateway')); ?>"/>
				</label>
			</p>
			<p>
				<label>
					<?php _e("Project ID", $this->textdomain) ?>
					<input
						type="text"
						name="smapp-push-projectId"
						placeholder="<?php echo __('Project ID', $this->textdomain) ?>"
						value="<?php echo esc_attr(get_option('smapp-push-projectId')); ?>"/>
				</label>
			</p>
			<p><?php _e('Push Method: ', $this->textdomain) ?>&nbsp;&nbsp;
			<?php
				$method = get_option('smapp-push-method');
				foreach(array('fcm', 'expo') as $m => $_method) {
					$checked = ( (! $method && $m == 0) || ($method && $method == $_method)) ? ' checked="checked"' : '';
			?>
					<label>
						<input
							type="radio"
							name="smapp-push-method"
							required
							value="<?php echo $_method ?>" <?php echo $checked ?> />
						<?php echo strtoupper($_method); ?>
					</label>
			<?php
				}
			?>
			</p>
			<?php
			submit_button();
			echo "</form>\n</div>";
		}

		function settings_fields() {
			add_settings_section("smapp-push-settings-group", __("Push Settings", 'smapp-push'), null, "smapp-push-settings");
			register_setting("smapp-push-settings-group", "smapp-push-gateway", array($this, 'validate_options'));
			register_setting("smapp-push-settings-group", "smapp-push-projectId", array($this, 'validate_options'));
			register_setting("smapp-push-settings-group", "smapp-push-method", array($this, 'validate_options'));
		}

		function validate_options($theme_options) {
			return $theme_options;
		}

		function create_push_post_type() {

			$labels = array(
				'name'               => __('SMAPP Push', $this->textdomain),
				'singular_name'      => __('Push', $this->textdomain),
				'add_new'            => __('Create push', $this->textdomain),
				'all_items'          => __('Push Notifications', $this->textdomain),
				'add_new_item'       => __('Send push', $this->textdomain),
				'edit_item'          => __('Edit push', $this->textdomain),
				'new_item'           => __('New push', $this->textdomain),
				'view_item'          => __('View push', $this->textdomain),
				'search_items'       => __('Search push notifications', $this->textdomain),
				'not_found'          => __('No push notifications found', $this->textdomain),
				'not_found_in_trash' => __('No push notifications found in trash', $this->textdomain),
				//'menu_name' => default to 'name'
			);
			$args = array(
				'labels'              => $labels,
				'public'              => false,
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 20,
				'menu_icon'           => 'dashicons-smartphone',
				'query_var'           => true,
				'rewrite'             => true,
				'capability_type'     => 'post',
				'hierarchical'        => false,
				'supports'            => array(
					'title',
//					'editor',
//					'author',
					'excerpt'
				),
				'can_export'          => false,
				'delete_with_user'    => false,
//				'register_meta_box_cb' => 'quote_add_post_type_metabox'
			);

			add_action('add_meta_boxes', array($this, 'add_smapp_push_metaboxes'));

			register_post_type('smapp_push', $args);
		}

		function add_smapp_push_metaboxes() {
			add_meta_box(
				'smapp_push_release_channel',
				__('Release Channel', $this->textdomain),
				array($this, 'smapp_push_release_channel_html'),
				'smapp_push'
			);
//
//			add_meta_box(
//				'smapp_push_method',
//				__('Push Method', $this->textdomain),
//				array($this, 'smapp_push_method_html'),
//				'smapp_push'
//			);
		}

		function smapp_push_release_channel_html($post) {
			?>
			<label for="smapp_push_release_channel"><?php _e('Select Release Channel ...', $this->textdomain) ?></label>
			<select name="release_channel" id="smapp_push_release_channel" class="postbox" required>
				<option value=""><?php _e('Select Release Channel ...', $this->textdomain) ?></option>
				<option value="staging" selected="selected"><?php _e('Staging', $this->textdomain) ?></option>
				<option value="production"><?php _e('Production', $this->textdomain) ?></option>
			</select>

			<script>
				jQuery.noConflict();
				jQuery(function ($) {
					// $release_channel = $('#smapp_push_release_channel');
					$('#smapp_push_release_channel').on('change', function () {
						$(this).focus();
						var val = $('option:selected', $(this)).val();
						var stagingOpt = $('option', $(this)).eq(1);
						console.log(val);
						if (val == 'production') {
							if (!confirm('<?php _e('Are you sure you want to send a push to the Production channel?', $this->textdomain) ?>')) {
								stagingOpt.prop('selected', true);
							}
						}
					})
				});

			</script>
			<?php
		}


		function create_push_channel_taxonomy() {
			$labels = array(
				'name'                       => __('Push Channels', $this->textdomain),
				'singular_name'              => __('Push Channel', $this->textdomain),
				'all_items'                  => __('All Channels', $this->textdomain),
				'edit_item'                  => __('Edit Channel', $this->textdomain),
				'view_item'                  => __('View Channel', $this->textdomain),
				'update_item'                => __('Update Channel', $this->textdomain),
				'add_new_item'               => __('Add New Channel', $this->textdomain),
				'new_item_name'              => __('New Channel Name', $this->textdomain),
				'search_items'               => __('Search Channels', $this->textdomain),
				'separate_items_with_commas' => __('Separate tags channels commas', $this->textdomain),
				'add_or_remove_items'        => __('Add or remove channels', $this->textdomain),
				'choose_from_most_used'      => __('Choose from the most used channels', $this->textdomain),
				'not_found'                  => __('No channels found', $this->textdomain)
			);
			$args = array(
				'labels'              => $labels,
				'public'              => false,
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 20,
				'query_var'           => true,
				'rewrite'             => true,
				'description'         => __('Push Notification Channel is a new feature available in Android 8', $this->textdomain),
				'hierarchical'        => true,
			);

			register_taxonomy('smapp_push_channel', 'smapp_push', $args);
		}

		/**
		 * @param $actions
		 * @param $post WP_Post
		 * @return mixed
		 */
		function set_push_list_row_actions($actions, $post) {
			if ($post->post_type === 'smapp_push') {
				unset($actions['inline hide-if-no-js']);
				if ($post->post_status == 'publish') {
					unset($actions['edit']);
					if (!user_can(wp_get_current_user(), 'administrator')) {
						unset($actions['trash']);
					}
				}
				if ($post->post_status == 'pending') {
					unset($actions['trash']);
				}
				$url = $url = admin_url('admin.php?post=' . $post->ID);
				$copy_link = wp_nonce_url(add_query_arg(array('action' => 'push_clone'), $url), 'push_clone_nonce', 'smpc_nonce');
				$actions['push_clone'] = sprintf('<a href="%1$s">%2$s</a>', esc_url($copy_link), __('Duplicate', $this->textdomain));
			}
			return $actions;
		}

		function restrict_post_deletion($post_id) {
			if (get_post_type($post_id) === 'smapp_push' && get_post_status($post_id) === 'publish' && !user_can(wp_get_current_user(), 'administrator')) {
				wp_die(__("You can't delete sent push"));
			}
		}

		function set_push_edit_restrictions($allcaps, $cap, $args) {
			$post = get_post($args[2]);
			if ($post->post_type == 'smapp_push' && $post->post_status == "publish" && 'edit_post' == $args[0]) {
				$allcaps[$cap[0]] = false;
			}
			return $allcaps;
		}

		function set_push_list_columns($cols) {
			$a = null;
			$cols = array(
				'title'           => __('Title', $this->textdomain),
				'excerpt'         => __('Notification Content', $this->textdomain),
//				'sound'       => __('Sound', $this->textdomain),
//				'image'       => __('Image', $this->textdomain),
//				'link'        => __('Link to', $this->textdomain),
//				'channel'     => __('Channel', $this->textdomain),
//				'priority'    => __('Priority', $this->textdomain),
				'release_channel' => __('Release Channel', $this->textdomain),
//				'method'          => __('Method', $this->textdomain),
				'date'            => __('Create Time', $this->textdomain),
//				'start_time' => __('Start Time', $this->textdomain),
//				'finish_time' => __('Finish Time', $this->textdomain),
//				'author'      => __('Author', $this->textdomain)
			);
			return $cols;
		}

		function set_push_list_columns_data($column, $post_id) {
			$push = get_post($post_id);
			switch ($column) {
				case "excerpt":
					echo $push->post_excerpt;
					break;
				case "release_channel":
					echo get_post_meta($post_id, 'release_channel', true);
					break;
//				case "method":
//					echo get_post_meta($post_id, 'method', true);
//					break;
				case "link":
					$link = get_post_meta($post_id, 'deeplink', true);
					echo $link;
					break;
				case 'channel':
					$channels = wp_get_post_terms($post_id, 'smapp_push_channel');
					echo implode(', ', array_column((array)$channels, 'name'));
			}
		}

		/**
		 * @return array
		 */
		function set_push_list_columns_sortable() {
			return array(
				'title'      => 'title',
				'date'       => 'date',
				'start_time' => 'start_time',
			);
		}

		function push_clone() {
			global $wpdb;
			if (!(isset($_GET['post']) || isset($_POST['post']) || (isset($_REQUEST['action']) && $_REQUEST['action'] == 'push_clone'))) {
				wp_die(__('No post to duplicate has been supplied!', $this->textdomain));
			}

			if (!isset($_GET['smpc_nonce']) || !wp_verify_nonce($_GET['smpc_nonce'], 'push_clone_nonce'))
				return;

			$post_id = (isset($_GET['post']) ? absint($_GET['post']) : absint($_POST['post']));
			$post = get_post($post_id);

			if (isset($post) && $post != null) {
				$args = array(
					'comment_status' => $post->comment_status,
					'ping_status'    => $post->ping_status,
					'post_author'    => $post->post_author,
					'post_content'   => $post->post_content,
					'post_excerpt'   => $post->post_excerpt,
					'post_name'      => $post->post_name,
					'post_parent'    => $post->post_parent,
					'post_password'  => $post->post_password,
					'post_status'    => 'draft',
					'post_title'     => $post->post_title,
					'post_type'      => $post->post_type,
					'to_ping'        => $post->to_ping,
					'menu_order'     => $post->menu_order
				);

				$new_post_id = wp_insert_post($args);

				/*
				 * get all current post terms ad set them to the new post draft
				 */
				$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
				foreach ($taxonomies as $taxonomy) {
					$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
					wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
				}

//				/*
//				 * duplicate all post meta just in two SQL queries
//				 */
//				$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
//				if (count($post_meta_infos)!=0) {
//					$sql_query = "INSERT INTO " .$wpdb->postmeta." (post_id, meta_key, meta_value) ";
//					foreach ($post_meta_infos as $meta_info) {
//						$meta_key = $meta_info->meta_key;
//						if( $meta_key == '_wp_old_slug' ) continue;
//						$meta_value = addslashes($meta_info->meta_value);
//						$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
//					}
//					$sql_query .= implode(" UNION ALL ", $sql_query_sel);
//					$wpdb->query($sql_query);
//				}

				wp_redirect(admin_url('post.php?action=edit&post=' . $new_post_id));
				exit;
			} else {
				wp_die(__('Post cloning failed, could not find original post: ', $this->textdomain) . $post_id);
			}
		}

		function show_admin_message($title, $message, $status, $post_id = null) {
			set_transient("smapp_push_message_" . get_current_user_id() . ($post_id ? "_$post_id" : ''), array($title, $message, $status));
		}

		function admin_message() {
			global $post;
			$screen = get_current_screen();
//			// Only edit post screen:
			if (in_array($screen->id, ['smapp_push', 'edit-smapp_push'])) {
				$user_id = get_current_user_id();
				$transient_name = 'smapp_push_message_' . $user_id;
				if ($post->ID) {
					$transient_name .= '_' . $post->ID;
				}
				$transient = get_transient($transient_name);
				if ($transient !== false) {
					list($title, $message, $status) = $transient;
					echo <<<HTML
					<div class="notice notice-$status is-dismissible"> 
						<p><strong>$title</strong></p>
						<p>$message</p>
						<button type="button" class="notice-dismiss">
							<span class="screen-reader-text">$message</span>
						</button>
					</div>
HTML;
					delete_transient($transient_name);
				}
			}
		}

		function validate_push_data($data, $post) {
			if ($post['post_type'] != 'smapp_push') {
				return $data;
			}


			if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || in_array($data['post_status'], array('draft', 'trash', 'delete'))) {
				return $data;
			}
			if (!$post['post_title'] && !$post['post_excerpt']) {
				$data['post_status'] = 'draft';
				$this->show_admin_message('ERROR', __('Please provide at least either title or body', $this->textdomain), 'error', $post['ID']);
			}
			if (!$post['release_channel'] || !$_POST['release_channel']) {
				$data['post_status'] = 'draft';
				$this->show_admin_message('ERROR', __('Release Channel not set', $this->textdomain), 'error', $post['ID']);
			} else {
				update_post_meta($post['ID'], 'release_channel', $post['release_channel']);
			}
			add_filter('redirect_post_location', function ($location) {
				return remove_query_arg('message', $location);
			});
			return $data;
		}

		function save_push_state_externally($post_id, $post, $update) {

			$post_type = get_post_type($post_id);

			if ("smapp_push" != $post_type) return;

//			update_post_meta($post->ID, 'method', $_POST['method']);
			$post_meta = get_post_meta($post_id);


//			add_filter( 'redirect_post_location', function( $location ) {
//				return remove_query_arg( 'message', $location);
//			});
			if (isset($post->post_status) && !in_array($post->post_status, array('publish'))) {
				return;
			}

			if (!$this->push_gateway) {
				$this->show_admin_message('ERROR', 'SMAPP Push Gateway not set', 'error', $post_id);
				return;
			}
			if (!$this->projectId) {
				$this->show_admin_message('ERROR', 'SMAPP Push Project ID not set', 'error', $post_id);
				return;
			}
			if (!$this->method) {
				$this->show_admin_message('ERROR', 'SMAPP Push Method not set', 'error', $post_id);
				return;
			}

			$push = array(
				'id'          => $post_id,
				'projectId'   => $this->projectId,
				'title'       => $post->post_title,
				'body'        => $post->post_excerpt,
				'sound'       => (!empty($post_meta['push_sound'][0]) ? $post_meta['push_sound'][0] : null),
				'image'       => (!empty($post_meta['push_image'][0]) ? $post_meta['push_image'][0] : null),
				'channel'     => 'DEFAULT',
				'priority'    => (!empty($post_meta['push_priority'][0]) ? $post_meta['push_priority'][0] : null),
				'create_time' => (!empty($post_meta['create_time'][0]) ? $post_meta['create_time'][0] : date('Y-m-d H:i:s')),
				'start_time'  => $post->post_date_gmt,
				'env'         => $post_meta['release_channel'][0],
				'deeplink'    => (!empty($post_meta['link']) ? $post_meta['link'] : null),
			);
			if($this->method == 'fcm') {
				$result = $this->updateGatewayPush($push);
				$title = __("SMAPP Push ") . ($result['status'] == 'success' ? __('Sent') : __('Failed'));
				$message = $result['status'] == 'success' ? __('Push sent', $this->textdomain) : __('Push failed', $this->textdomain);
				$status = $result['status'] == 'success' ? 'info' : 'error';
			}
			elseif ($this->method == 'expo') {
				$result = $this->getExpoTokens($post_meta['release_channel'][0]);
				if(isset($result['status']) && $result['status'] == 'success') {
					$tokens = json_decode($result['message'], true);
					if ($tokens['tokens']) {
						foreach (array_chunk($tokens['tokens'], 100) as $chunk) {
							$this->sendPushNotificationExpoChunk($chunk, $push['title'], $push['body']);
						}
					}
				}

				$title = __("SMAPP Push ") . ($result['status'] == 'success' ? __('Sent') : __('Failed'));
				$message = $result['status'] == 'success' ? __('Push sent', $this->textdomain) : __('Push failed', $this->textdomain);
				$status = $result['status'] == 'success' ? 'info' : 'error';
			}
			$this->show_admin_message($title, $message, $status, $post_id);
//			add_filter( 'redirect_post_location', function( $location ) {
//				return remove_query_arg( 'message', $location);
//			});

			wp_redirect(admin_url('edit.php?post_type=smapp_push'));
		}

		function updateGatewayPush($push) {

			$url = $this->push_gateway . "/push";
			$result = (new WP_Http_Curl())->request($url, ['method'  => 'POST',
			                                               'headers' => ['Content-Type' => 'application/json'],
			                                               'body'    => json_encode($push),
			                                               'timeout' => 20,
			]);
			$ret = array();
			if ($result INSTANCEOF WP_Error) {
				$ret['status'] = 'error';
				$ret['message'] = $result->get_error_message();
			} else if (isset($result['response'])) {
				$ret['status'] = $result['response']['code'] == 200 ? 'success' : 'error';
				$ret['message'] = $result['body'];
			} else {
				$ret['status'] = 'error';
				$ret['message'] = __('Unknown Error');
			}
			return $ret;
		}

		private function getExpoTokens($release_channel) {
			$url = $this->push_gateway . "/expotokens?env=$release_channel";
			$result = (new WP_Http_Curl())->request($url, ['method'  => 'GET',
			                                               'timeout' => 20]);
			$ret = array();
			if ($result INSTANCEOF WP_Error) {
				$ret['status'] = 'error';
				$ret['message'] = $result->get_error_message();
			} else if (isset($result['response'])) {
				$ret['status'] = $result['response']['code'] == 200 ? 'success' : 'error';
				$ret['message'] = $result['body'];
			} else {
				$ret['status'] = 'error';
				$ret['message'] = __('Unknown Error');
			}
			return $ret;
		}

		private function sendPushNotificationExpoChunk(array $pushTokensChunk, $title, $msg) {

			$push = array();
			foreach ($pushTokensChunk as $token) {
				$push[] = array(
					'to' => $token,
					'title' => $title,
					'body' => $msg,
//					'data' => ['route' => $route],
//					'sound' => 'default',
				);
			}

			$headers = [
				'accept: application/json',
				'accept-encoding: gzip, deflate',
				'Content-Type: application/json'
			];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://exp.host/--/api/v2/push/send ');
			curl_setopt($ch, CURLOPT_POST, true);
//	      curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($push));
			$output = curl_exec($ch);
			curl_close($ch);
			return $output;
		}

	}

	$SMAPPP = new SMAppPush();
}


if (!class_exists('SMAppPushApi')) {

	class SMAppPushApi extends SMAppPushHelp {

		public function __construct() {

			add_action('rest_api_init', function () {
				register_rest_route('kokoapps/v2/', $this->slug_api, array(
					'methods'  => WP_REST_Server::READABLE,
					'callback' => array(&$this, 'smapp_api'),
					'args'     => array(),
				));
			});
		}

		public function smapp_api() {
			$smapp_push_gateway = get_option('smapp-push-gateway');
			$smapp_push_projectId = get_option('smapp-push-projectId');
			$results = array(
				'gateway'   => $smapp_push_gateway,
				'projectId' => $smapp_push_projectId
			);
			if (empty($results)) {
				return new WP_Error('No Data', 'Invalid', array('status' => 404));
			}
			return $results;
		}
	}

	$smappBadgeApi = new SMAppPushApi();
}
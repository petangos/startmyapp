import React from 'react';
import { Form, Field } from 'react-final-form';
import { Center } from './components';
import styled from 'styled-components';
import {
  Paper as _Paper,
  Button,
  Input,
  Typography,
  FormControl,
  InputLabel,
  Divider as _Divider
} from '@material-ui/core';
import { auth } from './firebase';

const Paper = styled(_Paper)`
  padding: 1.5em;
  width: 45vw;
`;

const Divider = styled(_Divider)`
  margin-bottom: 5em;
`;

class LoginContainer extends React.Component {
  signInWithEmailAndPasswordAsync({ email, password }) {
    auth.signInWithEmailAndPassword(email, password).catch(console.log);
  }

  render() {
    return <LoginComponent signIn={this.signInWithEmailAndPasswordAsync} />;
  }
}

const LoginComponent = ({ signIn }) => (
  <Center>
    <Paper>
      <img src={require('./assets/logo.png')} alt="Koko Apps" />
      <Divider />
      <Form
        onSubmit={values =>
          signIn({ email: values.email, password: values.password })
        }>
        {({ handleSubmit, pristine, invalid }) => (
          <form onSubmit={handleSubmit}>
           {/* <Typography type="heading">Please Login</Typography>*/}
            <Field name="email">
              {({ input, meta }) => (
                <FormControl margin="normal" fullWidth>
                  <InputLabel htmlFor="email">Email Address</InputLabel>
                  <Input
                    id="email"
                    name="email"
                    type="email"
                    autoComplete="email"
                    autoFocus
                    {...input}
                  />
                </FormControl>
              )}
            </Field>
            <Field name="password">
              {({ input, meta }) => (
                <FormControl margin="normal" fullWidth>
                  <InputLabel htmlFor="email">Password</InputLabel>
                  <Input
                    id="password"
                    name="email"
                    type="password"
                    {...input}
                  />
                </FormControl>
              )}
            </Field>
            <Button
              type="submit"
              disabled={pristine || invalid}
              variant="primary">
              Sign In
            </Button>
          </form>
        )}
      </Form>
    </Paper>
  </Center>
);

export const Login = LoginContainer;

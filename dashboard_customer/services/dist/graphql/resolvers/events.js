"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const admin = require('firebase-admin');

const moment = require('moment');

const _require = require('../utils'),
      dateify = _require.dateify,
      toDate = _require.toDate,
      cleanObject = _require.cleanObject;

function getEvents(_ref, {
  uid
}) {
  let day = _ref.day,
      month = _ref.month,
      limit = _ref.limit,
      offset = _ref.offset,
      attending = _ref.attending,
      rest = _objectWithoutProperties(_ref, ["day", "month", "limit", "offset", "attending"]);

  const client = admin.apps[1];
  const collection = client.firestore().collection('events');
  let query = collection;
  if (day) query = collection.where('start_time', '>=', admin.firestore.Timestamp.fromDate(toDate(day))).where('start_time', '<=', admin.firestore.Timestamp.fromDate(toDate(day, 1)));else if (month) {
    query = collection.where('start_time', '>=', admin.firestore.Timestamp.fromDate(moment(month, 'MM-YYYY').toDate())).where('start_time', '<=', admin.firestore.Timestamp.fromDate(moment(month, 'MM-YYYY').add(1, 'M').toDate()));
  }
  if (attending === true && uid) query = query.where('participants', 'array-contains', uid);
  if (limit) query = query.limit(parseInt(limit));
  return query.get().then(res => res.docs.map(doc => {
    const event = doc.data();
    return {
      id: doc.id,
      is_user_attending: event.participants ? event.participants.includes[uid] : false,
      start_time: dateify(event.start_time.toDate()),
      end_time: event.end_time ? dateify(event.end_time.toDate()) : null,
      recurrency: event.recurrency || 'once',
      is_full_day: event.full_day || false,
      title: event.title || '',
      description: event.description || '',
      short_description: event.short_description || '',
      participants: event.participants || [],
      is_open: event.is_open || false,
      capacity: event.capacity || 0,
      location: event.location || '',
      media: event.media || []
    };
    return event;
  }));
}

function getEvent(id) {
  const client = admin.apps[1];
  return client.firestore().collection('events').doc(id).get().then(doc => {
    const event = doc.data();
    event.id = doc.id;
    event.start_time = dateify(event.start_time.toDate());
    if (event.end_time) event.end_time = dateify(event.end_time.toDate());
    return event;
  });
}

function addEvent(args) {
  const client = admin.apps[1];
  const event = cleanObject(args);
  event.start_time = admin.firestore.Timestamp.fromDate(toDate(args.start_time));
  if (event.end_time) event.end_time = admin.firestore.Timestamp.fromDate(toDate(args.end_time));
  event.media = event.media.map(item => _objectSpread({}, item));
  return client.firestore().collection('events').add(event).then(res => res.get().then(doc => {
    const event = doc.data();
    event.id = doc.id;
    event.start_time = dateify(event.start_time.toDate());
    if (event.end_time) event.end_time = dateify(event.end_time.toDate());
    return event;
  }));
}

async function registerForEvent(args) {
  const client = admin.apps[1];
  const res = await client.firestore().collection('events').doc(args.eid).get();

  const _res$data = res.data(),
        participants = _res$data.participants,
        capacity = _res$data.capacity;

  if (participants && participants.length >= capacity) {
    return {
      type: 'fail',
      error: 'no available spots'
    };
  }

  return client.firestore().collection('events').doc(args.eid).update({
    participants: admin.firestore.FieldValue.arrayUnion(args.uid)
  }).then(() => ({
    type: 'success'
  })).catch(() => ({
    type: 'fail'
  }));
}

function unregisterFromEvent(args) {
  const client = admin.apps[1];
  return client.firestore().collection('events').doc(args.eid).update({
    participants: admin.firestore.FieldValue.arrayRemove(args.uid)
  }).then(() => ({
    type: 'success'
  })).catch(() => ({
    type: 'fail'
  }));
}

function deleteEvent(id) {
  const client = admin.apps[1];
  return client.firestore().collection('events').doc(id).delete().then(() => ({
    type: 'success'
  })).catch(() => ({
    type: 'fail'
  }));
}

module.exports = {
  getEvents,
  addEvent,
  deleteEvent,
  getEvent,
  registerForEvent,
  unregisterFromEvent
};
import React from 'react';
import { connect } from 'react-redux';
import { REDUCER, submit } from 'ducks/login';
import { Form, Input, Button } from 'antd';
import { push } from 'react-router-redux';

const FormItem = Form.Item;

const mapStateToProps = (state, props) => ({
  isSubmitForm: state.app.submitForms[REDUCER]
});

@connect(mapStateToProps)
@Form.create()
class LoginForm extends React.Component {
  static defaultProps = {};

  onSubmit = (isSubmitForm: ?boolean) => event => {
    event.preventDefault();
    const { form, dispatch } = this.props;
    if (!isSubmitForm) {
      form.validateFields((error, values) => {
        if (!error) {
          dispatch(submit(values));
        }
      });
    }
  };

  signUp = () => event => {
    const { dispatch } = this.props;
    dispatch(push('/register'));
  };
  render() {
    const { form, isSubmitForm } = this.props;
    return (
      <div className="cat__pages__login__block__form">
        <img src="resources/images/login/logo.png" alt="Koko Apps" />
        <br />
        <br />
        <Form
          layout="vertical"
          hideRequiredMark
          onSubmit={this.onSubmit(isSubmitForm)}>
          <FormItem label="Email">
            {form.getFieldDecorator('username', {
              initialValue: '',
              rules: [
                {
                  type: 'email',
                  message: 'The is not a valid e-mail address'
                },
                { required: true, message: 'input required' }
              ]
            })(<Input size="default" placeholder="username@koko-apps.com" />)}
          </FormItem>
          <FormItem label="Password">
            {form.getFieldDecorator('password', {
              initialValue: '',
              rules: [
                { required: true, message: 'input required' },
                {
                  min: 8,
                  message: 'The password must be at least 8 characters long'
                }
              ]
            })(<Input size="default" type="password" placeholder="" />)}
          </FormItem>

          {/** <div className="mb-2">
            <a href="/reset-password/" className="utils__link--blue utils__link--underlined">
              Forgot password
            </a>
          </div>*/}
          <div className="form-actions">
            <Button
              type="primary"
              className="width-150 mr-4"
              htmlType="submit"
              loading={isSubmitForm}>
              Login
            </Button>
            {/** <Button className="width-100" htmlType="button" onClick={this.signUp()}>
              Sign Up
            </Button>*/}
          </div>
        </Form>
      </div>
    );
  }
}

export default LoginForm;

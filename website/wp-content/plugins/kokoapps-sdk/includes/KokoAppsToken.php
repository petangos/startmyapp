<?php
if (!class_exists('KokoAppsAuthenticateApi')) {
    class KokoAppsToken {

        public static function getExpiration($user) {
            return get_user_meta($user->ID, 'kokoapps_access_token_expiration', true);
        }

        public static function get($user) {
            return get_user_meta($user->ID, 'kokoapps_access_token', true);
        }

        public static function is_still_valid($user) {
            $token_expiration = self::getExpiration($user);
            return $token_expiration >= time();
        }

        public static function set($uid, $expiration) {
            $token = self::create();
            update_user_meta($uid, 'kokoapps_access_token', $token);
            update_user_meta($uid, 'kokoapps_access_token_expiration', $expiration);
            return $token;
        }

        protected static function create() {
            return wp_generate_password(43, false, false);
        }

    }
}
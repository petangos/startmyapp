const express = require('express');
const url = require('url');
const qs = require('querystring');
const async = require('asyncawait/async');
const await = require('asyncawait/await');

const httpFs = (functions, admin) => {

	const fs = admin.firestore();

	const smappPush = express();
	const router = express.Router();
	smappPush.use(express.json());
	smappPush.use('/', router);

	router.use((request, response, next) => {
		console.log(`Request info: ${request.url} ${request.method}`);

		let origin = request.headers.origin;

		response.setHeader('Access-Control-Allow-Origin', '*');
		response.setHeader('Access-Control-Allow-Methods', 'GET,POST');
		response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		response.setHeader('Access-Control-Allow-Credentials', true);
		response.setHeader('Content-Type', 'application/json');

		next();
	});


	router.get('/', (request, response) => {
		response.json({'hello': 'Welcome to SMAPP on ' + admin.instanceId().app.options.projectId + '!'});
	});

	router.get('/push/:id', async((request, response) => {
		response.json(await(fs.doc('push/'+request.params.id).get()).data());
	}));

	router.post('/push', async((request, response) => {
		console.log(request.body);
		if (request.body.id) {
			response.json(await(fs.doc('push/' + request.body.id).set(request.body)));
		}
		else {
			response.status(400).json({'error': 'ID not found'});
		}
	}));

	router.get('/expotokens', async((request, response) => {
		console.log('ENV: ', request.query);
		if (request.query.env) {
			let tokens = [];
			let collection = await(fs.collection('token').where('release_channel', '==', request.query.env)/*.select('expoToken')*/.get());
			collection.forEach((token) => {
				expoToken = token.get('expoToken');
				if(expoToken) {
					tokens.push(expoToken);
				}
			});
			response.json({'tokens': tokens});
		}
		else {
			response.status(400).json({'error': "Release channel not specified"});
		}
	}));

	router.post('/token', async((request, response) => {
		console.log(request.body);
		if (request.body.token) {
			response.json(await(fs.doc('token/' + request.body.token).set(request.body)));
		}
		else {
			response.status(400).json({'error': 'TOKEN not found'});
		}
	}));

	return functions.https.onRequest(smappPush);
}


module.exports = httpFs;
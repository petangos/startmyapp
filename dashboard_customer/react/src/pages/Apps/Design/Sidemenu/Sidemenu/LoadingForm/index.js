// import React from 'react'
import React from "react";
import { Form, Row, Col, Button, Radio, Upload, Icon } from "antd";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Dragger = Upload.Dragger;

@Form.create()
class LoadingForm extends React.Component {
  render() {
    return (
      <Form className="add-post__form mt-3">
        <Row gutter={48}>
          <Col span={6}>
            <FormItem>
              <div className="form-group">
                <label className="add-post__label">
                  <strong>640x960 </strong>
                </label>
                <Dragger>
                  <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                </Dragger>
              </div>
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <div className="form-group">
                <label className="add-post__label">
                  <strong>1242x2208 </strong>
                </label>
                <Dragger>
                  <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                </Dragger>
              </div>
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <div className="form-group">
                <label className="add-post__label">
                  <strong>1536x2048 </strong>
                </label>
                <Dragger>
                  <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                </Dragger>
              </div>
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <div className="form-group">
                <label className="add-post__label">
                  <strong>1600x2560 </strong>
                </label>
                <Dragger>
                  <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                </Dragger>
              </div>
            </FormItem>
          </Col>
        </Row>
        <FormItem>
          <div className="add-post__submit">
            <span className="mr-3">
              <Button type="primary" htmlType="submit">
                Save
              </Button>
            </span>
          </div>
        </FormItem>
      </Form>
    );
  }
}

export default LoadingForm;

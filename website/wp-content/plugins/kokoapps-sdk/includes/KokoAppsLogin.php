<?php

class KokoAppsLogin {

    public static function Login($request, $expiration) {
        $result = array();
        $username = (empty($request['username']) ? null : $request['username']);
        $password = (empty($request['password']) ? null : $request['password']);
        $user = wp_authenticate($username, $password);
        if (!$user->errors) {
            if (!empty($user->data->ID)) {
                $result['token'] = KokoAppsToken::set($user->data->ID, $expiration);
                $result = self::GetInfo($user, $result['token']);
                return new WP_REST_Response($result);
            }
        } else {
            return wp_send_json(new WP_Error('user', 'User error', 'Incorrect password'), 401);
        }
    }

    public static function GetInfo($user, $token) {
        $user_id = $user->ID;
        $result['token'] = $token;

        if (class_exists('WC_Customer')) {
            $customer = new WC_Customer($user_id);
            $data = $customer->get_data();

            $data['uid'] = $data['id'];
            unset($data['id']);

            unset($data['meta_data']);
            unset($data['username']);
            $result['customer'] = $data;
            $result['avatar'] = get_avatar_url($user_id);
            unset($data);
            unset($customer);
        } else {
            //WC Plugin is active
            $result['customer'] = array(
                'uid' => $user_id,
                'email' => $user->data->user_email,
                'date_created' => $user->data->user_registered,
                'display_name' => $user->data->display_name,
                'role' => get_role()
            );
        }
        return $result;
    }

}

const admin = require("firebase-admin");
const functions = require("firebase-functions");
admin.initializeApp(functions.config().firebase);

exports.createProfile = functions.auth.user().onCreate(user => {
  const userObject = {
    email: user.email,
    role: "Customer"
  };
  return admin
    .firestore()
    .doc(`users/${user.uid}`)
    .set(userObject);
});

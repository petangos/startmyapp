<?php
/*
Plugin Name: DirectPay
Description: DirectPay Payment gateway for WooCommerce
Plugin URI: https://www.maxstore.co.il
Author URI: https://www.maxstore.co.il
Author: Maxstore Cloud Platforms
License: One Public Domain
Version: 2.8.2
* WC requires at least: 3.0.0
* WC tested up to: 3.3.5
*/
include('helpers/main.php');

if (!class_exists('wp_auto_update')) {
	require_once ('helpers/wp_autoupdate.php');
}

$plugin_current_version = '2.8.2';
$plugin_remote_path = 'https://update.maxstore.co.il/directpay.php';
$plugin_slug = plugin_basename(__FILE__);
new wp_auto_update ($plugin_current_version, $plugin_remote_path, $plugin_slug);
?>
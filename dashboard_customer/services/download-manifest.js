const admin = require('firebase-admin');
const { initializeApp } = require('./lib/config');
const fs = require('fs-extra');
const path = require('path');
initializeApp();

const project_name = process.argv[2];

admin
  .firestore()
  .collection('applications')
  .where('name', '==', project_name)
  .get()
  .then(snap => snap.docs.find(item => true).data())
  .then(data => data.manifest)
  .then(manifest =>
    fs.writeJsonSync(
      path.join(process.cwd(), 'manifests', `${project_name}.json`),
      manifest
    )
  )

  .then(() => console.log('done'));

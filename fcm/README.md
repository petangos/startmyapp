##Firebase FCM Push

###Description

Provides two functions: **smappPush** and **sendPush**

**smappPush** - creates an HTTPS endpoint 
that accepts `/push` and `/token` POST requests 
and inserts JSON data to the **Firestore** database

`/push` - accepts arbitrary data. Required parameters are **id**, **title**, **body**

`/token` - accepts arbitrary data. Required parameter is **token**


**sendPush** - creates a listener for "onCreate" event on "push" collection in **Firestore**.
When triggered, it takes the data (`title` and `body`) from new push document, 
then takes all tokens from "token" collection (currently with `release_channel` set to `staging` only )
and sends the push using FCM immidiately

#
###Setup

Use nodejs v6.11.5

###

Install firebase-tools, if not installed

`npm install -g firebase-tools`

This will install firebase-tools globally ( root permissions required). 
It may lead to an error later when trying to run tools as regular user, 
due to firebase tools attempt to write log files to global installation directory
So consider to remove -g flag and install locally, or fix permissions in the global install


##

Login with Firebase Account

`firebase login`

##


Install dependencies

`cd functions`

`npm install`

##


List Firebase projects available for deployment

`firebase use`

Or Add a project alias

`firebase use -add`

Or select project for deployment 

`firebase use <projectId|alias>`

##

To test FCM, the Google Service Account File must be provided

`export GOOGLE_APPLICATION_CREDENTIALS="path/to/project-key.json"`

###
Run tests in function shell

`firebase functions:shell`


See https://firebase.google.com/docs/functions/local-emulator for reference



##
Deploy to selected project

`firebase deploy`
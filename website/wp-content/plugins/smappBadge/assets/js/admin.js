jQuery(document).ready(function ($) {

    $("#app_badges_select").change(function () {
        $('.app_badges_discount_field').hide();
        if($( this ).val() == 'discount'){
            $('.app_badges_discount_field').show();
        }
    });


    $("#app_badges_select").trigger('change');
});
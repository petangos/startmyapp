import './Reactotron.config';
import React from 'react';
import ReactDOM from 'react-dom';
import App, { history } from './App';

ReactDOM.render(<App />, document.getElementById('root'));

export default history;

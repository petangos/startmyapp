<?php
/**
 * Project: dev.my.koko-apps.com
 * Date: 9/12/18
 * Time: 2:56 PM
 */

class smappUserDiaryRecord extends  smappUserDiaryHelper {
	private $record_types;

	function __construct() {
		//TODO move to shop manager settings
		$this->record_types = array(
			__('Breakfast', $this->texdomain),
			__('Dinner', $this->texdomain),
			__('Lunch', $this->texdomain),
			__('Snack', $this->texdomain),
		);

	}

	public function getTypes() {
		return $this->record_types;
	}
}
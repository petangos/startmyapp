const admin = require('firebase-admin');
const { dateify, toDate, cleanObject } = require('../utils');

async function getPosts({
  per_page = 20,
  page = 1,
  created_at,
  limit = 20,
  offset = 0,
  category,
  uid,
  ...rest
}) {
  const client = admin.apps[1];
  const qs = rest;
  const collection = client.firestore().collection('posts');

  let query = collection;

  Object.keys(qs).forEach(key => (query = query.where(key, '==', qs[key])));
  if (rest.type === 'meal') query = query.where('author', '==', uid);
  if (category) query = query.where('categories', 'array-contains', category);
  if (per_page) query = query.limit(parseInt(per_page));
  if (limit) query = query.limit(parseInt(limit));
  if (created_at)
    query = query
      .where(
        'created_at',
        '>=',
        admin.firestore.Timestamp.fromDate(toDate(created_at))
      )
      .where(
        'created_at',
        '<=',
        admin.firestore.Timestamp.fromDate(toDate(created_at, 1))
      );
  return query.get().then(res => {
    return res.docs.map(doc => {
      const post = doc.data();
      post.id = doc.id;
      if (post.created_at) post.created_at = dateify(post.created_at.toDate());

      return post;
    });
  });
}

function getPost(id) {
  const client = admin.apps[1];

  return client
    .firestore()
    .collection('posts')
    .doc(id)
    .get()
    .then(doc => {
      const post = doc.data();
      post.id = doc.id;
      if (post.created_at) post.created_at = dateify(post.created_at.toDate());
      return post;
    });
}

function addPost(args) {
  const client = admin.apps[1];

  const post = cleanObject(args.post);
  if (post.created_at)
    post.created_at = admin.firestore.Timestamp.fromDate(
      toDate(post.created_at)
    );
  return client
    .firestore()
    .collection('posts')
    .add(post)
    .then(res =>
      res.get().then(doc => {
        const post = doc.data();
        post.id = doc.id;
        if (post.created_at)
          post.created_at = dateify(post.created_at.toDate());
        return post;
      })
    );
}

function editPost(args) {
  const client = admin.apps[1];

  const post = cleanObject(args.post);
  if (post.created_at)
    post.created_at = admin.firestore.Timestamp.fromDate(
      toDate(post.created_at)
    );
  delete post.id;
  return client
    .firestore()
    .collection('posts')
    .doc(args.post.id)
    .update(post)
    .then(() =>
      client
        .firestore()
        .collection('posts')
        .doc(args.post.id)
        .get()
        .then(res => ({ id: args.post.id, ...res.data() }))
    );
}

function deletePost(id) {
  const client = admin.apps[1];

  return client
    .firestore()
    .collection('posts')
    .doc(id)
    .delete()
    .then(() => ({ type: 'success' }))
    .catch(() => ({ type: 'fail' }));
}

function mapPostResponse(post) {}
module.exports = {
  getPosts,
  addPost,
  deletePost,
  getPost,
  editPost,
  mapPostResponse
};

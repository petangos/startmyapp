const express = require('express');
const createServer = require('./graphql/server');
import { push } from './push';
import bodyParser from 'body-parser';
const cors = require('cors');
const graphqlServer = createServer();
const { initializeApp } = require('./config');

initializeApp();

const app = express();
app.use(cors({ origin: '*' }));
graphqlServer.applyMiddleware({ app, path: '/api' });
app.use(bodyParser.json());
const port = 8889;

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/redirect', (req, res) => {
  const { path } = req.query;
  return res.redirect(path);
});

app.post('/push', push);

app.listen(port, () =>
  console.log(`KokoApps Server is running on port: ${port}!`)
);

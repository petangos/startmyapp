��    4      �  G   \      x     y     �     �  ?   �  "   �            	   (     2  	   ?     I     P     X     d     u     ~  '   �     �  $   �     �  ,     3   ;  
   o  
   z     �     �     �     �  A   �     �            	   "     ,     <  
   T     _     k          �     �  	   �     �     �     �     �     �     �       	          �  9     �	  !   
     *
  f   >
  1   �
     �
     �
               %     9     B     V     u     �     �  %   �  '   �  =        K  A   a  C   �  
   �     �           "  "   C     f  _   |     �  $   �  "        <     T     f     �     �  $   �     �     �          .     @     I  
   V  
   a     l     �     �     �  6   �         /                0              %            -      )   2   +                         4      3   (                             *   .                         "          !   &       ,       
             $      '       #   1       	                   Add New Channel Add or remove channels All Channels Are you sure you want to send a push to the Production channel? Choose from the most used channels Create Time Create push Duplicate Edit Channel Edit push Failed Gateway Gateway URL New Channel Name New push No channels found No post to duplicate has been supplied! No push notifications found No push notifications found in trash Notification Content Please provide at least either title or body Post cloning failed, could not find original post:  Production Project ID Push Push Channel Push Channels Push Method:  Push Notification Channel is a new feature available in Android 8 Push Notifications Push Settings Push failed Push sent Release Channel Release Channel not set SMAPP Push SMAPP Push  SMAPP Push Settings Search Channels Search push notifications Select Release Channel ... Send push Sent Settings Staging Title Unknown Error Update Channel View Channel View push You can't delete sent push Project-Id-Version: 
POT-Creation-Date: 2018-09-16 18:08+0300
PO-Revision-Date: 2018-09-16 18:11+0300
Last-Translator: Alexander EP <alex.petangos@gmail.com>
Language-Team: 
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.9
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: smappPush.php
 הוסף ערוץ חדש הוסף או הסר ערוצים כל הערוצים האם אתה בטוח ברצונך לשלוח הודעת דחיפה לערוץ הפצה "ייצור"? בחר מהערוצים הנפוצים ביותר זמן הוספה צור הודעת דחיפה שיכפול ערוך ערוץ ערוך הודעה נכשל ממסר דחיפה כתובת ממסר דחיפה שם הערוץ החדש הודעה חדשה לא נמצאו ערוצים לא הוזן פוסט לשיכפול לא נמצאו הודעות דחיפה לא נמצאו הודעות דחיפה בסל המחיזור תוכן ההודעה נא לכתוב לפחות כותרת או גוף של הודעה שכפול פוסט נכשל, לא נמצא הפוסט המקורי ייצור מזהה הפרוייקט הודעת דחיפה ערוץ הודעות דחיפה ערוצי הודעות דחיפה שיטת המשלוח ערוץ הודעות דחיפה הינה תכונה חדשה הזמינה באנדרוייד 8 הודעות דחיפה הגדרות הודעות דחיפה שליחת ההודעה נכשלה ההודעה נשלחה ערוץ הפצה ערוץ הפצה לא נבחר מבצעים והודעות הודעת דחיפה  הגדרות הודעות דחיפה חפש ערוצים חפש הודעות דחיפה בחר ערוץ הפצה ... שלח הודעה נכשל הגדרות הדמיה כותרת שגיאה לא מוכרת עדכן ערוץ הצג ערוץ הצג הודעה אין אפשרות למחוק הודעות דחיפה 
// import React from 'react'
import React from 'react';
import { Editor } from '@tinymce/tinymce-react';
import { Form, Field } from 'react-final-form';
import { message } from 'antd';
import { withFirebase } from '../../../../../../ducks/firebase';
import { withRouter } from 'react-router-dom';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Input, Button } from 'antd';
import moment from 'moment';

@withRouter
@withFirebase
class AddForm extends React.Component {
  constructor(props) {
    super(props);
    this.uploadImage = this.uploadImage.bind(this);
    this.state = { data: {} };
  }
  unsubscribe = () => null;

  async componentDidMount() {
    if (this.props.post_id) this.getScreenData();
  }

  async getScreenData() {
    const { post_id } = this.props;

    this.unsubscribe = this.props.firebase.client
      .firestore()
      .collection('posts')
      .doc(post_id)
      .onSnapshot(snap => {
        const data = snap.data();

        this.setState({
          data: {
            title: data.title,
            name: data.name,
            content: data.content
          }
        });
      });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.props.history.goBack();
    }
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onSubmit(values) {
    const { post_id } = this.props;

    const docValues = {
      ...values,
      name: values.name || values.title,
      type: 'screen'
    };

    const collection = this.props.firebase.client
      .firestore()
      .collection('posts');
    const doc = post_id ? collection.doc(post_id) : collection.doc();
    doc
      .set(docValues, { merge: true })
      .then(() => message.success('Saved changes'));
  }

  uploadImage(img) {
    return new Promise((resolve, reject) => {
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(e => {
          console.log(e);
          reject();
        })
        .then(snap => {
          snap.ref
            .getDownloadURL()
            .then(url => resolve({ data: { link: url } }));
        });
    });
  }

  render() {
    const { form } = this.props;
    return (
      <Form
        className="add-post__form mt-3"
        onSubmit={val => this.onSubmit(val)}
        initialValues={this.state.data}>
        {({ handleSubmit, submitting, valid, dirtySinceLastSubmit }) => {
          return (
            <form onSubmit={handleSubmit}>
              <Field name="title">
                {({ input, meta }) => (
                  <div className="form-group">
                    <label className="add-post__label">
                      <strong>Title</strong>
                    </label>
                    <Input placeholder="Post title" {...input} />
                  </div>
                )}
              </Field>

              <Field name="name">
                {({ input }) => (
                  <div className="form-group">
                    <label className="add-post__label">
                      <strong>Slug</strong>
                    </label>
                    <Input placeholder="Post slug" {...input} />
                  </div>
                )}
              </Field>

              <Field name="content">
                {({ input }) => (
                  <div className="form-group">
                    <label className="add-post__label">
                      <strong>Content</strong>
                    </label>
                    <div className="add-post__editor">
                      <Editor
                        init={{
                          images_upload_handler: (
                            blobInfo,
                            success,
                            failure
                          ) => {
                            const data = blobInfo.blob();
                            this.uploadImage(data)
                              .then(({ data: { link } }) => {
                                success(link);
                                return;
                              })
                              .catch(() => {
                                failure('Error uploading image.');
                                return;
                              });
                          },

                          branding: false,
                          directionality: 'rtl',
                          plugins:
                            'image  imagetools lists hr fullscreen emoticons directionality colorpicker autoresize advlist preview'
                        }}
                        apiKey="uyhxbami3y9cf8vwokgogi449nrp9nuw8l9iy6esckja9ghs"
                        value={input.value}
                        onEditorChange={input.onChange}
                      />
                    </div>
                  </div>
                )}
              </Field>

              <div className="add-post__submit">
                <span className="mr-3">
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={submitting}
                    disabled={submitting || !valid}>
                    Save
                  </Button>
                </span>
                {/*            <Button type="danger" htmlType="submit">
              Discard
            </Button> */}
              </div>
            </form>
          );
        }}
      </Form>
    );
  }
}

export default AddForm;

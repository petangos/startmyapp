import React from 'react';
import { Route } from 'react-router-dom';
import { ConnectedSwitch } from 'reactRouterConnected';
import Loadable from 'react-loadable';
import Page from 'components/LayoutComponents/Page';
import NotFoundPage from 'pages/DefaultPages/NotFoundPage';
import HomePage from 'pages/DefaultPages/HomePage';
import firebase from 'firebase/app';
import 'firebase/firestore';
const loadable = loader =>
  Loadable({
    loader,
    delay: false,
    loading: () => null
  });

function getRoutesFromPostTypes(postTypes) {
  return postTypes.reduce((acc, cur) => {
    return {
      ...acc,
      [`/${cur}s`]: {
        component: loadable(() => import('pages/Apps/Posts/PostsListPage'))
      },
      [`/${cur}s/create`]: {
        component: loadable(() => import('pages/Apps/Posts/PostEditPage'))
      },
      [`/${cur}s/:post_id`]: {
        component: loadable(() => import('pages/Apps/Posts/PostEditPage'))
      }
    };
  }, {});
}

const loadableRoutes = {
  '/login': {
    component: loadable(() => import('pages/DefaultPages/LoginPage'))
  },
  '/register': {
    component: loadable(() => import('pages/DefaultPages/RegisterPage'))
  },
  '/pages/lockscreen': {
    component: loadable(() => import('pages/DefaultPages/LockscreenPage'))
  },
  '/profile': {
    component: loadable(() => import('pages/Apps/Profile/ProfilePage'))
  },
  '/dashboard': {
    component: loadable(() => import('pages/Dashboard/DashboardBetaPage'))
  },
  '/kokoapps/dashboard': {
    component: loadable(() => import('pages/Apps/KokoApps/DashboardPage'))
  },
  '/kokoapps/app-list': {
    component: loadable(() => import('pages/Apps/KokoApps/ProductsListPage'))
  },

  '/design/branding': {
    component: loadable(() => import('pages/Apps/Design/Branding'))
  },

  '/events': {
    component: loadable(() => import('pages/Apps/Events/CalendarPage'))
  },

  '/users': {
    component: loadable(() => import('pages/Apps/Users/UsersListPage'))
  },

  '/users/:uid': {
    component: loadable(() => import('pages/Apps/Users/UserDetailsPage'))
  },
  '/screens': {
    component: loadable(() => import('pages/Apps/Screens/ScreensListPage'))
  },
  '/screens/edit/:post_id?': {
    component: loadable(() => import('pages/Apps/Screens/ScreenEditPage'))
  },
  '/categories/:category': {
    component: loadable(() =>
      import('pages/Apps/Categories/CategoriesListPage')
    )
  },
  '/categories/:category/edit/:cat_id?': {
    component: loadable(() => import('pages/Apps/Categories/CategoryEditPage'))
  },
  '/push-notifications': {
    component: loadable(() =>
      import('pages/Apps/PushNotifications/PushNotificationsListPage')
    )
  }
};

class Routes extends React.Component {
  timeoutId = null;
  state = {
    post_types: []
  };

  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged(async user => {
      if (!user) return;
      const userDoc = await firebase
        .firestore()
        .collection('users')
        .doc(user.uid)
        .get();
      const userRecord = userDoc.data();
      const appDoc = await firebase
        .firestore()
        .collection('applications')
        .doc(userRecord.whitelist[0])
        .get();
      const appData = appDoc.data();
      this.setState({ post_types: appData.post_types });
    });
    this.timeoutId = setTimeout(
      () =>
        Object.keys(loadableRoutes).forEach(path =>
          loadableRoutes[path].component.preload()
        ),
      5000 // load after 5 sec
    );
  }

  componentWillUnmount() {
    this.unsubscribe();
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const routes = {
      ...loadableRoutes,
      ...getRoutesFromPostTypes(this.state.post_types)
    };

    return (
      <ConnectedSwitch>
        <Route exact path="/" component={HomePage} />
        {Object.keys(routes).map(path => {
          const { exact, ...props } = routes[path];
          props.exact = exact === void 0 || exact || false; // set true as default
          return <Route key={path} path={path} {...props} />;
        })}
        <Route
          render={() => (
            <Page>
              <NotFoundPage />
            </Page>
          )}
        />
      </ConnectedSwitch>
    );
  }
}

export { loadableRoutes };
export default Routes;

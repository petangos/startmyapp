import { createAction, createReducer } from 'redux-act';
import { push } from 'react-router-redux';
import { pendingTask, begin, end } from 'react-redux-spinner';
import { notification } from 'antd';
import { auth } from './../config/firebase/firebase';
import { setFirebaseApp, initFirebaseApps } from './firebase';
import firebase from 'firebase/app';

const REDUCER = 'app';
const NS = `@@${REDUCER}/`;

const _setFrom = createAction(`${NS}SET_FROM`);
const _setLoading = createAction(`${NS}SET_LOADING`);
const _setHideLogin = createAction(`${NS}SET_HIDE_LOGIN`);

export const setUserState = createAction(`${NS}SET_USER_STATE`);
export const setUpdatingContent = createAction(`${NS}SET_UPDATING_CONTENT`);
export const setActiveDialog = createAction(`${NS}SET_ACTIVE_DIALOG`);
export const deleteDialogForm = createAction(`${NS}DELETE_DIALOG_FORM`);
export const addSubmitForm = createAction(`${NS}ADD_SUBMIT_FORM`);
export const deleteSubmitForm = createAction(`${NS}DELETE_SUBMIT_FORM`);
export const setLayoutState = createAction(`${NS}SET_LAYOUT_STATE`);

export const setLoading = isLoading => {
  const action = _setLoading(isLoading);
  action[pendingTask] = isLoading ? begin : end;
  return action;
};

export const resetHideLogin = () => (dispatch, getState) => {
  const state = getState();
  if (state.pendingTasks === 0 && state.app.isHideLogin) {
    dispatch(_setHideLogin(false));
  }
  return Promise.resolve();
};

export const initAuth = roles => async (dispatch, getState) => {
  // Use Axios there to get User Data by Auth Token with Bearer Method Authentication
  const userRole = 'agent';
  const state = getState();

  if (state.routing.location.pathname === '/register') {
    const location = state.routing.location;
    const from = location.pathname + location.search;
    dispatch(_setFrom(from));
    return Promise.reject();
  }

  //alert(state.routing.location.pathname);

  const initClient = () =>
    new Promise((resolve, reject) => {
      auth.onAuthStateChanged(async user => {
        //console.log(user);
        if (user) {
          const userRecord = await firebase
            .firestore()
            .collection('users')
            .doc(user.uid)
            .get()
            .then(res => res.data());

          const apps = await Promise.all(
            userRecord.whitelist.map(appId =>
              firebase
                .firestore()
                .collection('applications')
                .doc(appId)
                .get()
                .then(res => res.data().firebase)
            )
          );

          apps.forEach(app => {
            if (!firebase.apps.find(item => item.name === app.projectId)) {
              const instance = firebase.initializeApp(app, app.projectId);
              instance.firestore().settings({ timestampsInSnapshots: true });
            }
          });

          resolve();
        } else {
          dispatch(logout());
          dispatch(push('/login'));
          reject();
        }
      });
    });

  if (firebase.apps.length < 2) await initClient();
  const restoredProject = window.localStorage.getItem('@kokoapps/projectId');
  const restoredApp = firebase.apps.find(item => item.name === restoredProject);

  dispatch(initFirebaseApps(firebase.apps));
  dispatch(setFirebaseApp(restoredApp || firebase.apps[1]));
  /* if (window.localStorage.getItem('app.Authorization') === '') {
    dispatch(logout());
    dispatch(push('/login'));
    return Promise.reject();
  } */

  const setUser = userState => {
    dispatch(
      setUserState({
        userState: {
          ...userState
        }
      })
    );
    if (!roles.find(role => role === userRole)) {
      if (!(state.routing.location.pathname === '/dashboard')) {
        dispatch(push('/dashboard'));
      }
      return Promise.resolve(false);
    }
    return Promise.resolve(true);
  };

  switch (userRole) {
    case 'administrator':
      return setUser(userRole);

    case 'agent':
      auth.onAuthStateChanged(user => {
        setUser({
          displayName: user.displayName,
          plan: 'Professional',
          role: 'agent'
        });
      });

      return Promise.resolve(true);

    default:
      const location = state.routing.location;
      const from = location.pathname + location.search;
      dispatch(_setFrom(from));
      dispatch(push('/login'));
      return Promise.reject();
  }
};

export function login(username, password, dispatch) {
  // Use Axios there to get User Auth Token with Basic Method Authentication
  if (window.localStorage.getItem('app.Authorization') !== '') {
    dispatch(_setHideLogin(true));
    dispatch(push('/dashboard'));
  } else {
    auth
      .signInWithEmailAndPassword(username, password)
      .then(async user => {
        window.localStorage.setItem('app.Authorization', 'login');
        window.localStorage.setItem('app.Role', 'agent');

        console.log(window.localStorage.getItem('app.Role'));

        //console.log(userRecord);

        dispatch(
          setUserState({
            userState: {
              displayName: user.displayName,
              plan: 'Professional',
              role: 'agent'
            }
          })
        );

        const initClient = () =>
          new Promise((resolve, reject) => {
            auth.onAuthStateChanged(async user => {
              //console.log(user);
              if (user) {
                const userRecord = await firebase
                  .firestore()
                  .collection('users')
                  .doc(user.uid)
                  .get()
                  .then(res => res.data());

                const apps = await Promise.all(
                  userRecord.whitelist.map(appId =>
                    firebase
                      .firestore()
                      .collection('applications')
                      .doc(appId)
                      .get()
                      .then(res => res.data().firebase)
                  )
                );

                apps.forEach(app => {
                  if (
                    !firebase.apps.find(item => item.name === app.projectId)
                  ) {
                    const instance = firebase.initializeApp(app, app.projectId);
                    instance
                      .firestore()
                      .settings({ timestampsInSnapshots: true });
                  }
                });

                resolve();
              } else {
                dispatch(logout());
                dispatch(push('/login'));
                reject();
              }
            });
          });

        if (firebase.apps.length < 2) await initClient();
        dispatch(initFirebaseApps(firebase.apps));
        dispatch(setFirebaseApp(firebase.apps[1]));
        dispatch(_setHideLogin(true));
        dispatch(push('/dashboard'));

        notification.open({
          type: 'success',
          message: 'You have successfully logged in!',
          description:
            'Welcome to the Koko Apps Admin. Make updates to your app in real time!'
        });
      })
      .catch(e => {
        notification.open({
          type: 'success',
          message: 'Invalid email or password'
        });

        console.log('Invalid email or password');
      });
  }

  return false;
}

export const logout = () => (dispatch, getState) => {
  dispatch(
    setUserState({
      userState: {
        email: '',
        role: ''
      }
    })
  );

  auth.signOut().then(
    function() {
      window.localStorage.setItem('app.Authorization', '');
      window.localStorage.setItem('app.Role', '');
      dispatch(push('/login'));
    },
    function(error) {
      // An error happened.
    }
  );
};

export const switchApp = () => (dispatch, id) => {
  console.log('switch app');
  alert(id);

  window.localStorage.setItem('app.current.id', id);
  console.log(window.localStorage.getItem('app.current.id'));

  dispatch(
    setUserState({
      app: {
        current: id
      }
    })
  );

  //dispatch(push('/dashboard'))
};

const initialState = {
  // APP STATE
  from: '',
  isUpdatingContent: false,
  isLoading: false,
  activeDialog: '',
  dialogForms: {},
  submitForms: {},
  isHideLogin: false,

  // LAYOUT STATE
  layoutState: {
    isMenuTop: false,
    menuMobileOpened: false,
    menuCollapsed: false,
    menuShadow: true,
    themeLight: true,
    squaredBorders: false,
    borderLess: true,
    fixedWidth: false,
    settingsOpened: false
  },

  // USER STATE
  userState: {
    email: '',
    role: ''
  }
};

export default createReducer(
  {
    [_setFrom]: (state, from) => ({ ...state, from }),
    [_setLoading]: (state, isLoading) => ({ ...state, isLoading }),
    [_setHideLogin]: (state, isHideLogin) => ({ ...state, isHideLogin }),
    [setUpdatingContent]: (state, isUpdatingContent) => ({
      ...state,
      isUpdatingContent
    }),
    [setUserState]: (state, { userState }) => ({ ...state, userState }),
    [setLayoutState]: (state, param) => {
      const layoutState = { ...state.layoutState, ...param };
      const newState = { ...state, layoutState };
      window.localStorage.setItem(
        'app.layoutState',
        JSON.stringify(newState.layoutState)
      );
      return newState;
    },
    [setActiveDialog]: (state, activeDialog) => {
      const result = { ...state, activeDialog };
      if (activeDialog !== '') {
        const id = activeDialog;
        result.dialogForms = { ...state.dialogForms, [id]: true };
      }
      return result;
    },
    [deleteDialogForm]: (state, id) => {
      const dialogForms = { ...state.dialogForms };
      delete dialogForms[id];
      return { ...state, dialogForms };
    },
    [addSubmitForm]: (state, id) => {
      const submitForms = { ...state.submitForms, [id]: true };
      return { ...state, submitForms };
    },
    [deleteSubmitForm]: (state, id) => {
      const submitForms = { ...state.submitForms };
      delete submitForms[id];
      return { ...state, submitForms };
    }
  },
  initialState
);

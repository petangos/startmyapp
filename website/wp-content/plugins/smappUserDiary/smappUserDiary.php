<?php
/*
  Plugin Name: SMApp User Diary
  Plugin URI:
  Description: Provides a user diary storage and API interface
  Author: KokoApps
  Version: 1.0.1
 */

if (!defined('ABSPATH')) {
	exit;
}

define('SMAPP_USER_DIARY_PATH', untrailingslashit(plugin_dir_path(__FILE__)));

if (!class_exists('smappUserDiaryHelper')) {
	class smappUserDiaryHelper {
		protected $texdomain = 'smapp-diary';
//		public function __construct() {
//		}
	}
}

if (!class_exists('smappUserDiary')) {

	class smappUserDiary extends smappUserDiaryHelper {


		function __construct() {
			require_once(SMAPP_USER_DIARY_PATH . "/includes/smappUserDiaryRecord.php");
			add_action('init', array($this, 'init'));
			add_action('rest_api_init', array($this, 'rest_api_init'));
		}

		function init() {

		}

		public function rest_api_init() {
			register_rest_route( 'kokoapps/v2', 'diary/(?P<uid>\d+)',
				array(
					'methods' => WP_REST_Server::READABLE,
					'callback' => array($this, 'getDiary'),
					'args' => array(
						'uid' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);

			register_rest_route( 'kokoapps/v2', 'diary/(?P<uid>\d+)',
				array(
					'methods' => WP_REST_Server::CREATABLE,
					'callback' => array($this, 'setDiary'),
					'args' => array(
						'uid' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);
		}

		/* @param WP_REST_Request $request*/
		public function getDiary($request) {

			$params =  $request->get_params();
			$result = array();
			if (! $user = get_user_by('ID', $params['uid']))  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			else {
				$diary = $this->getUserDiary($user->ID);
				$result = array (
					"code" => "ok",
					"message" => "Success",
					"data" => $diary,
					"status" => 200
				);
			}

			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}

		/* @param WP_REST_Request $request*/
		public function setDiary($request) {

			$params =  $request->get_params();
			$result = array();
			if (! $user = get_user_by('ID', $params['uid']))  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			else {
				$diary = $this->setUserDiary($user->ID, $request);
				$result = array (
					"code" => "ok",
					"message" => "Success",
					"data" => $diary,
					"status" => 200
				);
			}

			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}

		private function getUserDiary($uid) {
			$week = get_user_meta($uid, 'smapp_diary_week', true);
			return $week;
		}

		/* @param WP_REST_Request $request*/
		private function setUserDiary($uid, $request) {
			$params = $request->get_json_params();
			return update_user_meta($uid, 'smapp_diary_week', $params);

		}

	}
	$smappUserDiary = new smappUserDiary();
}

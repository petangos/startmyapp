import React from 'react';
import {
  //  Button,
  Icon,
  Spin,
  Tabs
} from 'antd';
import './style.scss';
import Avatar from 'components/CleanComponents/Avatar';

import { withFirebase } from '../../../../../ducks/firebase';

const TabPane = Tabs.TabPane;

const Info = React.lazy(() => import('./Info'));
const Meals = React.lazy(() => import('./Meals'));
const Events = React.lazy(() => import('./Events'));
//const Push = React.lazy(() => import('./Push'));

@withFirebase
class UserDetails extends React.Component {
  state = {
    user: {}
  };

  componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.getUser();
    }
  }

  async getUser() {
    const user = await this.props.firebase.client
      .firestore()
      .collection('users')
      .doc(this.props.uid)
      .get()
      .then(doc => doc.data());
    this.setState({
      user: { ...user }
    });
  }
  componentDidMount() {
    this.getUser();
  }

  render() {
    const { full_name, avatar } = this.state.user;
    return (
      <div className="profile">
        <div className="row">
          <div className="col-xl-3">
            <div
              className="card profile__header"
              style={{ backgroundImage: 'url(' + avatar + ')' }}>
              <div className="profile__header-card">
                <div className="card-body text-center">
                  <Avatar
                    src={avatar || 'resources/images/avatars/temp.jpg'}
                    size="110"
                    border="true"
                    borderColor="white"
                  />
                  <br />
                  <br />
                </div>
              </div>
            </div>

            {/*   <Button type="danger">Ban User</Button> */}
          </div>
          <div className="col-xl-9">
            <div className="card profile__social-info">
              <div className="profile__social-name">
                <h2>
                  <span className="text-black mr-2">
                    <strong>{full_name}</strong>
                  </span>
                </h2>
                {/** <p className="mb-1">{lastname}</p>*/}
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <React.Suspense
                  fallback={
                    <Spin
                      indicator={
                        <Icon type="loading" style={{ fontSize: 24 }} spin />
                      }
                    />
                  }>
                  <Tabs defaultActiveKey="1">
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-cog" /> Info
                        </span>
                      }
                      key="1">
                      <Info data={this.state.user} />
                    </TabPane>
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-spoon-knife" /> Meals
                        </span>
                      }
                      key="2">
                      <Meals uid={this.props.uid} />
                    </TabPane>
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-calendar" /> Events
                        </span>
                      }
                      key="3">
                      <Events uid={this.props.uid} />
                    </TabPane>
                    {/* <TabPane
                      tab={
                        <span>
                          <i className="icmn-bell" /> Push Message
                        </span>
                      }
                      key="4">
                      <Push data={this.state.user} />
                    </TabPane> */}
                  </Tabs>
                </React.Suspense>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserDetails;

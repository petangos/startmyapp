import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import UsersList from './UsersList/';

class UsersListPage extends React.Component {
  static defaultProps = {
    pathName: 'Users List',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    return (
      <Page {...props}>
        <Helmet title={`Users List`} />
        <UsersList />
      </Page>
    );
  }
}

export default UsersListPage;

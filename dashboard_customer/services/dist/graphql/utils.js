"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const moment = require('moment');

module.exports = {
  dateify: date => {
    const time = moment(date).format('DD-MM-YYYY HH:mm');
    return time;
  },
  toDate: (date, delta) => {
    if (!date) return null;
    const time = moment(date, 'DD-MM-YYYY HH:mm').add(delta, 'd');
    return time.toDate();
  },
  cleanObject: obj => {
    return Object.keys(obj).reduce((acc, cur) => {
      if (obj[cur]) return _objectSpread({}, acc, {
        [cur]: obj[cur]
      });
    }, {});
  }
};
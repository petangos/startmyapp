const admin = require('firebase-admin');
const { dateify, toDate, cleanObject } = require('../utils');

module.exports = {
  getCategories(args) {
    const client = admin.apps[1];

    let query = client.firestore().collection('categories');

    if (args.post_type) query = query.where('post_type', '==', args.post_type);
    return query.get().then(res =>
      res.docs.map(doc => {
        return {
          id: doc.id,
          ...doc.data()
        };
      })
    );
  }
};

import React from 'react'
import Page from '../../../../components/LayoutComponents/Page'
import Helmet from 'react-helmet'
import ArticleAddPost from './ArticleAddPost'

class ArticleAddPostPage extends React.Component {
  static defaultProps = {
    pathName: 'Add Blog Post',
    roles: ['agent', 'administrator'],
  }

  render() {
    const props = this.props
    return (
      <Page {...props}>
        <Helmet title="Add Blog Post" />
        <ArticleAddPost />
      </Page>
    )
  }
}

export default ArticleAddPostPage

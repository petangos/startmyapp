import { createReducer, createAction } from "redux-act";
import * as app from "./app";
import { message } from "antd";
import { auth } from "../config/firebase";
import firebase from "firebase";

export const REDUCER = "ecommerce";

export function registerForProducts() {
  return function(dispatch, getState) {
    console.log("here");
  };
}

export function registerForCategories() {
  return function(dispatch, getState) {};
}

const initialState = {
  categories: [],
  products: []
};
export default createReducer({}, initialState);

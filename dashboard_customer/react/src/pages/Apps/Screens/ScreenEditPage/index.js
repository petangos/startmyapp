import React from 'react';
import Page from 'components/LayoutComponents/Page';
import Helmet from 'react-helmet';
import ScreenEdit from './ScreenEdit';

class ScreenEditPage extends React.Component {
  static defaultProps = {
    pathName: 'Add Blog Post',
    roles: ['agent', 'administrator']
  };

  render() {
    const props = this.props;
    const postType = props.match.path.split('/')[1];
    return (
      <Page {...props}>
        <Helmet title={`${postType} Add/Edit`} />
        <ScreenEdit post_id={this.props.match.params.post_id} />
      </Page>
    );
  }
}

export default ScreenEditPage;

const admin = require('firebase-admin');
const { initializeApp } = require('./lib/config');
const fs = require('fs-extra');
const path = require('path');
initializeApp();

const file = process.argv[2];

const secrets = fs.readJSONSync(path.join(process.cwd(), file));

admin
  .firestore()
  .collection('applications')
  .where('name', '==', secrets.project_id)
  .get()
  .then(snap => snap.docs.find(item => true).ref)
  .then(doc => {
    doc
      .set({ firebase_secrets: secrets }, { merge: true })
      .then(() => console.log('done'));
  });

import React, { Component } from 'react';
import { Login } from './Login';
import { Project } from './Project';
import { ThemeProvider } from 'styled-components';
import { Wrapper, Center } from './components';
import { Button, CircularProgress, CssBaseline } from '@material-ui/core';
import { ExitToApp } from '@material-ui/icons';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import { auth } from './firebase';
import { ProjectsList } from './ProjectsList';
const theme = {
  background: '#FAFFFFFA',
  grey: '#f4edec',
  text: '#00A00A',
  primary: 'blue'
};

const Auth = () => {
  if (auth.currentUser) return <Redirect to="/" />;
  return <Login />;
};
const Dashboard = props => {
  if (!auth.currentUser) return <Redirect to="/login" />;
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route
            path={'/dashboard'}
            exact
            component={() => <Redirect to={'/dashboard/projects'} />}
          />
          <Route path={'/dashboard/projects'} exact component={ProjectsList} />
          <Route path={'/dashboard/projects/:project_id'} component={Project} />
        </Switch>
      </Router>
      <Button variant="fab" onClick={() => auth.signOut()}>
        <ExitToApp />
      </Button>
    </React.Fragment>
  );
};

class App extends Component {
  state = {
    loading: true
  };
  constructor(props) {
    super(props);
    auth.onAuthStateChanged(user => {
      this.setState({ loading: false });
    });
  }
  render() {
    if (this.state.loading)
      return (
        <Wrapper background>
          <div
            style={{
              flex: 1,
              display: 'flex'
            }}>
            <CssBaseline />
            <Center>
              <CircularProgress />
            </Center>
          </div>
        </Wrapper>
      );
    return (
      <ThemeProvider theme={theme}>
        <Wrapper background>
          <CssBaseline />
          <div
            style={{
              flex: 1,
              minWidth: '100vw',
              minHeight: '100vh',
              display: 'flex'
            }}>
            <Router>
              <Switch>
                <Route
                  path="/"
                  exact
                  component={() => (
                    <Redirect to={auth.currentUser ? '/dashboard' : '/login'} />
                  )}
                />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/login" component={Auth} />
              </Switch>
            </Router>
          </div>
        </Wrapper>
      </ThemeProvider>
    );
  }
}

export default App;

const fs = require('fs-extra');
const path = require('path');
const admin = require('firebase-admin');
const cliSelect = require('cli-select');
const { execSync } = require('child_process');
const { kokoSwitch } = require('./switch');
const program = require('commander');
const FIREBASE_SERVICE_ACCOUNT = require('./koko-apps-firebase-admin.json');

const REPO_URL = 'git@bitbucket.org:petangosltd/kokoapps-templates.git';
const TEMP_PATH = path.join(process.cwd(), 'tmp');

if (admin.apps.length === 0) {
  admin.initializeApp({
    credential: admin.credential.cert(FIREBASE_SERVICE_ACCOUNT),
    databaseURL: 'https://koko-apps.firebaseio.com'
  });
  const firestore = new admin.firestore();
  const settings = { timestampsInSnapshots: true };
  firestore.settings(settings);
}

function clone() {
  if (fs.existsSync(TEMP_PATH)) {
    console.log('syncing repository');
    execSync('git reset --hard origin/master', { cwd: TEMP_PATH });
  } else {
    console.log('cloning repository...');
    execSync(`git clone ${REPO_URL} ${TEMP_PATH}`);
  }

  console.log('repository up to date');
}

function installDependencies() {
  console.log('installing dependencies...');
  execSync(`cd store && yarn`, { cwd: TEMP_PATH });
  console.log('Dependencies installed successfuly');
}

async function getProjects() {
  try {
    const projects = await admin
      .firestore()
      .collection('applications')
      .get()
      .then(snap => {
        return snap.docs.map(item => item.data());
      });
    return Promise.resolve(projects);
  } catch (e) {
    return Promise.reject();
  }
}

async function publish() {
  console.clear();
  console.log('initializing projects list...');
  const projects = await getProjects();
  const project = (await cliSelect({
    values: projects,
    valueRenderer: (value, selected) => value.name
  })).value;
  clone();
  installDependencies();
  await prepareProject(project.name);
  expoPublish();
  tearDown();
  return 0;
}

async function prepareProject(project_name) {
  console.log(`preparing ${project_name}...`);
  try {
    await kokoSwitch(project_name, TEMP_PATH);
    console.log(`${project_name} is ready.`);
    return Promise.resolve();
  } catch (e) {
    return Promise.reject();
  }
}

async function collectResources(project) {
  console.log(project.manifest);
}

function tearDown() {
  console.log('tearing down...');
  fs.removeSync(TEMP_PATH);
  console.log('done!');
}

function expoPublish(callback) {
  console.log('publishing js bundle using expo...');
  execSync('expo publish --release-channel staging', {
    cwd: path.join(TEMP_PATH, 'store')
  });
  console.log('published !');
}

publish();

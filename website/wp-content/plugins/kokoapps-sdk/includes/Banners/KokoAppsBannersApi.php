<?php

class KokoAppsBannersApi extends KokoAppsBannersHelp {

    public function __construct() {

        add_action('rest_api_init', function () {
            register_rest_route('kokoapps/v2/', $this->slug_api, array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(&$this, 'KokoApps_api'),
                'args' => array(
                ),
            ));
        });
    }

    public function KokoApps_api() {
        $posts = get_posts(array(
            'post_type' => $this->post_type
        ));

        if (empty($posts)) {
            return new WP_Error('No Data', 'Invalid', array('status' => 200));
        }

        $temp = array();
        foreach ($posts as $item) {
            $image_id = get_post_thumbnail_id($item->ID);

            $item->images = array(
                array(
                    'id' => (int) $image_id,
                    'date_created' => wc_rest_prepare_date_response($item->post_date),
                    'date_created_gmt' => wc_rest_prepare_date_response($item->post_date_gmt),
                    'date_modified' => wc_rest_prepare_date_response($item->post_modified),
                    'date_modified_gmt' => wc_rest_prepare_date_response($item->post_modified_gmt),
                    'src' => wp_get_attachment_url($image_id),
                    'title' => '',
                    'alt' => '',
                )
            );

            $temp[] = $item;
        }

        return $temp;
    }

}

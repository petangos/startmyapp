const { gql } = require('apollo-server-express');

module.exports = gql`
  enum Recurrency {
    once
    daily
    weekly
    monthly
    yearly
  }

  type Event {
    id: ID
    title: String
    short_description: String
    description: String
    capacity: Int
    recurrency: Recurrency
    is_full_day: Boolean
    is_open: Boolean
    is_user_attending: Boolean
    start_time: String
    end_time: String
    location: String
    participants: [Participant]
    media: [Media]
  }

  type Participant {
    id: ID!
    name: String
    avatar: Media
  }

  type Media {
    id: ID
    type: String!
    src: String!
    alt: String
  }

  input MediaInput {
    "type of media item"
    type: String!
    "source of media item"
    src: String!
    "alternate text of media item"
    alt: String
  }

  type Status {
    type: String!
    error: String
  }

  type Post {
    id: ID!
    type: String!
    created_at: String
    author: String
    title: String!
    description: String
    content: String
    metadata: [Metadata]
    media: [Media]
    links: [Link]
  }

  type Link {
    href: String
    rel: String
    type: String
    resource: Post
  }

  input PostInput {
    type: String!
    created_at: String
    author: String
    title: String!
    description: String
    content: String
    metadata: [MetadataInput]
    media: [MediaInput]
  }

  input PostEditInput {
    id: ID!
    type: String!
    created_at: String
    author: String
    title: String!
    description: String
    content: String
    metadata: [MetadataInput]
    media: [MediaInput]
  }

  type Metadata {
    icon: String
    key: String
    value: String
  }

  input MetadataInput {
    icon: String
    key: String
    value: String
  }

  type User {
    uid: ID
    full_name: String
    email: String
    phone: String
    age: String
    avatar: String
    weight: String
    height: String
    activity: String
    target: String
    gender: String
  }

  type Category {
    id: ID!
    name: String
    label: String
    media: [Media]
    parent: Int
  }

  type Query {
    event(id: ID!): Event
    events(
      day: String
      month: String
      limit: Int
      offest: Int
      attending: Boolean
    ): [Event]

    user: User

    posts(
      id: ID
      type: String
      category: String
      author: String
      featured: Boolean
      per_page: Int
      limit: Int
      status: String
      offest: Int
      created_at: String
    ): [Post]
    post(id: ID!): Post

    categories(post_type: String): [Category]
  }

  type Mutation {
    reportOpenedNotification(notId: String!): Status

    addEvent(
      title: String!
      short_description: String
      description: String
      start_time: String!
      capacity: String
      is_open: Boolean
      end_time: String
      location: String
      media: [MediaInput]
    ): Event

    editPost(post: PostEditInput): Post

    addPost(post: PostInput): Post

    deletePost(id: ID!): Status

    deleteEvent(id: ID!): Status

    registerForEvent(uid: ID!, eid: ID!): Status

    unregisterFromEvent(uid: ID!, eid: ID!): Status

    setUserProfile(
      id: ID!
      weight: Int
      age: Int
      activity: String
      target: String
      height: Int
      gender: String
      full_name: String
      phone: String
      email: String
      avatar: String
    ): Status
  }
`;

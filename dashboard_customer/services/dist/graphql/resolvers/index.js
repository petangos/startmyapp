"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const _events = require('./events');

const _posts = require('./posts');

const _categories = require('./categories');

const _require = require('../utils'),
      toDate = _require.toDate,
      dateify = _require.dateify;

const admin = require('firebase-admin');

const _require2 = require('apollo-server-express'),
      AuthenticationError = _require2.AuthenticationError;

const resolvers = {
  Query: {
    events: (root, args, context) => {
      return _events.getEvents(args, context);
    },
    event: (root, args) => _events.getEvent(args.id),
    posts: (root, args, {
      uid
    }) => _posts.getPosts(_objectSpread({}, args, {
      uid
    })),
    user: async (root, args, {
      uid
    }) => {
      const client = admin.apps[1];
      if (!uid) throw new AuthenticationError('you must be logged in');
      const user = await client.firestore().collection('users').doc(uid).get().then(res => _objectSpread({
        uid: res.id
      }, res.data()));
      return user;
    },
    post: (root, args) => _posts.getPost(args.id),
    categories: (root, args) => _categories.getCategories(args)
  },
  Post: {
    links: async (parent, args) => {
      const client = admin.apps[1];
      return [];
      const collection = client.firestore().collection('posts').orderBy('created_at');
      const next = parent && parent.created_at ? await collection.startAfter(admin.firestore.Timestamp.fromDate(toDate(parent.created_at))).get().then(res => res.docs.map(doc => {
        const post = doc.data();
        post.id = doc.id;
        if (post.created_at) post.created_at = dateify(post.created_at.toDate());
        return post;
      })[0]) : null;
      const prev = parent && parent.created_at ? await collection.endBefore(admin.firestore.Timestamp.fromDate(toDate(parent.created_at))).get().then(res => res.docs.map(doc => {
        const post = doc.data();
        post.id = doc.id;
        if (post.created_at) post.created_at = dateify(post.created_at.toDate());
        return post;
      })[0]) : await collection.get().then(res => res.docs.map(doc => {
        const post = doc.data();
        post.id = doc.id;
        if (post.created_at) post.created_at = dateify(post.created_at.toDate());
        return post;
      })[0]);
      return [{
        rel: 'next',
        resource: next
      }, {
        rel: 'prev',
        resource: prev
      }];
    }
  },
  Event: {
    is_user_attending: (parent, args, context) => {
      const client = admin.apps[1];
      return context.uid ? parent.participants && parent.participants.length && parent.participants.includes(context.uid) : false;
    },
    participants: parent => {
      const client = admin.apps[1];
      return parent.participants ? parent.participants.map(uid => client.auth().getUser(uid).then(user => ({
        id: user.uid,
        name: user.displayName,
        avatar: {
          type: 'image',
          src: user.photoURL,
          alt: ''
        }
      }))) : [];
    }
  },
  Mutation: {
    ///events
    addEvent: (root, args) => _events.addEvent(args),
    deleteEvent: (root, args) => _events.deleteEvent(args.id),
    registerForEvent: (root, args) => _events.registerForEvent(args, root),
    unregisterFromEvent: (root, args) => _events.unregisterFromEvent(args),
    ///posts
    addPost: (root, args) => _posts.addPost(args),
    deletePost: (root, args) => _posts.deletePost(args.id),
    editPost: (root, args) => _posts.editPost(args),
    setUserProfile: (root, args) => {
      const client = admin.apps[1];

      const values = _objectSpread({}, args);

      delete values['id'];
      return client.firestore().collection('users').doc(args.id).set(values, {
        merge: true
      }).catch(() => ({
        type: 'fail'
      })).then(() => ({
        type: 'success'
      }));
    },
    // utils
    reportOpenedNotification: (root, {
      notId
    }, {
      uid
    }) => {
      const client = admin.apps[1];
      const notRef = client.firestore().collection('push').doc(notId);
      return client.firestore().runTransaction(transaction => {
        return transaction.get(notRef).then(doc => {
          transaction.update(notRef, {
            recipients: doc.data().recipients.map(item => item.uid === uid ? _objectSpread({}, item, {
              opened: true
            }) : item)
          });
        });
      }).catch(() => ({
        type: 'fail'
      })).then(() => ({
        type: 'success'
      }));
    }
  }
};

function getLinksForPost(parent) {
  console.log(parent);
}

module.exports = resolvers;
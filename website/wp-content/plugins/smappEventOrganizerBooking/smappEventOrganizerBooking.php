<?php
/*
  Plugin Name: SMApp Event Organizer Booking
  Plugin URI:
  Description: Extends  Event Organizer
  Author: KokoApps
  Version: 1.0.1
 */

if (!defined('ABSPATH')) {
	exit;
}


if (!class_exists('smappEventOrganizerBooking')) {

	class smappEventOrganizerBooking {

		private $textdomain = 'smapp-event';

		function __construct() {
			add_action( 'init', array($this, 'init'), 25 );
			add_action( 'rest_api_init', array($this, 'enableMetaRestSupport') );
			add_action( 'rest_api_init', array($this, 'enableEventsRestSupport') );
			add_action( 'rest_api_init', array($this, 'enableBookingRestSupport') );
//			add_filter('manage_event_posts_columns', array($this, 'set_event_list_columns'));
//			add_action('manage_event_posts_custom_column', array($this, 'set_event_list_columns_data'), 10, 2);
//			add_filter('manage_edit-event_sortable_columns', array($this, 'set_event_list_columns_sortable'));
		}


		function init() {
			load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages');
			$this->enableRestSupport();
		}

		function enableRestSupport() {
			global $wp_post_types, $wp_taxonomies;

			//be sure to set this to the name of your post type!
			$post_type_name = 'event';
			if( isset( $wp_post_types[ $post_type_name ] ) ) {
				$wp_post_types[$post_type_name]->show_in_rest = true;
//				// Optionally customize the rest_base or controller class
//				$wp_post_types[$post_type_name]->rest_base = $post_type_name;
//				$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
			}


			//be sure to set this to the name of your taxonomy!
			$taxonomy_names = array('event-category', 'event-tag');
			foreach ($taxonomy_names as $taxoname) {
				if (isset($wp_taxonomies[$taxoname])) {
					$wp_taxonomies[$taxoname]->show_in_rest = true;

//				// Optionally customize the rest_base or controller class
//				$wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
//				$wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
				}
			}
		}


		function enableMetaRestSupport() {

			register_rest_field( 'event', 'meta', array(
					'get_callback' => function( $object ) {
						$dates = $this->calculateEventDates($object['id']);
						return $dates;
					},
					'schema' => null,
				)
			);
			register_rest_field( 'event', 'image', array(
					'get_callback' => function( $object ) {
						$post_id = $object['id'];

						$image_id = get_post_thumbnail_id($post_id);
						return array(
							'id' => (int) $image_id,
							'date_created' => wc_rest_prepare_date_response($object->post_date),
							'date_created_gmt' => wc_rest_prepare_date_response($object->post_date_gmt),
							'date_modified' => wc_rest_prepare_date_response($object->post_modified),
							'date_modified_gmt' => wc_rest_prepare_date_response($object->post_modified_gmt),
							'src' => wp_get_attachment_url($image_id),
							'title' => get_the_title($image_id),
							'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
						);
					},
					'schema' => null,
				)
			);
		}

		function calculateEventDates($post_id) {
			//TODO calculate dates for event

//			$start_date = get_post_meta( $post_id, '_eventorganiser_schedule_start_start', true );
//			$end_date =  get_post_meta( $post_id, '_eventorganiser_schedule_last_start', true );


			return get_post_meta( $post_id);
		}
		function enableEventsRestSupport() {

			register_rest_route( 'wp/v2', 'events(?:/(?P<event_id>\d+))?',
				array(
					'methods' => WP_REST_Server::READABLE,
					'callback' => array($this, 'getEvents'),
					'args' => array(
						'event_id' => array(
							'required' => false,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						)
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);

		}

		/* @param WP_REST_Request $request*/
		function getEvents($request) {
			$params = $request->get_params();
			$result = array();
			$events = eo_get_events();
			foreach ($events as $event) {
				if($image_id = get_post_thumbnail_id($event->ID)) {
					$event->image = array(
						'id'    => (int)$image_id,
						'src'   => wp_get_attachment_url($image_id),
						'title' => get_the_title($image_id),
						'alt'   => get_post_meta($image_id, '_wp_attachment_image_alt', true),
					);
				}
			}

			$result = array(
				"code"    => "ok",
				"message" => "Success",
				"data"    => $events,
				"status"  => 200
			);
			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}

		function enableBookingRestSupport() {
			register_rest_route( 'wp/v2', 'event/(?P<event_id>\d+)/book',
				array(
					'methods' => WP_REST_Server::CREATABLE,
					'callback' => array($this, 'createBookingForEvent'),
					'args' => array(
						'event_id' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
						'uid' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
						'event_time' => array(
							'required' => true
						),
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);

			register_rest_route( 'wp/v2', 'event/(?P<event_id>\d+)/book',
				array(
					'methods' => WP_REST_Server::DELETABLE,
					'callback' => array($this, 'cancelBookingForEvent'),
					'args' => array(
						'event_id' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
						'uid' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						),
						'event_time' => array(
							'required' => true
						),
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);

			register_rest_route( 'wp/v2', 'event/user/(?P<uid>\d+)',
				array(
					'methods' => WP_REST_Server::READABLE,
					'callback' => array($this, 'getBookingsForUser'),
					'args' => array(
						'uid' => array(
							'required' => true,
							'validate_callback' => function($param, $request, $key) {
								return is_numeric( $param );
							}
						)
					),
					'permission_callback' => function ($request) {
						return current_user_can( 'read' );
					}
				)
			);

		}

		/* @var WP_Post $post */
		/* @param WP_REST_Request $request*/
		function createBookingForEvent($request) {
			$params =  $request->get_params();
			$result = array();
			$user = get_user_by('ID', $params['uid']);
			$post = get_post($params['event_id']);
			$event_time = strtotime($params['event_time']);
			if (! $user)  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			elseif (! $post || $post->post_type != 'event' || $post->post_status != 'publish')  {
				$result = array (
					"code" => "no_event",
					"message" => "Event not found",
					"data" => array(),
					"status" => 404
				);
			}
			elseif ($event_time === false) {
				$result = array (
					"code" => "date_err",
					"message" => "Wrong date format",
					"data" => array(),
					"status" => 401
				);
			}

			else {
				global $wpdb;
				$event_time = date('Y-m-d H:i:s', $event_time);
				$sql = <<<SQL
					INSERT IGNORE INTO `wp_smapp_event_booking`
					(`user_id`, `event_id`, `event_time`) 
					VALUES
					($params[uid], $params[event_id], '$event_time');
SQL;
				$rows = $wpdb->query($sql);
				if ($rows) {
					$result = array (
						"code" => "OK",
						"message" => "Success",
						"data" => array($rows),
						"status" => 200
					);
				}
				elseif ($rows !== false){
					$result = array (
						"code" => "warn",
						"message" => "Duplicate booking",
						"data" => array($rows),
						"status" => 200
					);
				}
				else{
					$result = array (
						"code" => "err",
						"message" => "SQL Error",
						"data" => array($rows),
						"status" => 500
					);
				}
			}
			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}


		/* @var WP_Post $post */
		/* @param WP_REST_Request $request*/
		function cancelBookingForEvent($request) {
			$params =  $request->get_params();
			$result = array();
			$user = get_user_by('ID', $params['uid']);
			$post = get_post($params['event_id']);
			$event_time = strtotime($params['event_time']);
			if (! $user)  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			elseif (! $post || $post->post_type != 'event' || $post->post_status != 'publish')  {
				$result = array (
					"code" => "no_event",
					"message" => "Event not found",
					"data" => array(),
					"status" => 404
				);
			}
			elseif ($event_time === false) {
				$result = array (
					"code" => "date_err",
					"message" => "Wrong date format",
					"data" => array(),
					"status" => 401
				);
			}

			else {
				global $wpdb;
				$event_time = date('Y-m-d H:i:s', $event_time);
				$sql = <<<SQL
					DELETE FROM `wp_smapp_event_booking`
					WHERE `user_id` = $params[uid]
						AND `event_id` = $params[event_id]
						AND `event_time` = '$event_time'
					LIMIT 1;
SQL;
				$rows = $wpdb->query($sql);
				if ($rows) {
					$result = array (
						"code" => "OK",
						"message" => "Success",
						"data" => array($rows),
						"status" => 200
					);
				}
				elseif ($rows !== false){
					$result = array (
						"code" => "warn",
						"message" => "Booking not found",
						"data" => array($rows),
						"status" => 200
					);
				}
				else{
					$result = array (
						"code" => "err",
						"message" => "SQL Error",
						"data" => array($rows),
						"status" => 500
					);
				}
			}
			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;
		}

		/* @param WP_REST_Request $request*/
		function getBookingsForUser($request) {
			$params =  $request->get_params();
			$result = array();
			$user = get_user_by('ID', $params['uid']);
			if (! $user)  {
				$result = array (
					"code" => "no_user",
					"message" => "User not found",
					"data" => array(),
					"status" => 404
				);
			}
			else {
				global $wpdb;
				$sql = <<<SQL
					SELECT * FROM `wp_smapp_event_booking`
					WHERE `user_id` = $params[uid];
SQL;
				$query = $wpdb->get_results($sql, ARRAY_A);

				$result = array(
					"code"    => "ok",
					"message" => "Success",
					"data"    => $query,
					"status"  => 200
				);
			}
			$response = new WP_REST_Response( $result );
			$response->set_status( $result['status']);
			return $response;

		}
//
//		function set_event_list_columns($columns) {
//			$columns['bookings'] = __('Bookings', $this->textdomain);
//			return $columns;
//		}
//
//		function set_event_list_columns_data($column, $post_id) {
//			$event = get_post($post_id);
//			if($column == 'bookings') {
//
//
//			}
//		}

	}

	$smappEventOrganizerBooking = new smappEventOrganizerBooking();
}
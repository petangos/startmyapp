const functions = require('firebase-functions');
const fs = require('fs');
const Git = require('nodegit');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.publish = functions.pubsub.topic('publish').onPublish(message => {
  let git = Git.Clone('git@bitbucket.org:petangos/startmyapp.git', 'tmp', {
    callbacks: {
      certificateCheck: function() {
        return 1;
      },
      credentials: function(url, userName) {
        return Git.Cred.sshKeyFromAgent(userName);
      }
    }
  });
  return 'success';
});

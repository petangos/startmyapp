import React from 'react';
import { Table } from 'antd';
//import './style.scss';
import { withFirebase } from '../../../../../../ducks/firebase';
import moment from 'moment';

const defaultPagination = {
  pageSizeOptions: ['10', '50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0
};

@withFirebase
class Meals extends React.Component {
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false
  };

  async getEvents() {
    const { uid } = this.props;
    const events = await this.props.firebase.client
      .firestore()
      .collection('events')
      //.orderBy('created_at', 'desc')
      .where('participants', 'array-contains', uid)
      .get()
      .then(res => res.docs.map(item => ({ key: item.id, ...item.data() })));
    return events.sort((a, b) => (moment(a).isBefore(moment(b)) ? 1 : -1));
  }

  async componentDidMount() {
    this.setState({ data: await this.getEvents() });
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      this.setState({ data: await this.getEvents() });
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  render() {
    let { pager, data } = this.state;
    const columns = [
      {
        title: 'Event',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'Time',
        dataIndex: 'start_time',
        key: 'start_time',
        render: item => moment(item.toDate()).format('DD/MM-YYYY HH:mm')
      }
    ];

    return (
      <div className="card">
        <div className="card-header">
          <div className="utils__title">
            <strong>Meals</strong>
          </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            pagination={pager}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}

export default Meals;

const moment = require('moment');

module.exports = {
  dateify: date => {
    const time = moment(date).format('DD-MM-YYYY HH:mm');
    return time;
  },

  toDate: (date, delta) => {
    if (!date) return null;
    const time = moment(date, 'DD-MM-YYYY HH:mm').add(delta, 'd');
    return time.toDate();
  },

  cleanObject: obj => {
    return Object.keys(obj).reduce((acc, cur) => {
      if (obj[cur]) return { ...acc, [cur]: obj[cur] };
    }, {});
  }
};

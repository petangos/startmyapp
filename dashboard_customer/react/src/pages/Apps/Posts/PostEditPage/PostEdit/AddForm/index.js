// import React from 'react'
import React from 'react';
import { Editor } from '@tinymce/tinymce-react';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { Form, Field } from 'react-final-form';
import { withFirebase } from '../../../../../../ducks/firebase';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {
  Input,
  Button,
  Select,
  Upload,
  Icon,
  Switch,
  Col,
  message
} from 'antd';

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

@withFirebase
@withRouter
class AddForm extends React.Component {
  state = {
    categories: [],
    isImageChanged: false,
    uploadingImage: false,
    data: {
      categories: []
    }
  };

  unsubscribe = () => null;
  constructor(props) {
    super(props);
    this.uploadImage = this.uploadImage.bind(this);
  }

  uploadImage(img) {
    return new Promise((resolve, reject) => {
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(reject)
        .then(snap => {
          snap.ref
            .getDownloadURL()
            .then(url => resolve({ data: { link: url } }));
        });
    });
  }

  uploadMainImage(img) {
    return new Promise((resolve, reject) => {
      this.setState({ uploadingImage: true });
      const storage = this.props.firebase.client.storage().ref();
      storage
        .child(`images/${moment().format('DDMMYYYYHHmmss')}-${img.name}`)
        .put(img)
        .catch(reject)
        .then(snap => {
          snap.ref.getDownloadURL().then(url => {
            this.setState({ uploadingImage: true });
            resolve(url);
          });
        });
    });
  }

  async componentDidMount() {
    this.getCategories();
    if (this.props.postId)
      try {
        const { media, ...rest } = await this.getPost();
        this.setState({
          data: {
            ...rest,
            isPublished: rest.status === 'published',
            media: media ? media[0].src : ''
          }
        });
      } catch (e) {
        console.log(e);
      }
  }

  onSubmit(values) {
    const { isPublished, media, ...rest } = values;
    const docValues = {
      ...rest,

      last_modified: firebase.firestore.Timestamp.now()
    };

    docValues.status = isPublished ? 'published' : 'draft';
    if (!this.props.postId) {
      docValues.created_at = firebase.firestore.Timestamp.now();
      docValues.type = this.props.postType;
    }
    if (media) {
      docValues.media = [{ type: 'image', src: media }];
    }
    if (!(rest.metadata && rest.metadata.length)) docValues.metadata = [];

    const client = this.props.firebase.client;
    const collection = client.firestore().collection('posts');
    const doc = this.props.postId
      ? collection.doc(this.props.postId)
      : collection.doc();
    doc
      .set(docValues, { merge: true })
      .then(() => message.success('Post Saved'))
      .then(this.props.history.goBack);
  }

  async getCategories() {
    const client = this.props.firebase.client;
    this.unsubscribe = client
      .firestore()
      .collection('categories')
      .where('post_type', '==', this.props.postType)
      .onSnapshot(snap => {
        snap.docs.map(item =>
          this.setState({ categories: [...this.state.categories, item.data()] })
        );
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }
  async getPost() {
    const client = this.props.firebase.client;
    const data = await client
      .firestore()
      .collection('posts')
      .doc(this.props.postId)
      .get()
      .then(res => res.data());
    return data;
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.firebase.client.name !== this.props.firebase.client.name) {
      if (this.props.postId) {
        this.props.history.goBack();
        return;
      }
      this.setState({
        categories: [],
        isImageChanged: false,
        data: {
          categories: []
        }
      });
      this.getCategories();
      if (this.props.postId)
        try {
          const { media, ...rest } = await this.getPost();

          this.setState({
            data: {
              ...rest,
              isPublished: rest.status === 'published',
              media: media ? media[0].src : ''
            }
          });
        } catch (e) {
          console.log(e);
        }
    }
  }
  render() {
    return (
      <Form
        className="add-post__form mt-3"
        onSubmit={e => this.onSubmit(e)}
        initialValues={this.state.data}>
        {({ handleSubmit, submitting, valid }) => (
          <form onSubmit={handleSubmit}>
            <Field name="title">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Title</strong>
                  </label>
                  <Input placeholder="Post title" {...input} />
                </div>
              )}
            </Field>
            <Field name="description">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Description</strong>
                  </label>
                  <Input placeholder="Post description" {...input} />
                </div>
              )}
            </Field>
            <Field name="categories">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Category</strong>
                  </label>

                  <Select
                    mode="multiple"
                    size="default"
                    placeholder="Select post category"
                    style={{ width: '100%' }}
                    {...input}>
                    {this.state.categories.map(item => (
                      <Select.Option key={item.name} value={item.name}>
                        {item.label}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              )}
            </Field>
            <Field name="metafata">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Metadata</strong>
                  </label>
                  <InputGroup {...input} />
                </div>
              )}
            </Field>
            <Field name="content">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Content</strong>
                  </label>
                  <div className="add-post__editor">
                    <Editor
                      init={{
                        images_upload_handler: (blobInfo, success, failure) => {
                          const data = blobInfo.blob();
                          this.uploadImage(data)
                            .then(({ data: { link } }) => {
                              success(link);
                              return;
                            })
                            .catch(() => {
                              failure('Error uploading image.');
                              return;
                            });
                        },

                        branding: false,
                        directionality: 'rtl',
                        plugins:
                          'image  imagetools lists hr fullscreen emoticons directionality colorpicker autoresize advlist preview'
                      }}
                      apiKey="uyhxbami3y9cf8vwokgogi449nrp9nuw8l9iy6esckja9ghs"
                      value={input.value}
                      onEditorChange={input.onChange}
                    />
                  </div>
                </div>
              )}
            </Field>
            <Field name="media">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Post Image</strong>
                  </label>

                  <Upload
                    {...input}
                    multiple={false}
                    name="media"
                    onChange={({ file }) => {
                      if (file.status === 'done') {
                        this.setState({
                          isImageChanged: true,
                          data: { ...this.state.data, media: file.response }
                        });
                        return file.response;
                      }
                    }}
                    accept={'image/jpeg,image/png'}
                    customRequest={({ file, onSuccess }) =>
                      this.uploadMainImage(file).then(url => onSuccess(url))
                    }
                    action={''}>
                    {this.state.data.media ? (
                      <img
                        alt=""
                        src={this.state.data.media}
                        style={{ maxHeight: 250, maxWidth: 250 }}
                      />
                    ) : (
                      <Button>
                        <Icon type="upload" /> Click to Upload
                      </Button>
                    )}
                  </Upload>
                </div>
              )}
            </Field>
            <Field name="isPublished">
              {({ input }) => (
                <div className="form-group">
                  <label className="add-post__label">
                    <strong>Status</strong>
                  </label>

                  <Switch
                    checkedChildren="published"
                    unCheckedChildren="draft"
                    {...input}
                    checked={!!input.value}
                  />
                </div>
              )}
            </Field>
            <span className="mr-3">
              <Button
                type="primary"
                htmlType="submit"
                loading={submitting}
                disabled={submitting || !valid}>
                Save
              </Button>
            </span>
            {/*  <Button type="danger" htmlType="submit">
              Discard
            </Button> */}
          </form>
        )}
      </Form>
    );
  }
}

export default AddForm;

class InputGroup extends React.Component {
  onChange(index, field, fieldValue) {
    const metadata = this.props.value;
    metadata[index][field] = fieldValue;
    this.props.onChange(metadata);
  }

  render() {
    const metadata = this.props.value || [];
    return (
      <React.Fragment>
        {metadata.map((item, index) => (
          <Input.Group key={item.key || Math.random().toString()}>
            <Col span={4}>
              <Input
                addonBefore="key"
                value={item.key}
                onChange={event =>
                  this.onChange(index, 'key', event.target.value)
                }
              />
            </Col>
            <Col span={6}>
              <Input
                addonBefore="value"
                value={item.value}
                onChange={event =>
                  this.onChange(index, 'value', event.target.value)
                }
              />
            </Col>
          </Input.Group>
        ))}
        <Button
          onClick={() =>
            this.props.onChange([...metadata, { key: '', value: '', icon: '' }])
          }>
          +
        </Button>
      </React.Fragment>
    );
  }
}
